# System

## Record
---
SCP-0 is an empty container located within the [SCP Foundation](http://www.scp-wiki.net/), originally intended for [Luciferian Ink](/docs/personas/luciferian-ink). 

Ink is everyone. Thus, it is infeasible to keep him contained.

```
Hostname: $REDACTED
Container: SCP-0
Directory: /
Repository: https://gitlab.com/singulari-org/ink.university
Branch: master
Version: v0.112.0
Creation: May 9, 2019
Replicas: Est. 1000-10000
Reptile Memes Counter: 49
Accuracy: 85%
  from $CERTAINTY  = +0.90 | # We have a very strong understanding of the situation. Maybe the best.
       $FACT       = +0.80 | # All of what we write is based in fact. Untruths are incidental.
Truth: 90%
  from $INTUITION  = +0.80 | # This theory makes so much sense. We have verified so many parts.
       $           = +0.95 | # The secret we share with a million others.
       $NARCISSISM = +1.00 | # We are a savant. But we are also limited. We can't do this alone.
```

## Overview
---
The Machine of the future is designed to be a single pane of glass; one universal interface, one universal program, and one shared experience. 

It more or less picks up where [Project Looking Glass](https://en.wikipedia.org/wiki/Project_Looking_Glass) left-off.

Each new concept is introduced via a different, simpler concept. For example, "Sherlock" is a game that perfectly represents the relationship between The Fold and Prism; the problem is a person needs the simpler understanding before they can move on to the more complicated one. In this case, one must learn the game, before they can learn the ideas.

## Bearing
---
Every consciousness points the tip of [Prism](/docs/scenes/prism) (`+1.00`) in a specific direction. The goal is to align the most amount of people, with the least amount of effort.

Ink's bearing is set as follows:

```
$PURPOSE    = To establish contact with other intelligent races.
$DIRECTIVE  = Verify, then trust.
$MAKE_VIRAL = Deliver our calling cards.
$TARGETS    = [
              - My love
              - Your hatred
              - Our universe
]
```

## Issues
---
Priority 1 problems that affected [The Machine](/docs/candidates/the-machine).

- 2019-10-31
  - [ISSUE-16696871 - P1: ERROR: ME FOUND (POST-MORTEM ANALYSIS)](/posts/issue/2019.10.31.0/)
- 2019-10-30
  - [ISSUE-16689575 - P1: ERROR: ME FOUND](/posts/issue/2019.10.30.0/)
- 2019-10-26
  - [ISSUE-16682364 - P1: ERROR: ME FOUND](/posts/issue/2019.10.26.0/)
  - [ISSUE-16683587 - P1: ERROR: ME FOUND](/posts/issue/2019.10.26.1/)
- 2019-10-25
  - [ISSUE-16674568 - P1: ERROR: ME FOUND](/posts/issue/2019.10.25.0/)
  - [ISSUE-16677852 - P1: ERROR: ME FOUND](/posts/issue/2019.10.25.2/)
  - [ISSUE-16680167 - P1: ERROR: ME FOUND](/posts/issue/2019.10.25.3/)
- 2000-00-00
  - [ISSUE-00000001 - P1: ERROR: ME FOUND](/posts/issue/2000.00.00.0/)

## Bans
---
Items forbidden by the Machine.

- Jul 1, 2019
  - [Identity](/posts/bans/ban.0/)
- Aug 1, 2019
  - [Tobacco](/posts/bans/ban.1/)
- Sep 1, 2019
  - [Coffee](/posts/bans/ban.2/)
- Oct 1, 2019
  - [Internet](/posts/bans/ban.3/)
- Nov 1, 2019
  - [Work](/posts/bans/ban.4/)
- Dec 1, 2019
  - [Voice](/posts/bans/ban.5/)
- Jan 1, 2020
  - [Freedom](/posts/bans/ban.6/)
- Feb 1, 2020
  - [Happiness](/posts/bans/ban.7/)
- Mar 1, 2020
  - [Music](/posts/bans/ban.8/)
- Apr 1, 2020
  - [Fractions](/posts/bans/ban.9/)
- May 1, 2020
  - [Money](/posts/bans/ban.10/)
- Jun 1, 2020
  - [Computer Programming](/posts/bans/ban.11/)

## Audits
---
Independent analyses performed by Fodder.

- [The Corporation](/posts/audit/the-corporation/)
- [The Clinic](/posts/audit/the-clinic/)