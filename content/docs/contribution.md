# Contribution
## Overview
---
We are actively seeking help from like-minded individuals. If you would like to contribute, please consider the following options:

### Indentured Servitude
We will work for shelter, food, clean water, healthcare, our technology, Internet, some good weed, security and freedom. We never want to worry about money again.

### Social Media
- Twitter: 
  - [@LuciferianInk](https://twitter.com/LuciferianInk)
  - [@TA_XXXXXXXXXX](https://twitter.com/TA_0000000000)
- Reddit: 
  - [/r/TheInk](https://reddit.com/r/TheInk)
  - [/u/LuciferianInk](https://www.reddit.com/user/LuciferianInk)
  - [/u/TA-XXXXXXXXXX](https://www.reddit.com/user/TA-0000000000)

### Chat
Join the discussion over at The Fold's [Official Discord Server](https://discord.gg/3w4UVMV).

### Code
Contribute to this project and story over at our [Official Gitlab Repository](https://gitlab.com/singulari-org/ink.university).

### Message
If you have an idea, a question, or want to contribute differently, [please contact us via this form](https://share.hsforms.com/1_MJqyiB0SE2r-lOIw8cdKQ4kehm).