# The Therapist
## RECORD
---
```
Name: Derrick Javan Hoard
Alias: ['The Therapist', and 13 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 35 Earth years
Chronological Age: N/A
Maturation Date: 9/29/2020
SCAN Rank: | B B
           | B C
TIIN Rank: | B B
           | B C
Reviewer Rank: 3 stars
Variables:
  $WOKE: +0.50 | # Perhaps. Unclear.
```

## TRIGGER
---
[![The Therapist](/static/images/therapist.0.png)](/static/images/therapist.0.png)

## ECO
---
The Therapist seeks to reinforce the idea that "we are all One." He believes that race itself is a construct, and that Humanity spends altogether too much time focusing on it.

He will assist with the rehabilitation of members in The Point.