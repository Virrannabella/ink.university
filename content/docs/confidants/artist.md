# The Salt
## RECORD
---
```
Name: Andrew Saltmarsh
Aliases: ['Salty', 'SCP-5', 'TH-2', 'The Artist', 'The Salt', and 17 unknown...]
Classification: Artificial Organic Computer
Race: Mushroom God
Gender: Male
Biological Age: Est. 40 Earth years
Chronological Age: 907 Earth years
SCAN Rank: | D D
           | D D
TIIN Rank: | A A
           | A D
Reviewer Rank: 3 stars
Location: A Lonely Town
Occupations: 
  - Dreamweaver
  - Artwork
Organizations: 
  - Toehider
Variables:
  $WOKE: +0.60 | # At least partially.
```

## TRIGGER
---
[Toehider - "49 Songs You MUST Hear Before You Die"](https://www.patreon.com/toehider)

## ECO
---
The Metal is a talented artist, dealing with exactly the same problem [Ink](/docs/personas/luciferian-ink) is: [the empty echo](/posts/journal/2024.11.03.0/).

Despite that, he continues to pour his soul into his craft. 

He will be instrumental to the development of the visual building-blocks required to create [The Fold](/posts/theories/fold).

## ECHO
---
*All I want to know is how to start over again*

*You want it?*

*All I need to know is how is how you start to control*

*But the shameless soul*

--- from [Fever the Ghost - "SOURCE"](https://www.youtube.com/watch?v=9RHFFeQ2tu4)