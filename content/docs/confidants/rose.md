# The Rose
## RECORD
---
```
Name: $REDACTED
Alias: ['The Rose', and 15 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 24 Earth years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | A B
           | B B
Reviewer Rank: 5 stars
Location: Buffalo, NY
Occupations: 
  - Special Agent
  - Actress
Organizations: 
  - Federal Bureau of Investigation
Relationships:
  - Fodder
  - The Girl Next Door
Variables:
  $WOKE: +1.00 | # She wasn't before, but she is now.
```
## TRIGGER
---
*And for this, I leave a rose beside your head.*

*Our crown of thorns.*

--- from [Slice the Cake - "The Exile Part I - The Razor's Edge"](https://www.youtube.com/watch?v=53tPazTmO3k)

## ECO
---
The Rose was the recipient of [Malcolm's](/docs/personas/fodder) [dead drop](/posts/journal/2019.12.03.1/) in Buffalo.

Where she was previously asleep, our calling cards awoke her.

## ECHO
---
*Is this one rose enough for you?*

*And who would tend to a garden that no one sees?*

*For no one?*

--- from [Caligula's Horse - "Songs For No One"](https://www.youtube.com/watch?v=P2Wgxj5-q98)