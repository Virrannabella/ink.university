# The Yuck
## RECORD
---
```
Name: $REDACTED
Aliases: ['The Yuck', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Chubby Purple Boy
Gender: Male
Biological Age: Est. 50 Earth years
Chronological Age: N/A
Location: Michigan
SCAN Rank: | A A
           | A B
TIIN Rank: | F F
           | F B
Reviewer Rank: 1 stars
Occupations: 
  - Teacher
Organizations: 
  - A School District
Relationships:
  - The Inventor
Variables:
  $DEPRAVITY: -0.90 | # Definitely depraved.
```

## ECO
---
Throughout middle and high school, there were rumors that The Yuck was too intimate with [Fodder's](/docs/personas/fodder) classmates. There were rumors that he would often take young girls into his office, alone, and close the door behind him. Nobody ever spoke about what happened inside.

Fodder did, in fact, see this behavior often. However, he never attempted to confirm or deny what happened inside. He really didn't think about it.

Context tells us now that he is the reason [The Inventor](/docs/confidants/inventor/) is the way that she is.

## PREDICTION
---
```
Immediately after publishing this, The Yuck will be arrested.
```