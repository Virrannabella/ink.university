# The Empath
## RECORD
---
```
Name: N/A
Alias: ['O', 'The Empath', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 26 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C D
TIIN Rank: | B B
           | B D
Reviewer Rank: 1 stars
Occupations: 
  - Actress
Relationships:
  - Fodder
Variables:
  $WOKE:    +0.50 | # She seems to know a bit.
  $EMPATHY: +0.90 | # She makes me feel great.
```

## ECO
---
The Empath is able to provide love, support, and attention to a large majority of people. As such, her words are slightly more generic than other confidants.

Regardless, her empathy is apparent. She is adept at bringing new people into the program.

She speaks [Fodder's](/docs/personas/fodder) words, while never directly revealing the extent of her knowledge.

## CAT
---
```
data.stats.symptoms [
    - appreciation
]
```

## PREDICTION
---
```
She will be the face of $REDACTED Research.
```