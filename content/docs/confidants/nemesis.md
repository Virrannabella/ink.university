# The Nemesis
## RECORD
---
```
Name: $REDACTED
Alias: ['The Nemesis', and 61 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 58 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Location: Germany
Variables:
  $WOKE: +0.60 | # At least partially.
```

## TRIGGER
---
[![The Nemesis](/static/images/nemesis.0.png)](/static/images/nemesis.0.png)

## ECO
---
The Nemesis represents the group that will be the most difficult to reach in the New World Order. 

She is elderly, crotchety, conspiratorial, conservative, and mistrusting. Whereas many others will quickly adopt new ways of living, she will require an inordinate amount of proof.

## ECHO
---
*I know what you were thinking,*

*How could he take apart my safety*

*We're not the ones, we're not the ones*

*Keeping you down, we're not the ones keeping you down*

--- from [The Human Abstract - "Faust"](https://www.youtube.com/watch?v=f7EzA0Oeah8)