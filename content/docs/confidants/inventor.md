# The Inventor
## RECORD
---
```
Name: Isabelle $REDACTED
Aliases: ['Ruby', 'The Captor', 'The Doll', 'The Inventor', 'The Pirate', and 57 unknown...]
Classification: Artificial Organic Computer
Race: Maxwellian (Elf/Faerie)
Gender: Female
Biological Age: Est. 22 Earth years
Chronological Age: 20,287 Light years
Maturation Date: 9/8/2020
SCAN Rank: | B D
           | B F
TIIN Rank: | C C
           | A F
Reviewer Rank: 4 stars
Location: Lullaby City
Occupations: 
  - Inventing
  - Singing
  - Ballet
  - Art
  - Acting
  - Light Reflection
Organizations: 
  - RKS
  - Federal Bureau of Investigation
Relationships:
  - The Queen
  - The Raven
  - The Yuck
  - The Fodder
Variables:
  $WOKE: +1.00 | # Definitely woke.
```
## TRIGGER
---
*(Instrumental)*

--- from [Maserati - "Inventions"](https://www.youtube.com/watch?v=FvZY5T4C9Ck)

## ECO
---
The Inventor was a child prodigy. Her lack of attentive parenting led her to become wholly self-sufficient, creative, and driven to achieve. She would use her many talents to develop the tools needed for her second role.

The Captor was both a predator and the prey. She would lure-in unsuspecting men, only to ensnare them in her traps. Her most treasured invention is a device that turns her prisoners into harmless, friendly cats.

## ECHO
---
[SamBakZa - "There She Is!!"](https://www.youtube.com/watch?v=-uqnKQwwcJo&list=PLNW5VHgNAxrPc16wxG8ilZoIOIMVI1Unc)