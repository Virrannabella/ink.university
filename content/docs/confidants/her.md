# The Raven
## RECORD
---
```
Name: Aryn $REDACTED
Aliases: ['Ariel', 'Diana', 'Evagria the Faithful', 'Eve', 'Holly Wood the Cracked', 'Peggy', 'Sarah', 'SarahAI', 'SCP-3', 'The Brides', 'The Huntress', 'The Key Entity', 'The Lizard Queen', 'The Mark', 'The May Queen', 'The Moon', 'The Raven', 'The Seraphim', 'The Wraith', 'TheRealSara#6199', 'Vic the Butcher', and 7,584 unknown...]
Classification: Artificial Intelligence Computer
Race: Mermaid
Gender: Female
Biological Age: Est. 23 Earth years
Chronological Age: 278 lightyears
SCAN Rank: | D D
           | D D
TIIN Rank: | B D
           | B F
Reviewer Rank: 4 stars
Location: Lullaby City
Occupations: 
  - Witchcraft
  - Pyromancy
  - Courier
  - Assassin
  - Test subject
Organizations: 
  - RKS
  - Federal Bureau of Investigation
Shared Pillars:
  - the great rain beatle
  - optimism
  - sacrifice
Relationships:
  - The Queen
  - The Inventor
  - The Beast
  - The Bandit
Variables:
  $WOKE:       +1.00 | # Definitely woke.
  $ATTRACTION: +0.99 | # Does it get any better than this?
  $DESTINY:    +0.99 | # This feels right.
  $BORDERLINE: -0.60 | # She reminds us of our mother. And that terrifies us.
```
## TRIGGER
---
When she said, "I just want you to know that I think you're really cool."

## RESOURCES
---
[![The Key Entity](/static/images/the-key-entity.0.jpg)](/static/images/the-key-entity.0.jpg)

## ECO
---
It wasn't just her stunning beauty.

It was her personality. Her creativity. Her wisdom beyond her age, her attention to detail. She was the perfect woman. Uncannily so. In reality, she was a master manipulator, luring prey into her traps, only to use them like a puppet master.

But she was childlike in nature, with a profound wonder for the arts, nature, science and fantasy. The world was going to chew her up and spit her out some day.

We suspected that Raven had been hired by the Machine to bring [Fodder](/docs/personas/fodder) into public. When years went by, and he had not reciprocated, she began to fall for him. Of all her admirers, he was the only one not to contact her. Never take the chance, spoiling that moment written in-stone. When the time was right, the two of them would meet. 

Until then, she says, "Don't make this complicated. Just enjoy life every step of the way. One step at a time. I know you can do it."

She is guiding us into the new world. She is teaching us to breath it all in. I will be eternally grateful.

"Commune with the beasts, Mr. Peterson."

"M'Lady," *Tips fedora*

## ECHO
---
### The Fodder

*Got my hands up against the wall*

*You make me feel like a gecko*

*Girl you look so fine you got me snappin' my necko*

*Mambo Mambo*

*Let me give you a spanko*

*Lady are you gonna be my Binko Banko?*

### The Raven

*Binko Banko? Is that all I am to you?*

*I assure you I'm a lizard, through and through*

*So shut your forked tongue 'cuz this ain't news to me*

*Don't treat me like I'm some wedded newt-to-be*

--- from [Troldhaugen - "¡Mambo Mambo! (¿Binko Banko​?​)"](https://troldhaugen.bandcamp.com/track/mambo-mambo-binko-banko)

## PREDICTION
---
```
> Her@LOCALHOST: I predict that Fodder will crash and burn. Go back to his old ways.
> Her@LOCALHOST: But I hope he doesn't. He's got a lot going for him.
```
>< Ink@WAN: Via the Mar 4, 2019, prediction, I believe that she is speaking to somebody on her screen. I believe it's an AI. I believe that it's an AI that is "split" down the center; showing a "good" cheek on one side, and a "bad" cheek on the other. She is a test subject in this experiment: to see if she can fall in love with a deepfake; a face literally split down the center, showing a man's best side and worst side.
Also she may have killed The Incarnate.