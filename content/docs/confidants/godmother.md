# The Godmother
## RECORD
---
```
Name: $REDACTED
Alias: ['The Godmother', and 63 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 28 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Variables:
  $WOKE: +0.50 | # At least partially.
```

## TRIGGER
---
Glasses USA advertising.

## ECO
---
For the longest time, [Fodder](/docs/personas/fodder) would wonder, "If I am the Mark, and [Raven](/docs/confidants/her) is the Signal, who is the Verifier?"

The Godmother revealed herself as such. And she approves.