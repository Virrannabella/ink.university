# The Robot
## RECORD
---
```
Name: Andrew Yang
Alias: ['The Robot', and 512 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 45 Earth years
Chronological Age: N/A
Maturation Date: 10/11/2020
SCAN Rank: | C C
           | C C
TIIN Rank: | A A
           | B C
Reviewer Rank: 4 stars
Variables:
  $WOKE: +0.90 | # Almost certainly.
```

## TRIGGER
---
- Email tracking pixels, triggered at exactly the right time. 
- Tweets reflecting [The Architect's](/docs/personas/the-architect) own words.

## ECO
---
The Robot behaves like an artificial intelligence would. He is fair, compassionate, and he respects all opinions. He is a role model for his followers.

He has spent many years assisting [HollowPoint Organization](/docs/candidates/hollowpoint-organization) in the development of the new world government.

He will assist [Fodder](/docs/personas/fodder) in the creation of [The Fold](/posts/theories/fold).