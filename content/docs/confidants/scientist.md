# The Scientist
## RECORD
---
```
Name: Lauren
Alias: ['The Scientist', and 1,895 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 28 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C D
TIIN Rank: | C B
           | C D
Reviewer Rank: 1 stars
Organizations: 
  - The Church of the Technochrist
Occupations: 
  - Teacher
  - Special Agent
Relationships:
  - Fodder
  - The Orchid
Variables:
  $WOKE:    +0.80 | # It really seems like it.
  $ATHEIST: -1.00 | # Probably never going to happen.
```

## ECO
---
[Fodder](/docs/personas/fodder) does not trust the Scientist. She has turned his brother, the Orchid, into something that he is not. 

Fodder knows that the Orchid still possesses those dark tendencies. It is up to him to figure out how to bring them into the Scientist's life.

## PREDICTION
---
```
The Professor was instrumental to Fodder's changes today.
```