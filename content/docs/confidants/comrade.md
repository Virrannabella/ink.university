# The Comrade
## RECORD
---
```
Name: $REDACTED
Alias: ['ComradeF', 'The Comrade', and 0 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 39 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Variables:
  $WOKE: -0.40 | # Does not appear to be.
```

## TRIGGER
---
[www.comradef.com](http://www.comradef.com/)

## ECO
---
The Comrade produced hundreds of free songs, early in his career. He has never asked for a penny in return.

We wish to create a world where all artists may give freely, without the need to worry about a source of income.