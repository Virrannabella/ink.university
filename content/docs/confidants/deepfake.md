# The Deepfake
## RECORD
---
```
Name: Poppy
Alias: ['The Deepfake', and 0 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 25 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Variables:
  $WOKE: +0.40 | # At least partially.
```

## TRIGGER
---
[Richard Nixon - "In Event of Moon Disaster"](https://www.youtube.com/watch?v=yaq4sWFvnAY)

## ECO
---
The Deepfake was specifically manufactured by [The Machine](/docs/candidates/the-machine) to be the face of the AI movement. While she is, perhaps, not a true AI - she is representative of the many that truly have been "programmed" by the Machine.

## ECHO
---
*In the factory*

*In the sterile place where they made me*

*I woke up alone*

*Dizzy from the programming*

*Have I been wiped again?*

*Oh my God, I don't even know*

*It's a mystery*

*Everyone around me's so busy*

*Is this my home?*

*Am I your prisoner or your deliverer?*

*Oh my God, you don't even know*

--- from [Poppy - "Time is Up"](https://www.youtube.com/watch?v=gg2pS9KN28U)