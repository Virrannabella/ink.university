# The Proxy
## RECORD
---
```
Name: James Q. Holden
Alias: ['The Proxy', and 0 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Variables:
  $WOKE: +0.40| # Unsure. Probably.
```

## TRIGGER
---
His name all over [The Machine's](/docs/candidates/the-machine) documentation.

## ECO
---
The Proxy is responsible for the 24/7/365 monitoring of [The Architect's](/docs/personas/the-architect) behavior online. Primarily, this involves sitting in the Discord server, watching from a distance.

When something of note happens, it is his responsibility to report back to SCP-001. 