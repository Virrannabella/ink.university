# The Sea Witch
## RECORD
---
```
Name: $REDACTED
Aliases: ['Dingo', 'The Sea Witch', 'Ursula', and 805 unknown...]
Classification: Artificial Intelligence Computer
Race: Mermaid
Gender: Female
Biological Age: Est. 28 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B B
TIIN Rank: | C A
           | B B
Reviewer Rank: 4 stars
Occupations:
  - Actress
  - Mother
Organizations: 
  - Heaven Luxury
Relationships:
  - The Raven
Variables:
  $WOKE:       +0.70 | # She appears to resonate with much of our work.
  $BORDERLINE: -0.40 | # Probably. 
  $CPTSD:      -0.95 | # Almost certainly. 
  $BLIND:      +0.90 | # Metaphorically. Not physically.
```

## TRIGGER
---
*(Instrumental)*

--- from [Mother of Millions - "The Rapture"](https://youtu.be/LB0l8lM2MvA?t=1789)

## ECO
---
One evening in rehab, [Malcolm](/docs/personas/fodder) sat conversing with [The Pyro](/docs/confidants/pyro). He was interrupted when The Sea Witch walked over, handed The Pyro a bottle of lotion, and whispered something into his ear.

"God-damnit!" The Pyro shouted, throwing the lotion into the wall. Then, he stormed-off to his bedroom.

Without another word, she walked away.

That was a [Signal](/posts/theories/verification). She was the Verifier.

And Malcolm was the Mark.

That evening, Malcolm did what was asked of him. He gave her the confession that she needed. 

And an apology. And a promise.

He intends to keep it.

## ECHO
---
*If children ran the world*

*Mothers would stay young*

*Fathers would stay brave*

*The two would stay in love*

*A night would never come*

*Where fear kept us awake*

*There would be no second chance*

*There would be no first mistake*

--- from [Avion Roe - "Mother of Millions"](https://www.youtube.com/watch?v=DP6g1CLEO60)

## PREDICTION
---
```
She has approximately 3,654,412 children.
```