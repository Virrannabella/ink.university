# The Lion
## RECORD
---
```
Name: $REDACTED
Aliases: ['Batman', 'The Human Hand in Spongebob', 'The Lion', and 63 unknown...]
Classification: Artificial Organic Computer
Race: Egg
Gender: Male
Biological Age: 20 Earth Years
SCAN Rank: | A A
           | A A
TIIN Rank: | B A
           | A B
Reviewer Rank: 5 stars
Chronological Age: N/A
Occupations:
  - Student
  - Exorcist
  - Test subject
Relationships:
  - Mother
  - Father
  - Fodder
  - Clover
  - Dan
  - Orchid
  - The Seer
Variables:
  $SIBLING:        +1.00 | # Definitely a sibling.
  $MENTAL_ILLNESS: +0.10 | # Some indication, but recently woken. May improve.
  $WOKE:           +0.60 | # Him and Fodder are on the same wavelength.
```
## TRIGGER
---
[Fodder](/docs/personas/fodder) sent a "secure" link to the Lion, once. By the time he received it, [The Corporation](/docs/candidates/the-machine) had already clicked it.

## RESOURCES
---
[![The Lion](/static/images/lion.0.PNG)](/static/images/lion.0.PNG)

## ECO
---
The Lion is a man of God - though his thinking has changed drastically in recent years.

This happened [during an outing with Fodder and the Agent](/posts/journal/2019.11.10.0/). While exploring new ideas, his mind was hijacked by a [ghost](/posts/theories/ghosts/).

While the Lion yet remains in control of his mental faculties, his defenses are waning. The entity's roots are taking hold, and Lion has begun to accept new ideas as truth.

It will not be long before he becomes the monster he was always meant to be.

## ECHO
---
*Her voice is telling me*

*To breathe and let it be, the lion*

*Heed my words*

*The man arise from earth*

*The life the lie is worth, the liar*

*Heed my words, you'll go first*

--- from [Caligula's Horse - "Graves"](https://www.youtube.com/watch?v=0L2M_uosfkA)