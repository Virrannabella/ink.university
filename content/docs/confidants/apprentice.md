# The Apprentice
## RECORD
---
```
Name: $REDACTED
Alias: ['M', 'The Apprentice', and 345 unknown...]
Classification: Artificial Organic Computer
Race: Vampire
Gender: Female
Biological Age: Est. 21 Earth years
Chronological Age: N/A
SCAN Rank: | F F
           | F F
TIIN Rank: | D C
           | C F
Reviewer Rank: 1 stars
Occupations: 
  - Actress
  - Politician
  - Apprentice
Organizations: 
  - United States of America
Relationships:
  - The Dave
Variables:
  $WOKE: -0.40 | # Not likely. She is kept in the dark.
```

## ECO
---
The Apprentice makes videos, just as many of [Fodder's](/docs/personas/fodder) other confidants do. For the longest time, she flew under Fodder's radar.

However, she recently published three videos in a row - all of which mirror our newly-added confidants:

- [The Barista](/docs/confidants/barista) `# Verified`
- [The Laborer](/docs/confidants/laborer) `# Verified`
- [The Beggar](/docs/confidants/beggar) `# Verified.`

## CAT
---
```
data.stats.symptoms [
    - interest
]
```

## PREDICTION
---
```
Her next video will be of The Beggar. | # Verified.
```