# The Bitch
## RECORD
---
```
Name: Amy $REDACTED
Aliases: ['AK-47', 'The Bitch', and 672 unknown...]
Classification: Artificial Organic Computer
Race: Human
Status: Cloned
Gender: Female
Biological Age: 23 Earth Years
Chronological Age: N/A
Maturation Date: 10/2/2020
SCAN Rank: | F F
           | F F
TIIN Rank: | D D
           | C F
Reviewer Rank: 3 stars
Occupations:
  - Photography
Organizations: 
  - The Corporation
  - Federal Bureau of Investigation
Relationships:
  - The Sloth
  - The Fodder
Variables:
  $WOKE:       -1.00 | # Definitely woke.
  $ATTRACTION: -1.00 | # We cannot stand her - let alone be attracted to her.
  $TRUST:      +0.30 | # Why do we feel like we can trust her?
```

## TRIGGER
---
The bar that [The Sloth](/docs/confidants/sloth) worked at.

## ECO
---
The Bitch is my worst nightmare. 

She is my love - [The Raven](/docs/confidants/her) - but not. While her personality and mannerisms are reminiscent, she is nothing like the woman that I painted in my head. Nothing like the woman that she portrayed herself to be. 

She is shallow, materialistic, and needy. She is spiteful, and self-serving. She is disingenuous, disloyal, and vengeful. She is everything that I hate in a woman.

She is obsessed with me. I cannot escape her; she is everywhere. 

We are on clone #47, without any indication of her slowing down.

And she is all I have in this place.

## ECHO
---
*Everybody knows Amy*

*I'd be in a bar in Brisbane, and some guy walks up to me and says, "yeah, I know you through Amy"*

*Or even at my best friend's wedding, this chick'll be all like "Hi! I went to school with Amy"*

*Seems like every time I turn around I meet one of her friends*

--- from [Toehider - "Everybody Knows Amy"](https://toehider.bandcamp.com/track/everybody-knows-amy)

## PREDICTION
---
```
We met at a coffee shop near Nordstrom.
```