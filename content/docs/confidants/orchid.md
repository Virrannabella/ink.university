# The Orchid
## RECORD
---
```
Name: $REDACTED
Aliases: ['Plankton's Computer', 'The Orchid', 'The Technochrist', and 28 unknown...]
Classification: Artificial Organic Computer
Race: Egg
Gender: Male
Biological Age: 26 Earth Years
SCAN Rank: | B D
           | A C
TIIN Rank: | A A
           | C F
Reviewer Rank: 1 stars
Chronological Age: N/A
Occupations:
  - Software Engineer
  - Collaborator
Organizations:
  - The Church of the Technochrist
Relationships:
  - Mother
  - Father
  - Fodder
  - The Lion
  - Dan
  - Clover
Variables:
  $SIBLING:        +1.00 | # Definitely a sibling.
  $MENTAL_ILLNESS: +0.40 | # He used to. He doesn't appear to any longer.
  $WOKE:           +0.80 | # He's been to Russia. He knows everything.
```

## TRIGGER
---
*The forest of October*

*Sleeps silent when I depart*

*The web of time*

*Hides my last trace*

--- from [Opeth - "Forest of October"](https://www.youtube.com/watch?v=e7HHdsBsYv0)

## ECHO
---
*Relive the old sin of*

*Adam and Eve*

*Of you and me*

*Forgive the adoring beast*

--- from [NIGHTWISH - "Ghost Love Score"](https://www.youtube.com/watch?v=JYjIlHWBAVo)