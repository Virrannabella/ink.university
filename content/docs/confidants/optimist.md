# The Optimist
## RECORD
---
```
Name: $REDACTED
Alias: ['Kel', 'The Optimist', and 16 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 25 Earth years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | C B
           | B D
Reviewer Rank: 2 stars
Occupations: 
  - Actress
Organizations: ORNL
Relationships:
  - The Metal
  - The Artist
Variables:
  $WOKE: +0.40 | # At least partially.
```

## TRIGGER
---
When asked why she chose her creepy profile picture, she replied, "Because this is how I feel right now."

## RESOURCES
---
[![The Optimist](/static/images/optimist.0.png)](/static/images/optimist.0.png)

## ECO
---
The Optimist is eternally hopeful and positive, though she knows very little about what's truly going on right now. She trusts with an open heart.

She doesn't realize just how important some of the messages she's relayed to [Fodder](/docs/personas/fodder) are.

## CAT
---
```
data.stats.symptoms [
    - trust
]
```

## ECHO
---
*But I'm a creep*

*I'm a weirdo*

*What the hell am I doing here?*

*I don't belong here*

--- from [Radiohead - "Creep"](https://www.youtube.com/watch?v=XFkzRNyygfk)

## PREDICTION
---
```
She is clairvoyant.
```