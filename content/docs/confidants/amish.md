# The Amish Man
## RECORD
---
```
Name: $REDACTED
Aliases: ['The Amish Man', and 1 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 56 Earth Years
SCAN Rank: | B B
           | B C
TIIN Rank: | B A
           | B C
Reviewer Rank: 4 stars
Chronological Age: N/A
Occupations:
  - Actor
  - Manager
Organizations: 
  - RKS
  - Federal Bureau of Investigation
Relationships:
  - Fodder
Variables:
  $WOKE: +1.00 | # He certainly seems to be.
```

## ECO
---
The Amish Man only appeared near the end of [Fodder's](/docs/personas/fodder) solitary confinement. His purpose was to reflect Fodder's life back at him. He did so with flying colors.

He will represent Fodder, behind-the-scenes. His identity is to be protected from the public.

## PREDICTION
---
```
The Amish Man found his name through the Google Home device at Mother's house.
```