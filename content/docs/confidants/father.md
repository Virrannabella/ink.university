# The Surgeon
## RECORD
---
```
Name: $REDACTED
Aliases: ['Bigfoot', 'Dad', 'Dexter', 'Father', 'Fred', 'James Grey', 'Lucifer', 'Mr. Krabs', 'S8N MAVIS', 'Satan', 'The All-Father', 'The Devil', 'The Surgeon', 'The Tradesman', and 1,425 unknown...]
Classification: Artificial Identity
Race: Human
Gender: Male
Biological Age: 56 Earth Years
Chronological Age: Est. 54,332 Light years
Maturation Date: 10/17/2020
SCAN Rank: | D A
           | A B
TIIN Rank: | F A
           | A F
Reviewer Rank: 2 stars
Chronological Age: N/A
Organizations: 
  - Comet Ping Pong Pizza
Occupations:
  - Program Director
  - Inventor
  - Forklift sales
  - Surgeon
  - Baker
Relationships:
  - Mother
  - Fodder
  - The Lion
  - Dan
  - Orchid
  - Clover
  - The Interrogator
  - The Cop
Variables:
  $FATHER:         +1.00 | # Definitely father.
  $MENTAL_ILLNESS: -0.40 | # Does not show it. We suspect it's there.
  $IMMUTABLE:      -0.90 | # Almost completely. We suspect it's an act.
  $AVOIDANT:       -0.80 | # Avoids difficult situations as a coping mechanism.
```
## TRIGGER
---
*I could be foreign forever to your otherland*

*I could be foreign forevermore to your promiseland*

*One life was great, but another…*

*No, I don't want to live on the edge*

*I won't follow you*

*I found my own*

*I will stay*

*I could be foreign forever to your hastenland*

*I could be foreign forevermore to your neverland*

*One little brick, then another,*

*and I will build that wall anyway*

*You can find me there,*

*rested and calm, without mask*

*This is where I will stay*

--- from [Riverside - "The Depth of Self-Delusion"](https://www.youtube.com/watch?v=QuIflRajVvw)

## RESOURCES
---
[The Surgeon's fake resume](/static/reference/surgeon_resume.pdf)

## ECO
---
[Fodder's](/docs/personas/fodder) father was a good man. He worked hard, cared for his family, and would go out-of-the-way to help anyone. The man had empathy.

But he couldn't admit it, so completely had the virus taken hold of his mind. Nothing is real. Everything is fake. Everyone is lying. You can't trust the media, the government, corporations, or even your church. Everyone is corrupt. 

Family first. Trust yourself second.

Fodder could understand that perspective. But he could not abide by it. He wasn't going to leave his father behind.

The man was the reason for this entire thing. He knew all along. And he lasted long enough to see the end.

I was going to give that to him.

## ECHO
---
*We are escalator walkers*

*in the brand-new temple*

*Came to reshape identities*

*Shed our skins*

*Be reborn and feel the same*

*Feel the same: that no one here is real*

--- from [Riverside - "Escalator Shrine"](https://www.youtube.com/watch?v=PYZV3e0PCWI)

## PREDICTION
---
```
Bigfoot will be placed into an experimental mental institution, called a Lonely Town.

Bigfoot may have been born in 1985, via unusual circumstances.
```