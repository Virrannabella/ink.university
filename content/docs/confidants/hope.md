# The Hope
## RECORD
---
```
Name: $REDACTED
Alias: ['The Hope', and 16 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 26 Earth years
Chronological Age: N/A
SCAN Rank: | B C
           | A D
TIIN Rank: | B A
           | A D
Reviewer Rank: 4 stars
Occupations: 
  - Mother
Relationships:
  - Fodder
  - The Protector
Variables:
  $WOKE:    +0.80 | # It really seems like it.
  $ATHEIST: +0.95 | # Somebody told us she is.
```

## TRIGGER
---
We considered using "Anaconda," but nah. Trying to keep things kid-friendly around here.

Plus the lyrics remind us of something a shitty AI would generate.

## ECO
---
This was one of the first - if not the very first - women to see [Fodder](/docs/personas/fodder) for who he really is. To treat him with respect. 

When she married [Fodder's brother](/docs/confidants/protector), she was a holy woman. Over time, she lost her faith - just as Fodder had. Fodder may have had an impact on her decision.

Her primary job is to keep the family together. To figure out how to make a theist/atheist dynamic work.

She is succeeding.

## ECHO
---
*Pop music 101*

*Some simple instructions, for a good first impression*

*Now lets start with verse one*

*A minor chord, tensions grow*

*Fade in the bass like so*

*Now with momentum go*

*Stop*

*And bring the beat back*

--- from [Marianas Trench - "Pop 101"](https://www.youtube.com/watch?v=yBDNvlvR8vA)

## PREDICTION
---
```
Hope was instrumental to Fodder's changes today.
```