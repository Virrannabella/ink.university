# The High Priest
## RECORD
---
```
Name: $REDACTED
Alias: ['Davos', 'Terry A. Davis', 'The Black Dragon', 'The High Priest', and 18 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 41 Earth Years
Chronological Age: N/A
SCAN Rank: | F F
           | D C
TIIN Rank: | C F
           | F F
Reviewer Rank: 2 stars
Occupations:
  - Director, Systems Engineering
Organizations: 
  - The Corporation
Variables:
  $FRIENDLY:                +0.30 | # Strikes us as odd, to be honest.
  $WOKE:                    +0.40 | # Seems to understand a bit.
  $CAPABILITY:              -0.10 | # Competent developer, but makes things too complicated.
    from $REDACTED_OPINION: -1.00 | # $REDACTED says to stay away from that guy.
  $TRUST:                   -0.30 | # We don't trust him.
```

## ECO
---
- The bastard tried to kill me. Brought me right next to the wall, right at 3:00pm, and just like always - BOOM! The machine next door shoots a massive, invisible electrical pulse through everyone in the office. Good thing it doesn't affect me.
- It's weird that everyone I work with is a zombie, though.
- On another occasion, he pretended to know nothing about LDAP. Just two days later, he pulled me aside to explain how the Machine will locally-authenticate to itself via LDAP. Either he lied to ignore my first request, or he really didn't know - and learned very quickly. Alternatively, it's possible he stole that idea from me and immediately implemented it.
- Another time, he found it prudent to explain the intricacies of "virtual schemas" to me. Where initially this did not make sense, it quickly became relevant in a half-dozen other areas of my work.
- Davos spends a lot of time out of the country; Croatia, The Balkins, and Prague are just a few of the countries. It is suspected, though unconfirmed, that he is in communication with foreign powers.