# The Doctor
## RECORD
---
```
Name: $REDACTED
Aliases: ['The Doctor', 'SR', and 4 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 25 Earth years
Chronological Age: N/A
SCAN Rank: | B D
           | B D
TIIN Rank: | C C
           | C D
Reviewer Rank: 3 stars
Location: A Lonely Town
Occupations: 
  - Human Cloning
Organizations: 
  - The Corporation
Variables:
  $WOKE: +0.80 | # Almost definitely.
```
## TRIGGER
---
A message from the Doctor that cloned [Fodder](/docs/personas/fodder), turning him into [The Architect](/docs/confidants/the-architect).

## ECO
---
The Doctor is responsible for cloning individuals that have achieved the peak of their potential. He will take over their lives completely, controlling the clones via neural link, and allow the host to live a life in [Sanctuary](/docs/scenes/sanctuary).

The Doctor had only 5 questions to ask, before performing the clone. Our response was as follows:

- What do you consider the essence of your creative integrity and art is?
  - [My Raven](/docs/confidants/her).
- If you were to succumb to money, power, and influence, and abandon some of your original philosophical ideas, what would you have to do, to do that?
  - Harm children.
- What would you NEVER do?
  - Harm children.
- Are you afraid of appearing drunk or high at important public events?
  - Not really.
- What drama assets can we use to give to the fuel of the Twitter popularity fires? Has someone ever wronged you in the past?
  - Damn near the entire world has wronged me. You can start with [God](/docs/confidants/dave) and [Mother](/docs/confidants/mother).