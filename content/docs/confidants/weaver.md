# The Weaver
## RECORD
---
```
Name: Arachne Siofra
Alias: ['The Weaver', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 29 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | B B
           | B C
Reviewer Rank: 2 stars
Location: MI
Occupations: 
  - Storyteller
  - Thoughtweaver
Relationships:
  - Fodder
Variables:
  $MENTAL_HEALTH: -0.60 | # Not well. Depressed most of his life.
```
## TRIGGER
---
*Weave us a mist*

*Thought weaver*

*Hide us in shadows*

*Unfathomable wall-less maze*

*A secular haze*

--- from [Ghost - "Secular Haze"](https://www.youtube.com/watch?v=vyQZ13jobIY)

## ECO
---
The Weaver will tell this story to [Fodder's](/docs/personas/fodder) friends and family back home. 

Somebody needs to tell him to check his damn email, though. We sent one to b**************d@gmail.com quite some time ago - with no response.

He will be a test of Fodder's theory of [Ghosts](/posts/theories/ghosts).