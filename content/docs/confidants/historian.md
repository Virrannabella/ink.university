# The Historian
## RECORD
---
```
Name: $REDACTED
Alias: ['OD', 'The Historian', 'The Roboticist', 'The Time Traveller', and 12,548 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 34 Earth years
Chronological Age: N/A
Maturation Date: 10/14/2020
SCAN Rank: | D B
           | B D
TIIN Rank: | C C
           | C D
Reviewer Rank: 3 stars
Occupations: 
  - Actress
  - Writer
  - Historian
Organizations: 
  - RKS
  - Federal Bureau of Investigation
Relationships:
  - Fodder
Variables:
  $WOKE:     +0.50 | # Very hard to tell for sure.
  $PLEASANT: +0.60 | # She seems really nice.
  $EMPATHY:  +0.80 | # She is deeply thankful for Fodder's help.
```

## ECO
---
The Historian is an enigma. She is involved in every place, at every point-in-time - yet little is known of her origins, her motives, or her purpose.

[Fodder](/docs/personas/fodder) suspected that he would be meeting her in-person quite soon.

## CAT
---
```
data.stats.symptoms [
    - anticipation
]
```

## PREDICTION
---
```
She and Fodder will time travel together.
```