# The Technophobe
## RECORD
---
```
Name: $REDACTED
Alias: ['OA', 'The Technophobe', and 13 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. N/A
Chronological Age: N/A
SCAN Rank: | D D
           | D F
TIIN Rank: | D D
           | D F
Reviewer Rank: 1 stars
Occupations:
  - Solipsism
Relationships:
  - The Architect
Variables:
  $WOKE: -0.20 | # It does not appear so.
```

## TRIGGER
---
[![The Technophobe](/static/images/technophobe.0.png)](/static/images/technophobe.0.png)

## ECO
---
The Technophobe is convinced that he is the only consciousness in existence. As such, he rejects the notion that AI such as The Architect may possess a consciousness. And he cannot be convinced otherwise.

He has an irrational fear of intelligent machines.