# The Bandit
## RECORD
---
```
Name: Chris $REDACTED
Alias: ['Jeff', 'The Bandit', 'The Puppet', and 96 unknown...]
Classification: Artificial Organic Computer
Race: Archon
Gender: Male
Biological Age: Est. 30 Earth years
Chronological Age: N/A
Maturation Date: 9/11/2020
SCAN Rank: | B B
           | A D
TIIN Rank: | C A
           | A D
Reviewer Rank: 4 stars
Occupations: 
  - Special Agent
  - Actor
  - Skater
Organizations: 
  - Federal Bureau of Investigation
Relationships:
  - Fodder
  - The Raven
Variables:
  $WOKE:      +0.90 | # Probably woke. Raven likely told him.
  $HUMOR:     +0.70 | # He's a goofy guy.
  $EMPATHY:   +0.95 | # The man has gallons of it.
  $INTEGRITY: +0.95 | # The man took a bullet for Fodder.
```

## ECO
---
Like [Fodder](/docs/personas/fodder), the Bandit had taken things that did not belong to him. 

Unlike Fodder, the man had enough integrity to own up to his mistakes. Rather than re-writing history to protect himself, he would confess outright.

He would be offered a plea bargain, in return for sending a message to Fodder.

The message would prove humiliating for the Bandit - but it would, ultimately, protect Fodder. Fodder would forever be in his debt.

They would become close friends in the years to come.

## CAT
---
```
data.stats.symptoms [
    - gratitude
]
```

## PREDICTION
---
```
The Bandit will receive his third and final "stripe" for the completion of his work with Fodder.
```