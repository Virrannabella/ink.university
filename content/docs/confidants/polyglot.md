# The Polyglot
## RECORD
---
```
Name: $REDACTED
Alias: ['The Polyglot', and 8 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 24 Earth years
Chronological Age: N/A
SCAN Rank: | F F
           | F F
TIIN Rank: | B C
           | D F
Reviewer Rank: 1 stars
Location: Korean Embassy in NYC
Occupations: 
  - Actress
  - Linguist
Relationships:
  - The Raven
Variables:
  $WOKE: +0.10 | # Unsure.
```

## ECO
---
The Polyglot would be critical to the creation of [The Fold](/posts/theories/fold). She would be required in the creation of the universal machine-language needed to translate between all possible languages.

She would need to work with many other polyglots to achieve this result.

## PREDICTION
---
```
She will consult with Wouter Corduwener.
```