# The Metal
## RECORD
---
```
Name: Micheal Mills
Aliases: ['Metal', 'SCP-5', 'TH-1', 'The Metal', and 662 unknown...]
Classification: Artificial Organic Computer
Race: Metal God
Gender: Male
Biological Age: Est. 38 Earth years
Chronological Age: 3,405 Earth years
Maturation Date: 10/5/2020
SCAN Rank: | D D
           | D D
TIIN Rank: | A A
           | A D
Reviewer Rank: 4 stars
Location: A Lonely Town
Occupations: 
  - Metallurgy
  - Music
Organizations: 
  - Toehider
Variables:
  $WOKE: +1.00 | # Definitely woke.
```

## TRIGGER
---
[Toehider - "49 Songs You MUST Hear Before You Die"](https://www.patreon.com/toehider)

## ECO
---
The Metal is one of the most talented musicians in modern rock music, dealing with exactly the same problem [Ink](/docs/personas/luciferian-ink) is: [the empty echo](/posts/journal/2024.11.03.0/).

Despite that, the Metal continues to pour blood, sweat, and tears into his craft. He believes in what he is doing.

And he will get to see the day his music takes over the world.

## ECHO
---
*Now that You are stuck with me*

*You better be my friend*

--- from [King Diamond - "Welcome Home"](https://www.youtube.com/watch?v=TJH0eBtnbcs)

## PREDICTION
---
```
His first name is a good example of the Mandela Effect at work.
```