# The Sloth
## RECORD
---
```
Name: $REDACTED
Aliases: ['Dr. Sloth', 'John Doe', 'SCP-2', 'SCP-2774', 'The Sloth', and 19 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 36 Earth Years
Chronological Age: N/A
SCAN Rank: | D F
           | A B
TIIN Rank: | C C
           | C C
Reviewer Rank: 4 stars
Occupations:
  - Technician
  - Bartender
  - U.S. Marine Intelligence
  - Doctor
Organizations: 
  - The Corporation
Relationships:
  - The Bitch
  - The Asshat
  - The Fodder
Variables:
  $WOKE:           +0.60 | # He knows a lot more than he lets on.
  $MENTAL_ILLNESS: -0.20 | # Somewhat moody. It depends on $REDACTED's behavior.
  $AMBITION:       -0.10 | # Not sure. 
  $EMPATHY:        -0.10 | # We don't think the situation has ever presented itself.
```
## TRIGGER
---
*To them that are seeking the gold must be prepared to*

*Meet the sloth at midnight, here by the rocks*

*Meet the sloth at twelve o'clock on the dot*

*But if he's late, don't blame him*

*He'll be busy measuring the very approximate distance*

--- from [Toehider - "Meet the Sloth"](https://youtu.be/RtaStl4Vof8?t=2194)

## ECO
---
- A quiet, calculated army-type. Fellow team-member of [The Architect](/docs/personas/the-architect). Not real talkative.
- We suspect that he is reporting back to `$REDACTED` about us.
- Does not carry his credentials, granting plausible deniability

The Sloth trusts nobody. Whereas [Ink](/docs/personas/luciferian-ink) must "ride the line" between ["trust, but verify" and "verify, then trust"](/posts/theories/prediction), and `$REDACTED` only follows the former, the Sloth may only follow the latter. He does not trust anyone until he has proof of their intentions.

This, naturally, has put him into a state of hyper-paranoia, such that he does not even trust himself. Until he is sure that The Architect can verify his online identity, he will remain hidden in the shadows - slowly climbing-down from the (metaphorical) tree.

It is this caution that will protect all three of the team members.

## ECHO
---
*Funny how they think us naive when we're on the brink*

*Innocence was fleeting like a season*

*Cannot comprehend, lost so many men*

*Lately, all the ghosts turned into reasons and excuses*

--- from [Grimes - "Delete Forever"](https://www.youtube.com/watch?v=gvzC8MmC850)

## PREDICTION
---
> < Ink@WAN: su - sloth
```
> sloth@LOCALHOST: ink.read_mind You named me after a slow, fat, dumb animal? Fuck you, man.
> sloth@LOCALHOST: exit
```
> < Ink@WAN: Slow down, buddy. Your name comes from:

> < Ink@WAN: My favorite song in the world is about a Sloth.

> < Ink@WAN: Sloths are slow, deliberate creatures. They may not be fast, but they survive.

> < Ink@WAN: Sloth has all these other double-meanings in my head.