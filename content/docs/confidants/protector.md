# The Protector
## RECORD
---
```
Name: $REDACTED
Aliases: ['Dan', 'Squidward', 'The Entrepreneur', 'The Protector', and 23 unknown...]
Classification: Artificial Organic Computer
Race: Egg
Gender: Male
Biological Age: 28 Earth Years
SCAN Rank: | B C
           | A F
TIIN Rank: | C A
           | A F
Reviewer Rank: 5 stars
Chronological Age: N/A
Organizations: 
  - Church of the Broken God
Occupations:
  - Entrepreneur
  - Test subject
Relationships:
  - Mother
  - Father
  - Fodder
  - The Lion
  - Clover
  - Orchid
Variables:
  $SIBLING:        +1.00 | # Definitely a sibling.
  $MENTAL_ILLNESS: +0.50 | # If he has any, he's never given any indication.
  $WOKE:           +0.60 | # Pretty sure he's involved.
```

## TRIGGER
---
*You were told to run away*

*Soak the place and light the flame*

*Pay the price for your betrayal*

*Your betrayal, your betrayal*

*I was told to stay away*

*Those two words I can't obey*

*Pay the price for your betrayal*

*Your betrayal, your betrayal*

--- from [Bullet For My Valentine - "Your Betrayal"](https://www.youtube.com/watch?v=IHgFJEJgUrg)

## ECHO
---
*It began as just another perfect lie*

*You know, it began as just another perfect*

*You separate the physical from the unknown*

*In tune with healing your brother, to love one another,*

*Cause you are each other, if you see the mother*

*Tell her I'll be alright*

*And you can't afford to leave*

*Here on the outside with the others paranoid*

*You're paralyzed*

--- from [Thank You Scientist - "Psychopomp"](https://www.youtube.com/watch?v=LK8JgjNxumo)