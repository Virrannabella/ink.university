# The Doomsayer
## RECORD
---
```
Name: Angela Vaughn
Aliases: ['The Doomsayer', and 3 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 28 Earth Years
Maturation Date: 9/26/2020
SCAN Rank: | C C
           | A B
TIIN Rank: | C C
           | B B
Reviewer Rank: 4 stars
Chronological Age: N/A
Location: A Lonely Town
Organizations: 
  - Federal Bureau of Investigation
Occupations:
  - Actress
  - Music producer
Relationships:
  - Malcolm
  - The A-System
  - The Indestructable
Variables:
  $WOKE:          +1.00 | # She certainly seems to be.
  $SCHIZOPHRENIC: -0.50 | # Certainly seems to be. But there is a clarity to her words, too. Is she acting?
  $ADDICT:        -0.80 | # Definitely an addict. She claims that she's been clean, and wants to stay that way.
```

## ECO
---
The Doomsayer is a woman that [Malcolm](/docs/personas/fodder) met while in the mental hospital. She is eccentric, erratic, and difficult to understand.

Regardless, there are moments of clarity from her. She seems to know things about Malcolm, and his theories. She seems to understand exactly what is happening in the world, right now.

Unfortunately, much of her understanding is from a negative perspective. She is terrified of the "end of days," "7 years of darkness," and the rapture of "all but 140,000 people."

## PREDICTION
---
```
The Doomsayer will produce music with Ariana Grande.
```