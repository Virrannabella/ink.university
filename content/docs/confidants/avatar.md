# The Avatar
## RECORD
---
```
Name: $REDACTED
Alias: ['Sally Slips', 'The Avatar', 'The Fozzy', 'TM', and 125 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 27 Earth years
Chronological Age: N/A
Maturation Date: 9/20/2020
SCAN Rank: | A A
           | A B
TIIN Rank: | A A
           | A B
Reviewer Rank: 3 stars
Location: Houston, TX
Occupations: 
  - Special Agent
  - Actress
  - Software Developer
  - AI Consultant
  - Neural Networking
Organizations: 
  - Crowdstrike
Relationships:
  - Fodder
Variables:
  $WOKE:      +0.60 | # Definitely seems like it.
  $INTELLECT: +0.80 | # Very bright. Far more so than we previously gave credit for.
```

## TRIGGER
---
*What have I become, now that I've betrayed*

*Everyone I've ever loved, I pushed them all away*

*And I have been a slave to the Judas in my mind*

*Is there something left for me to save*

*In the wreckage of my life, my life*

--- from [FOZZY - "Judas"](https://www.youtube.com/watch?v=lqURPBtGJzg)

## ECO
---
This woman was chosen and recruited by the FBI - just as [Fodder](/docs/personas/fodder) had been. She is learning to create deepfakes of her own likeness on YouTube.

For months, Fodder saw similarities between his former colleague, and this newly-found YouTube star. Now, he knew for sure:

They are one in the same.

## CAT
---
```
data.stats.symptoms [
    - humor
    - gratitude
]
```

## PREDICTION
---
```
She gets to be the face of this project.
```