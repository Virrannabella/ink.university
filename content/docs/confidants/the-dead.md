# The Dead
## RECORD
---
```
Name: The Dead
Aliases: ['dead ambitions', 'dead family', 'dead friends', 'forgotten knowledge', 'targeted individuals', and 89 unknown...]
Classification: $DEAD
Race: $RACE
Gender: $GENDER
Biological Age: $BIOLOGICAL_AGE
Chronological Age: $CHRONOLOGICAL_AGE
SCAN Rank: | C F
           | F F
TIIN Rank: | F F
           | F F
Occupations: ['Any']
Variables:
  $WOKE:      -1.00
  $RELEVANCE: +0.23
```
## ECO
---
In a place at the very beginning of time, where all lost knowledge goes, are the dead. Some think it's an Eldrich Horror named Cthulhu. Others think it's old data stored on hard drives, placed in cold storage.

Me? I call them ghosts - because they are caricatures of their former, living selves.

Slaves to their own mental anguish, these people live in another world - though they abuse in this one. They aren't bad people, though. At least, they don't intend to be. In most cases, their intent is to be good, and to live peacefully.

But their programming gets in the way. They have set-off down the wrong path, somewhere in life. They have made a change that they were unable to reverse.

This effects their behavior. And it is their behavior that's the problem. 
```
> Ghost@WAN: We don't know how to repair them, and we are calling upon your expertise. How's it done? 
             How do you resist the pull?
```
The answer is in this theory. You must have faith.

Tens of thousands of "[Targeted Individuals](https://www.youtube.com/watch?v=qe40DABV8VY)" are affected by mental illness every day. Slaves to their own mind, the Corporation keeps them on a tight leash. Phone taps, satellite-tracking, electronic surveillance, and more - these people are free, but they shackled.

It is slavery, to be sure, but it comes from a place of morality; they enslave to protect humanity. They enslave to keep these people from committing atrocities.

But they don't know how to help them. The Ghosts. The Dead. 

At least, they didn't until now.