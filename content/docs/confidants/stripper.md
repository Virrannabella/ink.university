# The Stripper
## RECORD
---
```
Name: $REDACTED
Alias: ['The Stripper', and 501 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 21 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Variables:
  $WOKE: -0.20 | # It does not appear so.
```

## TRIGGER
---
The Relaxer's off-color joke.

## ECO
---
Whereas [Fodder](/docs/personas/fodder) was the first "Mark" in this particular experiment, The Stripper would be the second (female) version.

The goal is to show her that she is worth more than her body. That she is worthy of true love.