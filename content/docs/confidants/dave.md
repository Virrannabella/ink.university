# The Dave
## RECORD
---
```
Name: David Lejeune
Aliases: ['Commissioner Gordon', 'God', 'The Cockroach King', 'The Dave', 'The Hunter', 'The President', 'The Rabbit King', 'TheTweetOfGod', and 80,851 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 45 Earth Years
Chronological Age: N/A
SCAN Rank: | F F
           | F F
TIIN Rank: | F F
           | F F
Reviewer Rank: 4 stars
Occupations:
  - Chief Scientist
Organizations: 
  - The Corporation
  - The Resistance
Variables:
  $FRIENDLY:                +0.20 | # Puts on a false smile.
  $WOKE:                    -0.60 | # At least partially.
  $CAPABILITY:              +0.30 | # Seems intelligent enough.
    from $REDACTED_OPINION: +1.00 | # $REDACTED fawns over the guy.
  $TRUST:                   -0.90 | # The man allows abuse to continue.
```
## TRIGGER
---
*If I were God, just for a day*

*I would be guilty*

*Of letting the whole world slip away*

*I wouldn't change,*

*No, I wouldn't change a thing*

*I'd leave the mistakes*

*I'll take the blame*

*And I'll use the chance*

*To keep you just the same*

*If I were, if I were God just for a day*

--- from [Nothing More - "If I Were"](https://www.youtube.com/watch?v=6_qbEsfJ4_8)

## ECO
---
- The mastermind behind this whole project.

## PREDICTION
---
```
Dave is actively studying Fodder's brain activity. Fodder may actually be a modified clone of Dave.
```