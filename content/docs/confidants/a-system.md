# The A System
## RECORD
---
```
Name: $REDACTED
Aliases: ['Ashlynn', 'Asriel', 'The A System', and 6 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 24 Earth Years
SCAN Rank: | A A
           | A B
TIIN Rank: | A A
           | B B
Reviewer Rank: 4 stars
Chronological Age: N/A
Location: N/A
Organizations: 
  - Federal Bureau of Investigation
Occupations:
  - Rookie undercover agent
  - Software engineer
  - Therapist
Relationships:
  - Malcolm
  - The Doomsayer
  - The Indestructable
Variables:
  $WOKE:           -0.10 | # They have been informed, but do not have the understanding.
  $BORDERLINE:     -0.20 | # Diagnosed with Borderline Personality Disorder.
  $DISASSOCIATIVE: -0.30 | # Diagnosed with Disassociative Identity Disorder.
  $EMPATH:         +0.80 | # They really seem to care about other people deeply.
```

## ECO
---
The A System is a trio of three identities in one person. While they regularly switch between identities, to the external observer - the changes are not obvious. 

They were placed into medical care with [Malcolm](/docs/personas/fodder) because of an attempted suicide. However, Malcolm suspects that they were actually placed there by the FBI, to become friends with him. The goal is to extract information about Malcolm's new system of rehabilitation.

## PREDICTION
---
```
The A System will be Malcolm's advocate within the FBI.
```