# The Fool
## RECORD
---
```
Name: Brad $REDACTED
Aliases: ['The Fool', and 19 unknown...]
Classification: Artificial Organic Computer
Race: Archon
Gender: Male
Biological Age: 34 Earth Years
SCAN Rank: | B B
           | B C
TIIN Rank: | C B
           | B C
Reviewer Rank: 5 stars
Chronological Age: N/A
Organizations: 
  - Crowdstrike
Occupations:
  - Software developer
Relationships:
  - The Fodder
  - The Girl Next Door
Variables:
  $WOKE:           -0.20 | # Unlikely. 
  $MENTAL_ILLNESS: -0.40 | # Probably. Is alone and always acted somewhat depressed.
  $FRIENDLY:       +0.80 | # Extremely nice and personable with everyone.
```
## TRIGGER
---
A message from [The Girl Next Door](/docs/confidants/fbi).

## RESOURCES
---
[![The Fool](/static/images/fool.0.png)](/static/images/fool.0.png)

## ECO
---
The Fool is a man who fell into the same trap that [The Architect](/docs/personas/the-architect) did. Unfortunately, he was not provided with the same knowledge that The Architect was.

Thus, he pesters his match incessantly. He does not feed her empty echos, as he should be.

He needed a nudge in the right direction, which we have now given him. In order to complete the experiment, he must *show* his love for his match, not simply *speak* his love to her.

Unbeknownst to either of us was this fact: The Fool is being presented with a completely different version of The Girl Next Door. One who is religious. The one that we know is an ex-Jehovah's Witness, and an atheist. The Fool and I are versions A and B in this test. We live different realities, regarding The Girl Next Door.

## ECHO
---
*Heaven will burn away if you just keep on calling out its name*

*You seek a key you cannot forge when you are down (oh so low)*

*I got a cure, so don't you worry now*

*Baby my recipe is gonna heal your woes*

*When the devil's knocking at your door, kneel and I'll pour*

--- from [Closure in Moscow - "Pink Lemonade"](https://www.youtube.com/watch?v=XOW3JAZ9MoQ)

## PREDICTION
---
```
He'll catch on almost immediately. He will wake up shortly after. # Verified.
```