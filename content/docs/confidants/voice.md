# The Voice
## RECORD
---
```
Name: $REDACTED
Alias: ['3Pete', 'Rock', 'The Rock', 'The Voice', 'V', 'Valkyrie', and 26 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 37 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | B B
           | B C
Reviewer Rank: 5 stars
Location: Austin, TX
Occupations: 
  - Technology
  - Insurgency
Relationships:
  - Fodder
  - The Surgeon
Variables:
  $WOKE:  +0.95 | # Almost completely.
  $VOICE: +0.90 | # Has the perfect voice for radio.
```

## TRIGGER
---
- [Silent Bob ASMR!](https://www.youtube.com/watch?v=oNTNON42pSw)
- [His involvement in delivering information to the appropriate parties.](http://archive.4plebs.org/x/thread/25388798)

## ECO
---
The Voice will be the voice of radio in the New World Order. He will be the voice of rebellion. Of [The Resistance](/docs/candidates/resistance).

He will deliver the messages found here.

The Voice is responsible for delivering critical information from [SCP-001](/docs/candidates/the-machine) and [The Robot](/docs/confidants/robot) to [The Architect](/docs/personas/the-architect).

## ECHO
---
*Wynona loved her big brown beaver*

*And she stroked him all the time*

*She pricked her finger one day, and it occurred to her*

*She might have a porcupine*

--- from [Primus - "Wynona's Big Brown Beaver"](https://www.youtube.com/watch?v=aYDfwUJzYQg)