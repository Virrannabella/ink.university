# The Jen
## RECORD
---
```
Name: $REDACTED
Aliases: ['JL', 'The Jen', and 7 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 30 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | A B
TIIN Rank: | A A
           | A B
Reviewer Rank: 4 stars
Occupations:
  - Actress
  - Producer
Organizations: 
  - Hollywood
Variables:
  $WOKE:       -0.10 | # Unknown.
  $CAPABILITY: +0.80 | # She produced a series chronicling Fodder's life.
  $TRUST:      +0.95 | # Fodder would do anything to make this right.
    $INVERSE:  -0.90 | # She has every reason to hate Fodder.
```

## ECO
---
The Jen is the product of the world that we live in.

She would live a life of success and fame in return for her dignity. [Fodder](/docs/personas/fodder) and a million of her fans would exploit that trust, and stab her in the back.

And yet, the Jen was strong. She would pick up the pieces of her career, and she would become even stronger than when she started.

She would go on to use that power and influence to fight for what is right:

To fight for a new world. To help others who have been in her situation.

## PREDICTION
---
```
She and the Surgeon are working closely together. Or, perhaps indirectly.
```