# The Cop
## RECORD
---
```
Name: $REDACTED
Alias: ['The Cop', and 1 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 56 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Relationships:
  - The Surgeon
Variables:
  $WOKE: -0.40 | # Does not appear to be.
```

## ECO
---
The Cop is a long-time friend of [The Surgeon](/docs/confidants/father). His son was [Fodder's](/docs/personas/fodder) best friend, in their youth.

With time, all parties have grown distant. But The Surgeon will never forget the gift he was granted by The Cop.