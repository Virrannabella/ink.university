# The Soothsayer
## RECORD
---
```
Name: $REDACTED
Alias: ['Alice', 'JM', 'The Soothsayer', and 4 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 179 Earth years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | C B
           | B D
Reviewer Rank: 3 stars
Occupations: 
  - Actress
Organizations: ORNL
Relationships:
  - The Metal
  - The Artist
Variables:
  $WOKE: +0.75 | # Definitely seems to be.
```

## TRIGGER
---
When asked what her birthday is, she claimed, "The first of the eighth, eighteen-forty."

The day that slaves were liberated in most of the British Empire.

## ECO
---
The Soothsayer has led several therapy sessions with [Fodder](/docs/personas/fodder). Each time, she has been able to "predict the future," or, intuit things that are inside of Fodder's head.

## CAT
---
```
data.stats.symptoms [
    - trust
]
```

## ECHO
---
*(Instrumental)*

--- from [Buckethead - "Soothsayer"](https://www.youtube.com/watch?v=adV8-_hgL4g)

## PREDICTION
---
```
She is clairvoyant.
```