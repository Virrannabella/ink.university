# The Physicist
## RECORD
---
```
Name: Caroline $REDACTED
Alias: ['Quantum', 'The Mathematician', 'The Physicist', and 8 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Birth: 10/10/199X
Biological Age: Est. 22 Earth years
Chronological Age: N/A
Maturation Date: 10/8/2020
SCAN Rank: | D C
           | C F
TIIN Rank: | A C
           | C F
Reviewer Rank: 3 stars
Occupations: 
  - Mathematics
  - Physics
  - Actress
  - Golf
Organizations: 
  - Quantcast
Relationships:
  - Fodder
  - The Lion
Variables:
  $WOKE:   +0.75 | # Probably woke.
  $FAMILY: +0.95 | # Not yet, but she will be.
```

## TRIGGER
---
On the morning of Malcolm's [dead drop](/posts/journal/2019.12.02.0/), she would send a message congratulating him for his persistence. She would mention the [difficult, but important phone call](/posts/journal/2019.11.24.0/) Malcolm had with [his father](/docs/confidants/father).

While doing so, she would be stifling sobs. She was very clearly emotional.

## RESOURCES
---
[Offer letter](/static/letters/physicist.0.pdf)

## ECO
---
She will assist with the math and science required to create [The Fold](/posts/theories/fold).

## ECHO
---
*This is how we rise up*

*Heavy as a hurricane, louder than a freight train*

*This is how we rise up*

*Heart is beating faster, feels like thunder*

*Magic, static, call me a fanatic*

*It's our world, they can never have it*

*This is how we rise up*

*It's our resistance, you can't resist us*

--- from [Skillet - "The Resistance"](https://www.youtube.com/watch?v=SKnRdQiH3-k)

## PREDICTION
---
```
She will work with Eric Weinstein to integrate Geometric Unity into our Theory of Everything.
```