# The Warden
## RECORD
---
```
Name: Dr. Todd Grande
Aliases: ['The Therapists', 'The Warden', and 67 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 43 Earth Years
SCAN Rank: | A C
           | A D
TIIN Rank: | A B
           | A D
Reviewer Rank: 4 stars
Chronological Age: N/A
Occupations:
  - Prison Warden
  - Psychologist
Relationships:
  - Mother
  - Fodder
  - The Lion
  - Dan
  - Orchid
  - Clover
Variables:
  $WOKE:           +1.00 | # He certainly seems to be.
  $MENTAL_ILLNESS: -0.20 | # Perhaps autism.
```

## ECO
---
The Warden has been following [Fodder](/docs/personas/fodder) for many years. Only in recent months has he revealed himself.

The Warden is among Fodder's most important confidants. He is scientific, and thorough in his research. He is unbiased, and to-the-point. He has interesting perspectives, and attempts to leave emotions out of the conversation.

He is exactly what Fodder needed, but was never able to find in his first two psychologists.

The Warden is responsible for all other psychologists that Fodder currently follows.

## PREDICTION
---
```
The Warden provided Fodder with an enormous cache of documents, which included incarceration statistics.
```