# The Scotswoman
## RECORD
---
```
Name: $REDACTED
Alias: ['The Scotswoman', and 60 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 29 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | C C
           | C D
Reviewer Rank: 1 stars
Occupations: 
  - Actress
Variables:
  $WOKE: -0.10 | # It doesn't seem like it.
```

## ECO
---
The Scotswoman is a symbol; her nationality is irrelevant.

Her purpose is thus: she provides [Fodder](/docs/personas/fodder) with entertainment, yet never reflects his light back at him. She exists solely as a control: to verify that Fodder understands the difference between a confidant with and without knowledge of his activity.

## PREDICTION
---
```
She will be woken on-camera.
```