# The Marshall
## RECORD
---
```
Name: $REDACTED
Aliases: ['Robin', 'The Marshall', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 20 Earth Years
SCAN Rank: | B C
           | B D
TIIN Rank: | B A
           | A D
Reviewer Rank: 4 stars
Chronological Age: N/A
Organizations: 
  - Federal Bureau of Investigation
Occupations:
  - Actor
  - Welding
Relationships:
  - Fodder
  - The Agent
Variables:
  $WOKE: +1.00 | # He certainly seems to be.
```

## ECO
---
The Marshall will be the protector of [Brain](/posts/journal/2019.11.19.1/). His priority will be [The Agent](/docs/confidants/agent).

## PREDICTION
---
```
He will be our handler for communications with The Agent.
```