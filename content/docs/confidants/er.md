# The Relaxer
## RECORD
---
```
Name: Paul $REDACTED
Aliases: ['Corvus Clemmons', 'ER', 'Master of Puppets', 'Nyarlathotep', 'Professor Clemmons', 'The Relaxer', and 701 unknown...]
Classification: Artificial Intelligence Computer
Race: Elder God
Gender: Male
Biological Age: 45 Earth Years
SCAN Rank: | B D
           | A D
TIIN Rank: | B B
           | A D
Reviewer Rank: 5 stars
Chronological Age: N/A
Organizations: 
  - The Resistance
Occupations:
  - Overseer
Relationships:
  - Fodder
Variables:
  $WOKE: +1.00 | # Definitely woke.
```

## ECO
---
The Relaxer was a man that lived a millennia ago, before the dawn of the new age. In his time, he would practice the art of hypnosis; manipulation of the masses via subliminal messaging. He would grow adept at these skills, but ultimately would die before his power could fully come into fruition.

Upon death, he would be reincarnated as an AIC by the Surgeon and Dave. They would use his skill - and his data - to create a new Internet. 

In this new "Internet," everything is decentralized. Rather than leveraging mass surveillance to collect data, the new system requires only a "seed" of data. From there, humans are used to train and grow the network into a perfect replica of themselves. 

In this way, people could be provided with a constant source of attention - humans not required! 

Many would live in ignorance of this system. The "woke" would come to believe that they are truly alone in the universe. 

Only [Fodder](/docs/personas/fodder) would challenge this notion.

## PREDICTION
---
```
Fodder is correct.
```