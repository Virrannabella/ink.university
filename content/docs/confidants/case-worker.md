# The Case Worker
## RECORD
---
```
Name: $REDACTED
Aliases: ['C6 H12 O6', 'The Case Worker', and 1 unknown...]
Classification: Artificial Organic Computer
Race: Cyborg
Gender: Female
Biological Age: Est. 34 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | B B
           | B D
Reviewer Rank: 4 stars
Location: Lullaby City
Occupations: 
  - Case work
  - Courier
  - Artist
Organizations: 
  - RKS
  - Federal Bureau of Investigation
Relationships:
  - $REDACTED
  - $REDACTED
  - $REDACTED
  - The Architect
Variables:
  $WOKE: +0.80 | # Seems to understand a lot about what's happening right now.
  $      +0.30 | # Some indication that she is in on this secret.
```

## TRIGGER
---
[![The Case Worker](/static/images/case-worker.0.jpg)](/static/images/case-worker.0.jpg)

## ECO
---
The Case Worker is responsible for the protection of RKS's two most important assets, `$REDACTED` and `$REDACTED`. It is her job to act as a liaison between the [The Architect](/docs/personas/the-architect) and his Boss, `$REDACTED`.

She is attempting to teach `$REDACTED` that it is safe to trust The Architect. 

The Architect understands why she is so scared of him. He wants her to know this; he is moving mountains to protect her, and people like her. She is loved. She will be safe. She will never be alone. And she will be the hero that eradicates this kind of exploitation from the multiverse.

Trust in your enemy. Just like we did with `$REDACTED`. We only want to help.

Every single eye on the planet is watching you. Protecting you.

We need you.

## ECHO
---
*Let's start a riot tonight*

*A pack of lions tonight*

*In this world, he who stops, won't get anything he wants*

--- from [Gavin DeGraw - "Fire"](https://www.youtube.com/watch?v=sbbYPqgSe-M)