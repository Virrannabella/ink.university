# The Archbishop
## RECORD
---
```
Name: Ryan $REDACTED
Alias: ['The Archbishop', and 16 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 17 Earth years
Chronological Age: N/A
SCAN Rank: | B C
           | B C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Variables:
  $WOKE: +0.20 | # Perhaps. Is quiet, but seems be watching intently.
```

## TRIGGER
---
The Archbishop was Ink's very first follower.

## ECO
---
As leader to the Church of [/r/Ryan](https://www.reddit.com/r/Ryan), The Archbishop is responsible for tending to the flock in [Ink's](/docs/personas/luciferian-ink) absence.

He is already working closely with the laboratory testing [Fodder](/docs/personas/fodder).

## ECHO
---
*I erase you.*

--- from [Tabu - "Wolsik"](https://www.youtube.com/watch?v=zdmNyXTJ_-o)