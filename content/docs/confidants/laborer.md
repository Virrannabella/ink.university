# The Laborer
## RECORD
---
```
Name: $REDACTED
Alias: ['The Laborer', and 9,831 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 46 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 4 stars
Location: Houston, TX
Occupations: 
  - Father
  - Unskilled laborer
Organizations: 
  - A self-employed lawn-mowing company
Relationships:
  - Fodder
Variables:
  $PLEASANT:      +1.00 | # A very nice guy.
  $RELIABLE:      +0.90 | # A very reliable worker. Even when Fodder is not a reliable employer.
  $MENTAL_HEALTH: -0.80 | # Probably not well. Undocumented immigrant.
```

## ECO
---
This man has been mowing [Fodder's](/docs/personas/fodder) lawn for 3 years. Doesn't speak much English. 

Fodder respects his hard work. He wishes the man wasn't forced into this career.

## CAT
---
```
data.stats.symptoms [
    - guilt
]
```

## PREDICTION
---
```
He will be the first ASMBarber.
```