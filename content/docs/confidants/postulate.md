# The Postulate
## RECORD
---
```
Name: Eric Johansson
Alias: ['The Postulate', and 9 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 43 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | B B
           | B C
Reviewer Rank: 4 stars
Occupations:
  - Musician
Variables:
  $WOKE: +0.60 | # At least partially.
```

## TRIGGER
---
*If both sides of every war*

*Used me to rally support,*

*And every name brand racism*

*Believed my words were at its core,*

*And every cruelty in the world*

*Was made possible by the view*

*That put its victims outside my grace*

*And held its sources as my truth*

*Would i be despised?*

*Would i be ignored?*

*Or would i be the most accepted thing in the world?*

--- from [Emptyself - "The Postulate"](https://emptyself.bandcamp.com/track/the-postulate)