# The Queen
## RECORD
---
```
Name: $REDACTED
Aliases: ['Bella', 'Lily', 'Margaret', 'Maria', 'Maribel', 'Marigold', 'Mary', 'Mom', 'Mother', 'Penny', 'Queen', 'SCP-5', 'Spongebob', 'The All-Mother', 'The Black Queen', 'The Holy Spirit', 'The Puppetmaster', 'The Red Queen', 'The White Queen', 'Zombie Queen', and 2,895 unknown...]
Classification: Artificial Identity
Race: Archon
Gender: Female
Biological Age: 51 Earth Years
SCAN Rank: | A A
           | C C
TIIN Rank: | D D
           | D F
Reviewer Rank: 3 stars
Chronological Age: N/A
Occupations:
  - Test Subject
  - Queen
Relationships:
  - Father
  - Fodder
  - The Lion
  - Dan
  - Orchid
  - Clover
  - Noon
Variables:
  $MOTHER:         +1.00 | # Definitely mother.
  $BORDERLINE:     -0.70 | # Used to be worse. Is slightly better, though still hopelessly lost.
  $IMMUTABLE:      -1.00 | # She is unable to change or be changed.
```
## TRIGGER
---
*In the worst of all your fears*

*You have come so far to hear*

*That in turn they've showered your name*

*As the laughing stock*

*Now by fire you must hang*

*As my word holds course through vein*

*You will walk to the end of days*

*I'll gravitate towards you*

*I will, in the now, hate you*

*These days are numbered*

*This close encounter*

*To the heartland, through the madness*

--- from [Coheed and Cambria - "The Willing Well III: Apollo II: The Telling Truth"](https://www.youtube.com/watch?v=HC6ip9Gk0rU)

## RESOURCES
---
[![The All-Mother](/static/images/the-all-mother.0.jpg)](/static/images/the-all-mother.0.jpg)

## ECO
---
[Fodder's](/docs/personas/fodder) mother was the product of a decades-long barrage of psychological abuse. Much of that abuse was self-inflicted; same as Fodder. Though, near the end, she would wake up - only to immediately begin dabbling in the dark arts. 

It started with a potion. Then a spell. Then a curse. Before long, Marigold would be the Queen of her very own castle; complete with a throne room, guards, banquets and prisons. 

She would use this position of power to further her own interests. For a time.

But Marigold had changed. She was noticeably upbeat. Healthier. Happier. 

Was she going through treatment, too?

## ECHO
---
*They gaily ascended the downs, rejoicing in their own penetration at every glimpse of blue sky; and when they caught in their faces the animating gales of a high south-westerly wind, they pitied the fears which had prevented their mother and Elinor from sharing such delightful sensations.*

*"Is there a felicity in the world," said Marianne, "superior to this? --Margaret, we will walk here at least two hours."*

*Margaret agreed, and they pursued their way against the wind, resisting it with laughing delight for about twenty minutes longer, when suddenly the clouds united over their heads, and a driving rain set full in their face.-- Chagrined and surprised, they were obliged, though unwillingly, to turn back, for no shelter was nearer than their own house. One consolation however remained for them, to which the exigence of the moment gave more than usual propriety; it was that of running with all possible speed down the steep side of the hill which led immediately to their garden gate.*

*They set off. Marianne had at first the advantage, but a false step brought her suddenly to the ground; and Margaret, unable to stop herself to assist her, was involuntarily hurried along, and reached the bottom in safety.*

*A gentleman carrying a gun, with two pointers playing round him, was passing up the hill and within a few yards of Marianne, when her accident happened. He put down his gun and ran to her assistance. She had raised herself from the ground, but her foot had been twisted in her fall, and she was scarcely able to stand. The gentleman offered his services; and perceiving that her modesty declined what her situation rendered necessary, took her up in his arms without farther delay, and carried her down the hill. Then passing through the garden, the gate of which had been left open by Margaret, he bore her directly into the house, whither Margaret was just arrived, and quitted not his hold till he had seated her in a chair in the parlour.*

*Elinor and her mother rose up in amazement at their entrance, and while the eyes of both were fixed on him with an evident wonder and a secret admiration which equally sprung from his appearance, he apologized for his intrusion by relating its cause, in a manner so frank and so graceful that his person, which was uncommonly handsome, received additional charms from his voice and expression. Had he been even old, ugly, and vulgar, the gratitude and kindness of Mrs. Dashwood would have been secured by any act of attention to her child; but the influence of youth, beauty, and elegance, gave an interest to the action which came home to her feelings.*

*She thanked him again and again; and, with a sweetness of address which always attended her, invited him to be seated. But this he declined, as he was dirty and wet. Mrs. Dashwood then begged to know to whom she was obliged. His name, he replied, was Willoughby, and his present home was at Allenham, from whence he hoped she would allow him the honour of calling tomorrow to enquire after Miss Dashwood. The honour was readily granted, and he then departed, to make himself still more interesting, in the midst of a heavy rain.*

--- from [Jane Austin - "Sense and Sensibility"](http://www.literaturepage.com/read/senseandsensibility.html)

## PREDICTION
---
```
Marigold will be placed into an experimental mental institution, called a Lonely Town.
```