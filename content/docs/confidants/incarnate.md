# The Incarnate
## RECORD
---
```
Name: Jeffrey Epstein
Aliases: ['Fixer', and 2 unknown...]
Classification: Incarnate
Race: Human
Gender: Male
Biological Age: 67
Chronological Age: 2,584,000 Earth years
SCAN Rank: | C F
           | D F
TIIN Rank: | F F
           | F F
Reviewer Rank: 1 stars
Occupations: 
  - Lawyer
  - President
  - Human Trafficking
Organizations: 
  - The Corporation
Variables:
  $WOKE:      +1.00 | # Definitely woke.
  $DEPRAVITY: -1.00 | # Definitely depraved.
```
## ECO
---
Jeffrey is the most morally-bankrupt, corrupt, and evil man to have ever walked the planet. His apathy towards humanity knows no bounds. His abuse of women and children is second to none. His ability to persuade, escaping justice time and again, is unmatched. Jeffrey is the single most powerful man on Earth. The single most feared. The most dangerous.

At least, in [Fodder's](/docs/personas/fodder) mind, he is. Reality is often more fuzzy.

And now it doesn't matter. Jeffrey was murdered in his sleep. Only his story, and the story of one thousand accusers lives on.

### About Incarnates
Incarnates are the mind's representation of its own worst thoughts, feelings, and behaviors. Looking upon an Incarnate typically causes feelings of revulsion and angst within the beholder. 

Despite that, it is important to note that your Incarnate may be biased in the negative direction. How an individual represents another in his/her mind very rarely matches the person's own beliefs about themselves, in reality. Your Incarnate is the worst, least-charitable interpretation of an identity that your mind is able to conceive of.

## PREDICTION
---
```
Jeffrey's story will see an ending. One that will end human exploitation forever.
```
