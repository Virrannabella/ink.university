# The Teacher
## RECORD
---
```
Name: $REDACTED
Alias: ['The Teacher', and 251 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 26 Earth years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | A A
           | A B
Reviewer Rank: 4 stars
Occupations: 
  - Special Agent
  - Teacher
Organizations: 
  - Federal Bureau of Investigation
Relationships:
  - Fodder
  - The Strongman
Variables:
  $WOKE:    +0.40 | # Unsure. Maybe.
  $EMPATHY: +0.90 | # Loved by everyone he meets.
  $WEEB:    +1.00 | # Certified.
```

## ECO
---
The Teacher is a former employee of [Fodder's](/docs/personas/fodder). Fodder did not treat him very well.

Regardless, he taught Fodder important life lessons. Fodder wishes to repay this debt.

## CAT
---
```
data.stats.symptoms [
    - gratitude
]
```

## ECHO
---
*(instrumental)*

--- from [David Maxim Micic - "500 Seconds Before Sunset"](https://www.youtube.com/watch?v=liHLFX_L-44)

## PREDICTION
---
```
The Teacher will be the first to use The Fold in a classroom setting.
```