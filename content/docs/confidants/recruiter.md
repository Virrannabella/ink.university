# The Recruiter
## RECORD
---
```
Name: $REDACTED
Alias: ['QC', 'The Recruiter', and 36 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 28 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | B B
           | B C
Reviewer Rank: 3 stars
Occupations: 
  - Actress
  - Recruiter
Organizations: 
  - The Bureau of the Occult
Relationships:
  - Fodder
Variables:
  $WOKE: +0.80 | # Almost certainly.
```

## TRIGGER
---
*(instrumental)*

--- from [Caligula's Horse - "Calliope's Son"](https://www.youtube.com/watch?v=_5W_qVfOm2Y)

## ECO
---
The Recruiter is responsible for extracting [Fodder](/docs/personas/fodder) from his place of origin, and seeing him to safety. 

She has already communicated her plan. The two simply wait until the timing is right.

She is extremely protective of her assets, even going so far as to call them her "children."