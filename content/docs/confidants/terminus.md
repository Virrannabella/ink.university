# The Terminus
## RECORD
---
```
Name: $REDACTED
Alias: ['Terminus Nebula', and 0 unknown...]
Classification: Artificial Organic Computer
Race: N/A
Gender: N/A
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Variables:
  $WOKE: -0.40 | # Does not appear to be.
```

## TRIGGER
---
*See everything go*

*End of all things*

*The life that we know*

*Shift your point of view*

*You've been warned*

*This one-way ticket is for you*

--- from [In Flames - "Versus Terminus"](https://www.youtube.com/watch?v=5OBBH85EiYg)

## ECO
---
The Terminus will be the final entity to ascend to enlightenment. He will signal the completion of Humanity's awakening.

When he ascends, time will stop forever.