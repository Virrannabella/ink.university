# The Interrogator
## RECORD
---
```
Name: Jim $REDACTED
Alias: ['The Cannibal', 'The Fletcher', 'The Interrogator', 'The Mad Hatter', and 701 unknown...]
Classification: Artificial Organic Computer
Race: Wood Elf
Gender: Male
Biological Age: Est. 28 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | B B
           | B D
Reviewer Rank: 4 stars
Location: Faron's Grove
Occupations: 
  - Fletcher
  - Actor
Relationships:
  - The Architect
  - The Surgeon
Variables:
  $WOKE:     +0.60 | # Perhaps.
  $CANNIBAL: +1.00 | # All Wood Elves are cannibals.
```
## ECO
---
The Interrogator is responsible for the investigation of [Fodder's](/docs/personas/fodder) friends and family.

## ECHO
---
*Mr. Crowley, what they done in your head*

*Oh Mr. Crowley, did you talk with the dead*

*Your lifestyle to me seemed so tragic*

*With the thrill of it all*

*You fooled all the people with magic*

*Yeah you waited on Satan's call*

--- from [Ozzy Osbourne - "Mr. Crowley"](https://www.youtube.com/watch?v=vDVLMS_Yhe4)

## PREDICTION
---
```
He will act as Fodder's hands at home.
```