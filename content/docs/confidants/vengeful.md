# The Vengeful
## RECORD
---
```
Name: David Draiman
Alias: ['The Vengeful', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 47 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Organizations: 
  - Disturbed
Occupations: 
  - Musician
  - Singer
Variables:
  $WOKE:  +0.70 | # Almost certainly. His lyrics are insightful.
  $VOICE: +0.90 | # Has the perfect voice for rebellion.
```

## TRIGGER
---
*I'm the hand of God*

*I'm the dark messiah*

*I'm the vengeful one*

*(Look inside and see what you're becoming)*

*In the blackest moment of a dying world*

*What have you become*

*(Look inside and see what you're becoming)*

--- from [Disturbed - "The Vengeful One"](https://www.youtube.com/watch?v=8nW-IPrzM1g)