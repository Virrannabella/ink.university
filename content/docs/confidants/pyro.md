# The Pyro
## RECORD
---
```
Name: $REDACTED
Aliases: ['The Hype Man', 'The Pyro', and 1 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 20 Earth Years
SCAN Rank: | B B
           | A D
TIIN Rank: | D D
           | B D
Reviewer Rank: 4 stars
Chronological Age: N/A
Location: N/A
Organizations: 
  - Federal Bureau of Investigation
Occupations:
  - Undercover agent
Relationships:
  - Malcolm
  - The Doomsayer
Variables:
  $WOKE:       -0.10 | # He's been informed of a lot, but he doesn't have the understanding.
  $NARCISSIST: -0.50 | # Certainly seems to be.
  $VICTIM:     -0.80 | # Was deeply abused as a child. Does not trust.
```

## TRIGGER
---
[Malcolm's](/docs/personas/fodder) very first interaction with the Pyro. He entered the room, sat across the table from Malcolm, and stated:

"I'll bet your favorite movie is Fight Club."

He was right.

## ECO
---
The Pyro is an undercover agent placed into the mental health facility with Malcolm. His sole purpose is to set fires, just to observe how Malcolm puts them out.

He was placed there to obtain a confession from Malcolm.

## PREDICTION
---
```
The Pyro will also be the Hype Man for a nationwide musical tour by bus.
```