# The Indestructable
## RECORD
---
```
Name: $REDACTED
Aliases: ['The Indestructable', and 3 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 34 Earth Years
SCAN Rank: | C C
           | C C
TIIN Rank: | D D
           | D C
Reviewer Rank: 3 stars
Chronological Age: N/A
Organizations: 
  - Federal Bureau of Investigation
Occupations:
  - Test Subject
  - Undercover agent
Relationships:
  - Malcolm
  - The Doomsayer
  - The A-System
  - The Fozzy
Variables:
  $WOKE:       +0.80 | # Definitely seems to be.
  $BORDERLINE: -0.20 | # Has been diagnosed with Borderline Personality Disorder.
  $CPTSD:      -0.80 | # Is the survivor of horrific trauma.
```

## ECO
---
The Indestructable is the survivor of horrific, repeated traumas. No matter what has happened to her, the world cannot keep her down. She is tough, abrasive, and takes no shit from anyone.

[Malcolm](/docs/personas/fodder) suspects that she is working with the FBI.

## ECHO
---
*I'll have you know*

*That I've become*

*Indestructible*

*Determination that is incorruptible*

*From the other side*

*A terror to behold*

*Annihilation will be unavoidable*

*Every broken enemy will know*

*That their opponent had to be invincible*

*Take a last look around while you're alive*

*I'm an indestructible master of war*

--- from [Disturbed - "Indestructable"](https://www.youtube.com/watch?v=aWxBrI0g1kE)

## PREDICTION
---
```
Is related to/working with the Fozzy.
```