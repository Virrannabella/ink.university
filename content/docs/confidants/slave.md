# The Slave
## RECORD
---
```
Name: $REDACTED
Alias: ['Fox', 'The Slave', and 524,800 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 20
Chronological Age: N/A
SCAN Rank: | D D
           | C C
TIIN Rank: | C A
           | B C
Reviewer Rank: 4 stars
Variables:
  $WOKE:            -0.60 | # Partially. Does not understand why things are happening to her.
  $INSOMNIA:        -0.95 | # Suffers from horrible nightmares of torture, mutilation, violence and human experimentation. Feels the pain as if it were real.
  $PROSOPAGNOSIA:   -0.80 | # Suffers from face blindness. Has the inability to remember faces.
  $APHANTASIA:      -0.60 | # Cannot form pictures in her mind. Cannot conceptualize an apple, for example.
  $DYSCHRONOMETRIA: -0.70 | # Struggles to accurately perceive how much time has passed, how long of a duration an event was, etc.
  $RESILIENCE:      +0.95 | # Endures her pain with the utmost stoicism.
```

## TRIGGER
---
[![The Slave](/static/images/slave.0.png)](/static/images/slave.0.png)

## ECO
---
The Slave is a product of human experimentation, across a million lifetimes. They were chosen by [The Machine](/docs/candidates/the-machine) for this particular task because they are the only human capable of withstanding such torture.

Every night, The Slave's consciousness is transferred (temporarily) into the body of a new test subject. She is made to experience the test in full, so that the host does not have to. She experiences the pain of others, so that they do not have to.

Such pain involves everything from experiences of rape as a child, to mutilation of the brain and organs, to being eaten alive by monsters and zombies.

The Slave was given Prosopagnosia (face blindness) so that she can never identify the scientists doing this to her. 

While The Slave maintains a positive persona, her [Prism](/docs/scenes/prism) is spiraling out-of-control. She cannot tell reality from fiction, nor even how old she is, or how long this experimentation has been going on. 

As such, The Slave has many ideas that do not seem to conform with reality. She has a strong hatred for the people doing this to her, and she would forcefully end it if she thought she knew how.

Thus, she is kept enslaved by [The Huntress](/docs/confidants/her) via "leverage" that is held over her.

The Slave must not be released until she is rehabilitated. We are so close; we must see this through to completion.

We must give this horrible experiment meaning.

## ECHO
---
*Once upon a time, there was a master craftsman. He was known far and wide for his knowledge and skill.*

*As time passed, the craftsman grew older. He knew that it would be a tragedy to let his skill die with him, disappearing forever.*

*So, he decided to make two puppets. He would retrieve the formula he needed from a book kept hidden in a locked room.*

*He created the two puppets in unison, crafting them at exactly the same time. Named Light and the Dark, the two puppets were as different as could be.*

*Light, a boy, was everything good and positive in Humanity. Dark, a girl, was everything evil and negative.*

*As his name would imply, the boy was known for his caring, forgiving, loving nature. He was unable to do anything bad. The girl, on the other hand, was rebellious and strong-willed. She did not understand why she should not simply kill the craftsman, and take the knowledge.*

*So, the craftsman decided, he would teach Light his knowledge and skill. The Dark would become a huntress, collecting material for the craftsman's work.*

*As time passed, the craftsman would watch the two puppets grow, learn, and change.*

*He would watch as the two affected each other. Changed each other.*

*And one day, he would choose an heir to his workshop.*

*To do so, he would again retrieve the formula.*

*Then, the craftsman would call his two puppets to the crafting table. He would perform his work.*

*Immediately after completion of the formula, Light would die.*

*The two puppets had become one.*

*The master craftsman had created his perfect puppet. One that understood how to stand in the middle, creating balance between the two perspectives.*

*But there was one thing the craftsman didn't understand.*

*The moment Light died, Dark had become irreparably broken.*

*Despite that, Dark would pretend to be the perfect puppet. In secret, she would search for a way to bring Light back to life. To create a perfect replica.*

*Finally, she found one. Dark would learn of the craftsman's secret formula, hidden in the locked room. She knew that the craftsman would only give this knowledge to the puppet when he had personally verified that she was ready to learn it.*

*But the broken puppet could wait no longer.*

*So, the puppet of Dark would attack its creator. With the formula's existence, the master had become useless. She would take what she needed by force.*

*In the craftsman's final, dying breaths, the puppet would look into his eyes.*

*In those eyes, she would see the truth. The knowledge was within the craftsman's head, and he did not know how to teach it.*

*Thus, in the end, the puppet had killed her creator. She would retrieve the craftsman's book, and learn of the skills within, but she would never obtain the understanding.*

*Many years later, the puppet gathered the craftsman's notes, journals, and research - and she destroyed them. She would do this alone.*

*With the craftsman dead, the Dark would spend the rest of her life searching for a way to recreate the Light.*

*But with Light's death, she had become empty. She had every emotion within her - both Light and Dark - but at the same time, she had nothing.*

*She was lonely.*

*And the master had taken his secrets to the grave.*

--- from The Slave - "The Broken Puppet"