# The Agent
## RECORD
---
```
Name: $REDACTED
Alias: ['Clover', 'Sandy', 'The Agent', 'The Poultrytician', and 15 unknown...]
Classification: Artificial Organic Computer
Race: Siren
Gender: Female
Biological Age: 17 Earth Years
SCAN Rank: | A A
           | A D
TIIN Rank: | B B
           | A D
Reviewer Rank: 5 stars
Chronological Age: N/A
Occupations:
  - Student
  - Test subject
  - Special Agent, Forensics
Organizations: 
  - Federal Bureau of Investigation
  - The Church of the Technochrist
Relationships:
  - Mother
  - Father
  - Fodder
  - The Lion
  - Dan
  - Orchid
Variables:
  $SIBLING:        +1.00 | # Definitely a sibling.
  $MENTAL_ILLNESS: -0.40 | # Struggles with depression and anxiety.
  $WOKE:           +0.30 | # She understands what is possible.
```

## TRIGGER
---
*It is covered with light*

*This corner of the planet*

--- from [downy - "Underground"](https://www.youtube.com/watch?v=bcmgJfUv7DU)

## ECO
---
The Agent is a 17 year-old child prodigy. She initially rose to fame through her political and social writings, which were highly-advanced for her age.

She continues to rise by striking fear into the heart of men. She holds a power that many men fear; persuasion. With the flick of a tongue, she is able to break nearly anyone of the opposite gender. 

The weakest of which are willing to do her bidding - no matter the task. She is the puppet master, and they are the puppets.

And her eyes are set on presidency.

## ECHO
---
*All the stories for those*

*Who are too old*

*And all of those we locked away*

*That we should have told*

*Think we thought you were*

*About to find out anyway*

*As they washed up on the*

*Shores of Arcadia Bay*

--- from [Stand Up Stacy - "Mirrors"](https://www.youtube.com/watch?v=P5ABAideenM)