# The Schizo
## RECORD
---
```
Name: $REDACTED Durden
Alias: ['The Schizo', and 2,805 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 24 Earth years
Chronological Age: N/A
SCAN Rank: | D D
           | D D
TIIN Rank: | C C
           | C D
Reviewer Rank: 2 stars
Variables:
  $WOKE:          -0.40 | # Does not appear to be.
  $SCHIZOPHRENIC: -0.80 | # Debilitatingly so.
```

## TRIGGER
---
[![The Schizo](/static/images/schizo.0.png)](/static/images/schizo.0.png)

## ECO
---
The Schizo was brought into the mental hospital at the same time [Malcolm](/docs/personas/fodder) was. He was utterly crippled by his disease. Barely lucid, he spent most of his time sleeping, or speaking to the non-existent entities in his head.

Malcolm would spend an evening playing dominos with him. The Schizo would beat him time and time again, always laughing, saying, "it's about the 5's. 5 wins every time."

Malcolm wasn't interested in winning; he was interested in the way the Schizo was keeping score. He was using a system unlike any he had ever seen before (see above TRIGGER), and no matter how many times he asked for an explanation, he just couldn't understand. This system was completely foreign to him.

Malcolm would come to believe that schizophrenia is wildly misunderstood. Perhaps there is very real, very valid intelligence behind the crippling side-effects - and Malcolm's own [Perspective](/posts/theories/perspective) was preventing him from understanding.

## PREDICTION
---
```
It is by no mistake that the Schizo's last name is the same as Tyler Durden's, from Fight Club - Malcolm's favorite movie.
```