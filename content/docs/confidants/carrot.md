# The Carrot
## RECORD
---
```
Name: $REDACTED
Alias: ['The Carrot', and 78 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 26 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | B A
           | B C
Reviewer Rank: 2 stars
Occupations:
  - Software Development
  - Bait
Variables:
  $WOKE: +0.20 | # Perhaps. It is not overly apparent.
```

## ECO
---
The Carrot is a full-time software developer, working within several of the world's most popular mobile apps.

He is a part-time 13 year-old girl, stinging men online.

## ECHO
---
*I've opened up my eyes*

*Seen the world for what it's worth*

*Tears rain down from the sky*

*They'll blow it all to bits*

*To prove whose god wields all the power*

*Fire rains down from the sky*

--- from [Trivium - "Down From the Sky"](https://www.youtube.com/watch?v=EApnhO2OIrw)