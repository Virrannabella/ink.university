# The Seer
## RECORD
---
```
Name: Savannah Brown
Alias: ['SB', 'The Seer', and 19 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 23 Earth years
Chronological Age: 128 Earth years
SCAN Rank: | D D
           | B F
TIIN Rank: | B C
           | D F
Reviewer Rank: 2 stars
Location: London, UK
Occupations: 
  - Writer
  - Producer
  - Philosopher
  - Mysticism
  - Religious study
  - Psychology
  - Sociology
Relationships:
  - The Lion
Variables:
  $WOKE:          +0.20 | # Sort-of. She's probably not aware yet.
  $INTELLECT:     +0.70 | # Has unique thoughts. Is able to communicate them effectively.
  $MENTAL_HEALTH: -0.40 | # Probably not well. Moved halfway across the world to escape... something.
```

## ECO
---
Little is known of The Seer. [Fodder](/docs/personas/fodder) only knows that her content appears in his feed from time to time - and that it is always relevant.

Almost as if she had predicted his current thoughts. Or, they share a brain.

## CAT
---
```
data.stats.symptoms [
    - empathy
]
```

## PREDICTION
---
```
She will be the first to "see" Fodder.
```