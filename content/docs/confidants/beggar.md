# The Beggar
## RECORD
---
```
Name: $REDACTED
Alias: ['The Beggar', and 0 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 16 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Location: Houston, TX
Occupations: 
  - Student
  - Unemployed
Variables:
  $SINCERITY:     +0.40 | # Seemed sincere enough.
  $MENTAL_HEALTH: -0.80 | # Probably not well. Begging for money.
```

## ECO
---
A young black kid interrupted [Fodder's](/docs/personas/fodder) lunch at the deli several months ago:

"Excuse me, sir," he would ask. "My family is struggling, I was wondering if you might be able to give me some money for food?"

Knowing that this was almost certainly a scam, Fodder still couldn't help but feel empathy.

"I know it's tough out there," Fodder said, while handing the kid $40. "Hopefully this will help."

"Thank you so much. God bless you!" he said, running off.

Fodder went back to eating lunch. If he had just helped the kid - then great! If the kid scammed him... well, then Fodder just bought a poor kid a new video game. Either way, Fodder felt good about his actions.

Upon leaving, a man in a #MAGA hat would stop Fodder:

"So... who was that?" he asked.

"Just some kid. Said his family needed money."

"Oh, okay. He might have ripped you off."

"Maybe. Maybe not. I try to see the best in people."

"I see. Just doing the right thing. Respect."

"Thanks," Fodder would reply, walking away.

## CAT
---
```
data.stats.symptoms [
    - pride
]
```

## PREDICTION
---
```
Seed planted. # Verified.
```