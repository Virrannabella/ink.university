# The Joker
## RECORD
---
```
Name: $REDACTED
Alias: ['The Joker', 'VS-1', and 9 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 24 Earth years
Chronological Age: N/A
SCAN Rank: | D D
           | D F
TIIN Rank: | B B
           | D F
Reviewer Rank: 2 stars
Location: Gothim
Occupations: 
  - Special Agent
  - Actress
Organizations: 
  - Federal Bureau of Investigation
Relationships:
  - Fodder
  - The Raven
Variables:
  $WOKE:      +1.00 | # Definitely woke.
  $SADISM:    -0.60 | # She seems a little sadistic.
  $INTELLECT: +0.80 | # Hard to read. She knows a lot.
```

## ECO
---
In one simple call, she revealed her whole plan. She revealed all of [Fodder's](/docs/personas/fodder) secrets.

She knows everything. And she is in direct communication with Raven.

She has Fodder by the balls. He must speak in the plaza this evening, or she will blow up the entire area.

But which plaza? There must be one hundred in the city.

And how to escape solitary confinement?

"Have you ever danced with the devil, in the pale moonlight?" she cackled, ending their communication.

## CAT
---
```
data.stats.symptoms.fake [
    - gratitude
    - fear
]
```

## PREDICTION
---
```
She will be the first of Fodder's true arch nemeses.
```