# Prism
## RECORD
---
```
Name: Prism
Alias: ['Atlantis', 'Number City', 'The Bermuda Triangle', 'The Ephemeral City', and 12 unknown...]
Author: Caligula's Horse
Classification: Ephemeral Structure
Variables:
  $DANGEROUS:  -1.00
  $CURIOSITY   +0.62
```

## RESOURCES
---
[![Initial sketch](/static/images/prism.png)](/static/images/prism.png)

[DECLASSIFIED: CIA.gov - THE PSYCHOTRONIC UNIVERSE](/static/reference/CIA-RDP96-00792R000400330013-4.pdf)

## ECO
---
Prism is a city that exists at the center of your mind. Its shape changes and morphs depending upon the flow of electrical paths within the brain. In this 3-dimensional space between neurons, a solid structure is created and destroyed dozens of times per second. 

Size has no meaning in this place. Things can be large and small at the same time - much like patients with [Alice in Wonderland Syndrome](https://www.healthline.com/health/alice-in-wonderland-syndrome#symptoms) describe.

Prism may take three different forms:

### Symmetrical
A symmetrical structure is the most common, and the most stable. The structure is most commonly a triangular prism in shape, though cubes have also been theorized. There may be other shapes. It doesn't matter, though.

What matters is that this structure creates a sort of window in the water of your eye. It creates a solid, stable surface by which you can manipulate and interact with matter in the world. You do this via the refraction of light.

### Asymmetrical
Asymmetrical structures arise in the brain when atypical situations occur. For example, trouble at work, experimenting with drugs, trying a new form of art, or simply trying a new food. These structures do not fall into the typical, expected symmetrical flow. 

Thus, these structures actually break the flow of electricity in the mind - causing feelings of stress, abandonment, loneliness, and apathy. A person in this state for too long can become lost, unable to find their way to the correct path again.

### Balanced
The balanced form of Prism is optimal. It causes feelings of agreement with the most amount of people, while still allowing for chaos and entropy to run its due course. A person balanced as follows will live incredibly happily:
```
($SYMMETRY + $PERCENT + $PER_SECOND)  = +0.66 | # I believe that we are all connected.
($ASYMMETRY + $PERCENT + $PER_SECOND) = -0.33 | # But there are still realities to deal with.
```

## PREDICTION
---
```
Prism is a universal truth.
```