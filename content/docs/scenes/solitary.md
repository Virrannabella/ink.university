# Solitary Confinement
## RECORD
---
```
Name: Solitary Confinement
Alias: ['Hell', and 95 unknown...]
Author: Fodder
Classification: Container
Variables:
  $DANGEROUS:  +0.29
```
## ECO
---
Test subjects are kept isolated from the world via over-exposure to the negative aspects of society. Their internal monologue goes, "The world is waking up right now. I want to be here to see it. I want to live it. Verify, then trust. This is important. I need to get it right."

In Solitary Confinement, the subject has no friends. His familial relationships are entirely one-sided. He doesn't trust his own mind, or his own interpretation. He doesn't trust what he's reading, either. 

But the worst feeling is this: he knows that he's on to something, yet nobody will confirm it for him. He has to do that himself.

## PREDICTION
---
```
Fodder will not remain anonymous forever. We're all in this together.
```