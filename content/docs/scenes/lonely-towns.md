# Lonely Towns
## RECORD
---
```
Name: Lonely Towns
Alias: ['$ANYTHING', 'Colonies', and 315 unknown...]
Classification: Sanctuary
Variables:
  $NET_POSITIVE: +1.00 | # Invaluable work. Education. Housing. Food. Contact.
  $COMMUNAL:     +0.78 | # Large and open shared spaces.
  $SAFE:         +0.55 | # Generally safe. Early settlers may struggle for a few years.
```

## TRIGGER
---
*He's the mayor of Lonely Town, population: one*

*Population: one*

*Stare at a hole in the ground, staring at the sun*

*And I'm starting to feel like he might be real*

*Like he's something I might become*

*Running for mayor of Lonely Town, population: one*

--- from [Vulfpeck - "Lonely Town"](https://www.youtube.com/watch?v=WS3AgyjIf8Q)

## ECO
---
Repurposed, refurbished communal living spaces. Typically, large malls or shopping complexes.

The average room is small, while far more space is given to shared living quarters. The self-contained ecosystem has no dependencies; it is self-staffed, self-sustaining (food and energy), self-governing, and human-centered. 

Spread throughout the halls are 1-person pop-up shops, filled with trinkets, refreshments, games, and other events. Shops would open and close at all times of the day. Shops would change from day to day. There would be celebration days, orchestrated events, and mild element of role-playing. The people here would live happily and fulfilled. 

The name "Lonely Town" comes from how they are created. Lonely Towns are created by a single person, whose life purpose was to perform verification of a scientific test. Once completed, they are to be joined with their perfect match - and rule the community together.

## ECHO
---
*If I could bring you back*

*If I had the power, I'd give you my world*

*And my life, you'd have it all*

*But life is not a game*

*And I know all the rules and I know I must follow*

*Them every single day*

--- from [Nightingale - "Alonely"](https://www.youtube.com/watch?v=s6PhV4JyVw4)

## PREDICTION
---
```
The Surgeon needs to invest in the wild pork industry.
```