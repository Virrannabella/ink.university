# Void
## RECORD
---
```
Name: Void
Classification: Location
```
## TRIGGER
---
A voidwalking friend.

## ECO
---
The Void is made up of the inaccessible places outside of [Prism](/docs/scenes/prism). It is the unknown places in the multiverse.

It is possible to travel through the Void to reach other [Networks](/docs/scenes/networks). In fact, this is what is happening to all of us while [Dreaming](/docs/scenes/dreams/).

## ECHO
---
*We'll miss you everyone*

*It sure was fun, the times we all had*

*We'll miss you everyone*

*The time has come*

*We'll miss you*

--- from [Thank You Scientist - "My Famed Disappearing Act"](https://www.youtube.com/watch?v=8-QM0XP_h3c)

## PREDICTION
---
```
A person can travel through the Void to join other networks.
```