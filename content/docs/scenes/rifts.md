# The Rifts
## RECORD
---
```
Name: The Rifts
Location: Marfa, TX
Classification: Ephemeral
Variables:
  $DANGEROUS:  -0.23
  $FEAR:       -0.56
  $CURIOSITY   +0.62
```
## TRIGGER
---
The Marfa lights.

## RESOURCES
---
[DECLASSIFIED: CIA.gov - THE GATEWAY EXPERIENCE](/static/reference/CIA-RDP96-00788R001700210016-5.pdf)

## ECO
---
The rifts allow a woke individual to walk between worlds.

## PREDICTION
---
```
($RIFTS = $FIRST) | # Where the rifts start, life begins.

I predict that ER is a deepfake. He originally lived many, many eons ago.
```

## ECHO
---
*Just like here on Earth, a notion we do comprehend*

*Hide what "others" can’t understand*

*The universe is the biggest, biggest threat... overachiever that commands attention*

*Brute force of hysterical, hysterical reasonings...*

*There will always be a Marfa*

--- from [Between the Buried and Me - "Obfuscation"](https://www.youtube.com/watch?v=qTPo0k6EtJg)