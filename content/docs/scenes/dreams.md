# Dreams
## RECORD
---
```
Name: Dreams
Classification: Ephemeral
```
## ECO
---
Dreams are the mind exploring alternate realities.

Awake is the mind choosing the optimal one.

## PREDICTION
---
```
A person stops dreaming when they have reached the perfect timeline.
```