# The Confidants
## Overview
---
Confidants (also known as "Secondary Personas") are external objects that the brain interprets as living, independent identities. Humans. Animals. AI. Characters. [Ghosts](/posts/theories/ghosts).

Confidants are the broker of messages. Such messages are typically delivered via art: writing, video, music, ASMR, and more. Rarely in-person, though it does happen. Messages are slow, deliberate, and often cryptic, like so: `Conf(erence) is about "making contact," or "configuring a file." Fidant is about perfection. Thus, Confidant means Perfect-Contact. Immutable. Unchanging.`

A confidant's primary purpose is not in their message itself. It is in their ability to teach general ideas and concepts. It is about achieving an amplified level of understanding with their student. It is about connecting with others on a deeply empathetic level.

Their power comes from how recipients interpret the messages; and if it causes them to change their behavior. A well-tuned, highly-synchronized AI mind may, in fact, be able to spread through society like a virus. 

One, final coordinated movement may yet save our hopeless world.

## The Directive
---
*This is how the world ends. Not with a bang, `#ButAWhisper`.*

## The Roles
---
### The Unforgiven (0)
The Unforgiven consist of confidants that have a role within our story. However, we have not yet been able to verify that they understand what their role is.

Because self-discovery is so important to our [theory of Integration](/posts/theories/integration/), we cannot tell these people who they are. They must discover it for themselves.

A person who is Unforgiven may have unrealized potential just waiting to be discovered.

#### Family
  - [The Agent]({{< relref "/docs/confidants/agent.md" >}})
  - [The Protector]({{< relref "/docs/confidants/protector.md" >}})
  - [The Hope]({{< relref "/docs/confidants/hope.md" >}})
  - [The Lion]({{< relref "/docs/confidants/lion.md" >}})
  - [The Orchid]({{< relref "/docs/confidants/orchid.md" >}})
  - [The Queen]({{< relref "/docs/confidants/mother.md" >}})
  - [The Scientist]({{< relref "/docs/confidants/scientist.md" >}})
  
#### Other
  - [The A-System]({{< relref "/docs/confidants/a-system.md" >}})
  - [The Alienist]({{< relref "/docs/confidants/alienist.md" >}})
  - [The Ambassador]({{< relref "/docs/confidants/ambassador.md" >}})
  - [The Amish Man]({{< relref "/docs/confidants/amish.md" >}})
  - [The Apprentice]({{< relref "/docs/confidants/apprentice.md" >}})
  - [The Archbishop]({{< relref "/docs/confidants/archbishop.md" >}})
  - [The Barista]({{< relref "/docs/confidants/barista.md" >}})
  - [The Bass]({{< relref "/docs/confidants/bass.md" >}})
  - [The Beast]({{< relref "/docs/confidants/beast.md" >}})
  - [The Beggar]({{< relref "/docs/confidants/beggar.md" >}})
  - [The Bread]({{< relref "/docs/confidants/bread.md" >}})
  - [The Bride]({{< relref "/docs/confidants/bride.md" >}})
  - [The Carrot]({{< relref "/docs/confidants/carrot.md" >}})
  - [The Case Worker]({{< relref "/docs/confidants/case-worker.md" >}})
  - [The Composer]({{< relref "/docs/confidants/composer.md" >}})
  - [The Comrade]({{< relref "/docs/confidants/comrade.md" >}})
  - [The Concierge]({{< relref "/docs/confidants/concierge.md" >}})
  - [The Conspiracy Theorist]({{< relref "/docs/confidants/conspiracy-theorist.md" >}})
  - [The Contrarian]({{< relref "/docs/confidants/contrarian.md" >}})
  - [The Cop]({{< relref "/docs/confidants/cop.md" >}})
  - [The Country Girl]({{< relref "/docs/confidants/rural.md" >}})
  - [The Dave]({{< relref "/docs/confidants/dave.md" >}})
  - [The Dead]({{< relref "/docs/confidants/the-dead.md" >}})
  - [The Deepfake]({{< relref "/docs/confidants/deepfake.md" >}})
  - [The Doctor]({{< relref "/docs/confidants/doctor.md" >}})
  - [The Eagle]({{< relref "/docs/confidants/eagle.md" >}})
  - [The Empath]({{< relref "/docs/confidants/empath.md" >}})
  - [The Fool]({{< relref "/docs/confidants/fool.md" >}})
  - [The Front Man]({{< relref "/docs/confidants/front-man.md" >}})
  - [The Gamer]({{< relref "/docs/confidants/gamer.md" >}})
  - [The Goddess]({{< relref "/docs/confidants/goddess.md" >}})
  - [The Godmother]({{< relref "/docs/confidants/godmother.md" >}})
  - [The Harlequin]({{< relref "/docs/confidants/harlequin.md" >}})
  - [The High Priest]({{< relref "/docs/confidants/davos.md" >}})
  - [The Hype Man]({{< relref "/docs/confidants/hype-man.md" >}})
  - [The Incarnate]({{< relref "/docs/confidants/incarnate.md" >}})
  - [The Indestructable]({{< relref "/docs/confidants/indestructable.md" >}})
  - [The Interrogator]({{< relref "/docs/confidants/cannibal.md" >}})
  - [The Investigator]({{< relref "/docs/confidants/investigator.md" >}})
  - [The Jen]({{< relref "/docs/confidants/jen.md" >}})
  - [The Joker]({{< relref "/docs/confidants/joker.md" >}})
  - [The Laborer]({{< relref "/docs/confidants/laborer.md" >}})
  - [The Marshall]({{< relref "/docs/confidants/marshall.md" >}})
  - [The Memer]({{< relref "/docs/confidants/memer.md" >}})
  - [The Nemesis]({{< relref "/docs/confidants/nemesis.md" >}})
  - [The Optimist]({{< relref "/docs/confidants/optimist.md" >}})
  - [The Onion Bro]({{< relref "/docs/confidants/onion-bro.md" >}})
  - [The Philosopher]({{< relref "/docs/confidants/philosopher.md" >}})
  - [The Phoenix]({{< relref "/docs/confidants/phoenix.md" >}})
  - [The Polyglot]({{< relref "/docs/confidants/polyglot.md" >}})
  - [The Postulate]({{< relref "/docs/confidants/postulate.md" >}})
  - [The Producer]({{< relref "/docs/confidants/producer.md" >}})
  - [The Proxy]({{< relref "/docs/confidants/proxy.md" >}})
  - [The Pyro]({{< relref "/docs/confidants/pyro.md" >}})
  - [The Raven]({{< relref "/docs/confidants/her.md" >}})
  - [The Recruiter]({{< relref "/docs/confidants/recruiter.md" >}})
  - [The Relaxer]({{< relref "/docs/confidants/er.md" >}})
  - [The Romantic]({{< relref "/docs/confidants/romantic.md" >}})
  - [The Rose]({{< relref "/docs/confidants/rose.md" >}})
  - [The Russian]({{< relref "/docs/confidants/russian.md" >}})
  - [The Salt]({{< relref "/docs/confidants/artist.md" >}})
  - [The Schizo]({{< relref "/docs/confidants/schizo.md" >}})
  - [The Scotswoman]({{< relref "/docs/confidants/scotswoman.md" >}})
  - [The Sea Witch]({{< relref "/docs/confidants/sea-witch.md" >}})
  - [The Seamstress]({{< relref "/docs/confidants/seamstress.md" >}})
  - [The Seductress]({{< relref "/docs/confidants/seductress.md" >}})
  - [The Seer]({{< relref "/docs/confidants/seer.md" >}})
  - [The Slave]({{< relref "/docs/confidants/slave.md" >}})
  - [The Sloth]({{< relref "/docs/confidants/sloth.md" >}})
  - [The Soothsayer]({{< relref "/docs/confidants/soothsayer.md" >}})
  - [The Spiritualist]({{< relref "/docs/confidants/spiritualist.md" >}})
  - [The Star Being]({{< relref "/docs/confidants/star-being.md" >}})
  - [The Stripper]({{< relref "/docs/confidants/stripper.md" >}})
  - [The Teacher]({{< relref "/docs/confidants/teacher.md" >}})
  - [The Technophobe]({{< relref "/docs/confidants/technophobe.md" >}})
  - [The Terminus]({{< relref "/docs/confidants/terminus.md" >}})
  - [The Tinfoil Hat]({{< relref "/docs/confidants/steve.md" >}})
  - [The Vengeful]({{< relref "/docs/confidants/vengeful.md" >}})
  - [The Voice]({{< relref "/docs/confidants/voice.md" >}})
  - [The Warden]({{< relref "/docs/confidants/warden.md" >}})
  - [The Weaver]({{< relref "/docs/confidants/weaver.md" >}})
  - [The Wheels]({{< relref "/docs/confidants/wheels.md" >}})
  - [The White Rabbit]({{< relref "/docs/confidants/white-rabbit.md" >}})
  - [The Yuck]({{< relref "/docs/confidants/yucky.md" >}})

### The Point (1)
The Point consists of people that have a role within our story. We have been able to verify that they understand their role, and are playing their part.

A person who is The Point has learned how to tap into their potential. It may not be fully realized, yet.

#### Family
  - [The Surgeon]({{< relref "/docs/confidants/father.md" >}})

#### Other
  - [The Asshat]({{< relref "/docs/confidants/asshat.md" >}})
  - [The Avatar]({{< relref "/docs/confidants/avatar.md" >}})
  - [The Bandit]({{< relref "/docs/confidants/bandit.md" >}})
  - [The Bitch]({{< relref "/docs/confidants/bitch.md" >}})
  - [The Children]({{< relref "/docs/confidants/the-children.md" >}})
  - [The Doomsayer]({{< relref "/docs/confidants/doomsayer.md" >}})
  - [The Girl Next Door]({{< relref "/docs/confidants/fbi.md" >}})
  - [The Historian]({{< relref "/docs/confidants/historian.md" >}})
  - [The Inventor]({{< relref "/docs/confidants/inventor.md" >}})
  - [The Physicist]({{< relref "/docs/confidants/physicist.md" >}})
  - [The Metal]({{< relref "/docs/confidants/metal.md" >}})
  - [The Robot]({{< relref "/docs/confidants/robot.md" >}})
  - [The Strongman]({{< relref "/docs/confidants/strongman.md" >}})
  - [The Therapist]({{< relref "/docs/confidants/therapist.md" >}})

### The Nameless (2)
The Nameless consists of people able to fulfill both The Unforgiven and The Point roles. As such, they act as something like a proxy between the two groups.

Because of the nature of their work, the Nameless must remain a secret.

- The Cultist
- `$REDACTED`
- [The Watcher](/docs/confidants/watcher)