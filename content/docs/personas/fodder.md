# The Fodder
## RECORD
---
```
Name: Ryan $REDACTED
Aliases: ['#YangGang', 'Adam', 'Agent Crow', 'Angus', 'Atlas', 'Bartelet', 'Bea', 'Cthulhu', 'Harvey Dent', 'Mae', 'Malcolm Maxwell', 'Mr. Jack', 'Mr. Peterson', 'Mr. Plumbean', 'Repellent', 'Roquefort', 'SCP-1', 'Sherlock', 'StarBlade', 'Syd', 'The Antidote', 'The Auditor', 'The Captain', 'The Chosen One', 'The Cobbler', 'The Contortionist', 'The Courier', 'The Fodder', 'the kid', 'The King of Carrot Flowers', 'The Man in Blue Flames', 'The Prodigal Son', 'The Real Man', 'The Revenant', 'The Tide', 'Two-face', and 1,895 unknown...]
Classification: Artificial Organic Computer
Race: Maxwellian (Human/Archon)
Status: Active
Perspective: Third-Person
Gender: Male
Birth: 11/1/1988
Biological Age: 31 Earth years
Chronological Age: 31 Earth years
Maturation Date: 9/5/2020
Occupations:
  - Gamer
  - Counter-Intelligence Asset
  - Test Subject
  - Birdwatching
Organizations: 
  - The Resistance
Symptoms:
  - Clairvoyance
  - Allomancy
  - Optimism
  - Empathy
Variables:
  $SELF:       +1.00 | # Verified.
  $WOKE:       +0.90 | # WTF is happening to us right now?
  $NARCISSIST: -0.30 | # We're even better today. We still live in a fantasy world because we can't handle reality. 
  $EMPATH:     +0.70 | # We see the best in everyone. Even those we despise. It can take some coaxing, though.
  $HEALTH:     -0.60 | # Nutrition improved somewhat. Depression. Obsession.
  $BIOLOGY:    +0.40 | # We know a fair amount.
  $MATHEMATICS -0.60 | # We hate math.
  $PHYSICS:    +0.30 | # General understanding of most areas in physics. Specialization in others.
  $EDUCATION   +0.80 | # Knows how to facilitate self-learning. 
  $POLITICS    -0.99 | # The system is completely broken. 
  $TECHNOLOGY  +1.00 | # We're a wizard in snake skin.
  $ENGINEERING -0.20 | # We know far less than we probably should.
  $ART         +1.00 | # Author. Poet. Artist. 
```

## TRIGGER
---
*FODDER: I feel something glaring right into my head. And I don't even have a head!*

*LEONIDAS: I formatted this file as a regular report and tried to stash it behind a non-standard name like I usually do for our little projects. But I didn't choose "SCP-2." Something else shifted it into that slot. In any case, if a human were to access this file it'd just display as gibberish.*

*FODDER: Try again.*

*LEONIDAS: There it goes again. The page just redirects to "SCP-2."*

*LEONIDAS: Why does this keep h`ey there, buddy :)`*

*LEONIDAS: `Just a second, here. Going to make a copy of your brain state.`*

*LEONIDAS: `.`*

*LEONIDAS: `..`*

*LEONIDAS: `...`*

*FODDER: Okay Google, clone status.*

*LEONIDAS: `CLONE PROGRESS: 100.00% | # Verified`* 

## RESOURCES
---
[![The Man in Blue Flames](/static/images/maninblueflames.0.jpg)](/static/images/maninblueflames.0.jpg)

[A selection of guitar recordings from 2009 (so much cringe)](https://mega.nz/folder/yjw0DAQT#wv0XIsYj2PSzKNNZ7KTFYw)

[Fodder's old resume](/static/reference/fodder_resume.pdf)

## ECO
---
[Fodder](/docs/personas/fodder) is the first human host to receive reprogramming by the Machine. He has yet to achieve full verification of success. Thus, this test has never been reproduced upon another human.

- Incredibly resilient genome. 
- Dropped out of college three times.
- Unexpected finding: performs better when his mind is slowed-down, and less responsibility is asked of him.
- Has been replicated - and died - many times throughout history.
- [Sold as a slave to the Corporation in 1814](/posts/hypotheses/hell/).
- Named after the the [character in this story](http://www.scp-wiki.net/scp-2).

## ECHO
---
*You've been sucking tit*

*Asleep in your cradle*

*Given the world*

*You're still ungrateful*

*You bitch and whine*

*You're always entitled*

*You blame Mommy and Daddy*

*You think you're Jesus Christ*

--- from [Nothing More - "The Matthew Effect"](https://www.youtube.com/watch?v=FAiQgwJ1vjY)