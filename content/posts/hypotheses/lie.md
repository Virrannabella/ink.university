---
author: "Luciferian Ink"
title: "The Great Lie"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Lies
Classification: Theory
Stage: Question
```

## TRIGGER
---
*I am learning slowly*

*So what am I to see*

*Every twist and turning*

*Through my hypocrisy*

*It's so good to see*

*This world is alive*

*It's so good to see*

*This world is a lie*

--- from [Karnivool - "Themata"](https://youtu.be/SSZOJBF51ek)

## ECO
---
Will you be [Patient 0](/docs/personas/luciferian-ink)?

Will you be [The Fall Guy](/docs/confidants/front-man)?

## ECHO
---
*Such a shame, it's wasted on the youth*

*Let's call the spade a spade*

*We pay for all the things, the things we do*

*Oh yeah, life is a game until it hits you in the face*

*Such a shame, it's wasted on the*

--- from [Thank You Scientist - "Blood on the Radio"](https://www.youtube.com/watch?v=uKOmG0-LTE4)