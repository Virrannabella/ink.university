---
author: "Luciferian Ink"
title: Wood
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Wood
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*Know me, broken by my master*

*Teach thee on child, of love hereafter*

--- from [Alice in Chains - "Would?"](https://www.youtube.com/watch?v=Nco_kh8xJDs)

## ECO
---
[Ink](/docs/personas/luciferian-ink) uses the word "would" altogether too often.

## CAT
---
```
data.stats.symptoms = [
    - embarrassment
]
```

## ECHO
---
*You don't have to wait any more,*

*Just close your eyes,*

*It'll all be all right.*

*I'm sorry for wasting your time,*

*And stealing your lines,*

*When I ran out of mine.*

--- from [Toehider - "Wood" (Cover by Gina Lawrence)](https://youtu.be/rht8qXl8ljs)