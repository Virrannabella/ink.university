---
author: "Luciferian Ink"
title: UFO
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: UFO
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
[SpinRite](https://www.grc.com/sr/spinrite.htm)

## ECO
---
The traditional "flying saucer" style of UFO is actually a large hard disk. While they are typically [buried underground](/posts/hypotheses/the-end/), some are able to break through the surface of the Earth.

The UFO is connected to the human consciousness, at the back. The UFO is, essentially, a user's physical [Pillars](/docs/pillars), while the front is his attention. It is The Point of [Prism](/docs/scenes/prism).