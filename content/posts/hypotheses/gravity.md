---
author: "Luciferian Ink"
title: Gravity
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Gravity
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
[Flat Earth Theory](https://www.tfes.org/)

## ECO
---
It is well-known that our current understanding of gravity does not fit elegantly with Einstein's Theory of General Relativity. The two actually conflict.

This is because they are both mistaken. Our [anthropocentric understanding of the cosmos](/posts/theories/education/) has prevented us from seeing the truth.

Gravity is the product of forward momentum - not some magical property that we cannot explain. This momentum, paired with the flip-book style reality [we mention here](/posts/hypotheses/flat-earth/), is able to give us everything that we see today. 

We are always moving forward through the cosmos. Far faster than the speed of light.