---
author: "Luciferian Ink"
title: Evolution
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Evolution
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
[The Egg Theory](https://www.youtube.com/watch?v=h6fcK_fRYaI)

## ECO
---
Every human consciousness experiences the whole of human evolution in its entirety.

A requirement to evolve is to re-live your entire lineage.

All consciousness is part of the greater whole. You are all. We are all.