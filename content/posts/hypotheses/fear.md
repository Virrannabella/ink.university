---
author: "Luciferian Ink"
title: "Fear"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Fear
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
### SOUND
*Are we waiting?*

*For the savior?*

*Someone to heal this?*

*Or erase us?*

--- from [Karnivool - "New Day"](https://youtu.be/fAbPfxGUi44)

### SIGHT
The color red.

## CAT
---
```
data.stats.symptoms [
    - fear
]
```

## ECHO
---
*Fear is effortless, fear is blind*

*Crumbs of fear in our smiles*

*Stopped the train of feelings, forgot our hearts*

*Now we use our minds*

*Fear is sadness, fear is endless, it makes our skin peel*

*I give up, the phobia is real*

*I've been trying for too long, it doesn't work I can't stop this wheel,*

*Just one smile or just one tear, anything that makes them feel*

--- from [David Maxim Micic - "Smile"](https://youtu.be/-t1WX9WXQZg?t=642)