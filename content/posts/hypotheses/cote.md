---
author: "Luciferian Ink"
title: "Center of the Earth"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Center of the Earth
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*Are you willing to choose what I have in my mind?*

*Say it loud, with a voice of no reason in mind*

--- from [Karnivool - "Center of the Earth"](https://youtu.be/IpVpO9uJmqU)

## ECO
---
Those horrible nightmares from childhood.

The Earth is a giant hard drive that stores our memories. Each consciousness is stored upon a hyper-compressed, [UFO-like object](/posts/hypotheses/ufo/).