---
author: "Luciferian Ink"
title: Symbolism
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Symbolism
Classification: Theory
Stage: Hypothesis
```

## ECO
---
Symbols hold explanatory power. 

If we are not learning new symbols, we are losing access to new ways of thinking.

In a [model where mental paths are defined in the cosmos](/posts/theories/education) - we are severely limiting ourselves.