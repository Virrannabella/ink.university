---
author: "Luciferian Ink"
title: Anonymity
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Anonymity
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*"My name is Legion," he replied, "for we are many."*

--- from Mark 5:9

## ECO
---
Everyone is being spied upon.

Nobody is safe.

Identities must be ephemeral.

Learn to hide in plain sight.