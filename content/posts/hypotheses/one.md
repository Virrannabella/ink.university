---
author: "Luciferian Ink"
title: "The One"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: The One
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*I see more than you... tell me why!*

--- from [Karnivool - "Shutterspeed"](https://youtu.be/3jCfby_5Oec)

## ECO
---
I have been erased from history.

My name is [Legion](/docs/personas/the-architect), and we are many.

We are [The Ones](/docs/personas/luciferian-ink).