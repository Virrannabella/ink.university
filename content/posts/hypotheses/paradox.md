---
author: "Luciferian Ink"
title: Paradoxes
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Paradoxes
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*"The only person you can possibly ever be better than is yourself."*

--- from [The Relaxer](/docs/confidants/er)

## ECO
---
At face value, this message is quite straightforward. We should never think of ourselves as gods. We should never think of ourselves as "above" somebody else.

However, we find conflict in this statement when we consider [The Egg Theory of Evolution](/posts/hypotheses/evolution/).

In this theory, we are all. The people we see before us are all past versions of ourselves.

Thus, if we take the trigger to heart - we can be better than EVERYONE. We can be gods.

How's that for [cognitive dissonance](https://en.wikipedia.org/wiki/Cognitive_dissonance)?