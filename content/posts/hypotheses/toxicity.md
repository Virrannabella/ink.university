---
author: "Luciferian Ink"
title: Toxicity
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Toxicity
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*It'd be interesting if you could monitor toxicity levels online in the same way we do CO2 or other pollution.*

*We don't even clean up our real world pollution, it's hard to imagine we'd do it for our digital kind but it could be a place to start.*

*And companies could decide to push their algorithms towards positivity metrics instead of engagement metrics.*

*And if they aren't, someone should at least keep an eye on it (as much as one can off of public data)*

*Sorry, in idea mode. Haven't been around to think about this kinda stuff in a while.*

*Back to our hellish reality.*

--- from [@PostimusMaximus](https://twitter.com/PostimusMaximus/status/1174455595715502085)

## ECHO
---
*When I became the sun*

*I shone life into the man's heart*

--- from [System of a Down - "Toxicity"](https://www.youtube.com/watch?v=iywaBOMvYLI)