---
author: "Luciferian Ink"
title: Code Paradigms
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: $REDACTED
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
- [Computer Programming](/posts/bans/ban.11/)
- [/r/5September2020](https://www.reddit.com/r/5September2020/)
- Merge Requests

## ECO
---
The world is sending you messages/signals in code.

Pay attention to every detail.

Complex Problem.