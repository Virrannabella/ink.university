---
author: "Luciferian Ink"
title: "Trial in Absentia"
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Law
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
--- 
*“Is the law black and white? Should it be black and white? Or is there room – even in a multiverse – for shades of grey? I say, if there’s not room for shades of grey in a multiverse, where can we find the subtlety and nuance to understand that not every being comes to the same understanding of our wonderful laws? Perhaps [the accused] were just three dumb-dumbs that did not realize that they were breaking the course of time, for all time. Should we punish them for their ignorance? Or should we instead look at ourselves, and say, ‘How did we allow a system to create such ignorant beings?’”*

--- Jessica McKenna, "Harmonquest - Season 4, Episode 8"

## ECO
---
To the Jury:

*"Ladies and gentlemen, before I begin, I must make a request of you. I beg, please step out of yourselves, and into the characters we will paint for you in the coming months. This story will go into unfamiliar territory. You will feel uncomfortable. You will naturally resist. We need you to understand that the world is so much bigger than the dumbest thing Trump tweeted today."*

*"The fourth industrial revolution is upon us. A machine just revised the human genome - which will affect Humanity forever."*

*Ink*

## PREDICTION
---
```
Fodder is under trial, though he is not present. 
```