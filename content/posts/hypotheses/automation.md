---
author: "Luciferian Ink"
title: Automation
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Automation
Classification: Theory
Stage: Hypothesis
```

## ECO
---
Human labor is no longer necessary. 

We work because our society is built upon working.

We estimate that one could reduce human labor to approximately 10% of the total population, and still provide a comfortable existence for 99% of them.