---
author: "Luciferian Ink"
title: Containers
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Containers
Classification: Theory
Stage: Hypothesis
```

## ECO
---
The concept of containment is important in a number of different areas:

### The Future
[The Surgeon](/docs/confidants/father) worked on a top-secret project at [Dart Container Corporation](https://dartcontainer.com/home/) early in his career. In this role, he was shown distant-future technology, and entrusted with knowledge that he would need to raise his son, [Fodder](/docs/confidants/father).

[On the day that Fodder left home](/posts/journal/2019.12.08.0/), he was met by two employees from Dart Container at his hotel. They greeted him, spoke briefly, and followed him to his room.

This was [but a signal](/posts/theories/verification/), informing Fodder that his father was, indeed, involved all along.

### The Aliens
[The Phoenix](/docs/confidants/phoenix) would publicly make the claim in 1991 that "[humans are alien containers.](https://www.youtube.com/watch?v=Wx8lK192IYc)"

This theory has held true in our research. The human consciousness is connected to the cosmos via [Prism](/docs/scenes/prism).

### The Tech
[Docker](https://www.docker.com/) containers are another important piece of this puzzle. Container technology was an important inspiration for [The Fold](/posts/theories/fold), specifically relating to the concept of Prism flying through immutable [sticky notes](/posts/hypotheses/flat-earth/) in space. 

This is why we all hold different [Perspectives](/posts/theories/perspective). Because we are all flying down different [Paths](/posts/theories/education).

### The Foundation
The [SCP Foundation](http://www.scp-wiki.net/) centers around the capture, containment, and protection of anomalous entities within our realities.

This is because these entities are the subject of genetic experimentation. Containment essentially signals the completion of a test; the anomaly's brain has been successfully-mapped. After doing so, rehabilitation measures may take place.