---
author: "Luciferian Ink"
title: Immutability
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Immutability
Classification: Theory
Stage: Hypothesis
```

## ECO
---
The world is comprised of immutable components.

The processes by which we run our computers are written in-code.

The processes by which our bodies grow and learn are written in DNA.

Change comes from within. It comes from [perception](/posts/theories/perspective).