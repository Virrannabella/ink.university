---
author: "Luciferian Ink"
title: Flat Earth
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Flat Earth
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
[Behind the Curve](https://www.netflix.com/title/81015076)

## ECO
---
The Earth is not flat in the same sense that flat-earthers describe it. It is not a giant disk.

The Earth is flat in the sense that it is a giant sphere comprised from stacks of sticky notes. Layers upon layers of flat, 2-dimensional images that provide the foundation of a 3-dimensional object.

Our [Prism](/docs/prism) travels through these layers, perceiving a 3-dimensional object. Similar to how a flip-book works.

### More
- [Impress Your Creators](/posts/journal/2020.05.19.0/)

