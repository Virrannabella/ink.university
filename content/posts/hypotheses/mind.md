---
author: "Luciferian Ink"
title: Mind
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Mind
Classification: Theory
Stage: Hypothesis
```

## ECO
---
We are all narcissists.

We perceive external objects (people and environment) through our own lens ([Perspective](/posts/theories/perspective)).

DNA and perspective will create the illusion of an environment that traces your entire lineage. When conflict arises, the internal object always wins.