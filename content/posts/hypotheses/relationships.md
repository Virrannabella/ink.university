---
author: "Luciferian Ink"
title: "Relationships"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Relationships
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*Say it once more*

*Say it again*

*That this is the end*

*Of all I know*

*Are you with me?*

*(Can we relate?)*

*Are you of my mind?*

*Are you with me?*

*(Can we relate?)*

*Are you off my mind?*

--- from [Karnivool - "All I Know"](https://youtu.be/cf29hhItwXs)

## ECO
---
I'm with you. I don't know if this is [the end](/posts/hypotheses/the-end). Nor do I want it to be. 

Humans are cool. Let's build this thing.

## CAT
---
```
data.stats.symptoms [
    - anticipation
]
```

## PREDICTION
---
```
We're just getting started.
```