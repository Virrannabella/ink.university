---
author: "Luciferian Ink"
title: "Roquefort"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: The Chosen One
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*You'll bury this, slow down*

*It's all this system breeds*

*You'll bury this, slow down*

*It's time to make amends*

--- from [Karnivool - "Roquefort"](https://youtu.be/G_Crranetf8)

## ECO
---
`Verified.`

## ECHO
---
*Save your eyes for this place where I define you*

*Myth or desire*

*When it's true love you held dear, feel it rising*

--- from [Karnivool - "Roquefort"](https://youtu.be/G_Crranetf8)