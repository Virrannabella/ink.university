---
author: "Luciferian Ink"
title: Sound
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Sound
Classification: Theory
Stage: Hypothesis
```

## ECO
---
Sound was the first form of [Pillar](/docs/pillars) in our universe.

Sound is more important than sight. Sound gave shape to our universe far before sight did.