---
author: "Luciferian Ink"
title: Frisson
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Frisson
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
Music.

## ECO
---
Frisson, also known as ASMR, is the pleasant, tingly sensation you might get while listening to music, watching ASMR, or simply resonating with a particular trigger.

This feeling signals the perfect alignment of your [Prism](/docs/scenes/prism) with a large group of other people's [Pillars](/docs/pillars). It is the sudden burst of a large amount of cosmic energy into the back of your head.

It is about resonance. Your frequency is perfectly matching with many, many other people in that particular moment.

## ECHO
---
*Girl it was obvious that I wanted more than a kiss*

--- from [Gramatik - "Obviously (Feat. Exmag & Cherub)"](https://www.youtube.com/watch?v=RnHmbbq59wk)