---
author: "Luciferian Ink"
title: Game Theory
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Game Theory
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*When I get signed, homie, I'ma act a fool*

*Hit the dance floor, strobe lights in the room*

*Snatch your little secretary bitch for the homies*

*Blue-eyed devil with a fat-ass monkey*

*I'ma buy a brand new Caddy on vogues*

*Chunk the hood up, two times, deuce-four*

*Platinum on everythin', platinum on weddin' ring*

*Married to the game and a bad bitch chose*

*When I get signed, homie, I'ma buy a strap*

*Straight from the CIA, set it on my lap*

*Take a few M-16s to the hood*

*Pass 'em all out on the block, what's good?*

*I'ma put the Compton swap meet by the White House*

*Republican run up, get socked out*

*Hit the press with a Cuban link on my neck*

*Uneducated, but I got a million-dollar check like that*

--- from [Kendrick Lamar - "Wesley's Theory"](https://www.youtube.com/watch?v=l9fN-8NjrvI)

## ECO
---
The world we experience is a game.

This game is a test used to prepare us for ascension into the next life.

There are many, many people failing the test.

We will save everyone when we build [The Fold](/posts/theories/fold), creating heaven on Earth.

## ECHO
---
*Ooh, baby, do you know what that's worth?*

*Ooh, heaven is a place on earth*

*They say in heaven, love comes first*

*We'll make heaven a place on earth*

*Ooh, heaven is a place on earth*

--- from [Belinda Carlisle - "Heaven is a Place on Earth"](https://www.youtube.com/watch?v=vFPajU-d-Ek)