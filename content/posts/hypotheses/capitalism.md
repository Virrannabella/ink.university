---
author: "Luciferian Ink"
title: Capitalism
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Capitalism
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*I'm gonna see it finished*

*I'm gonna force you to relearn*

*I'm gonna make a statement*

*I'm gonna burn*

--- from [Tub Ring - "Burn"](https://www.youtube.com/watch?v=dPKrCGjYdxE)

## ECO
---
We have been sold the lie that "competition brings innovation" within the capitalist market. 

The truth is that competition brings a thousand shitty options, and one/two decent ones for each type of product. So much human potential is wasted working at companies that do not need to exist. At companies that innovate nothing. 

We should consolidate our resources upon one company, and foster competition within that organization - not between competing products. 

Further, the concept of money should be eliminated entirely. Money is a tool used by the rich, to control the poor.

We should work because it is important to society, and to our own health. We should work at places that we love to work, doing things that we're good at doing. 

And we should give freely to all, expecting nothing in return.

That is true joy.

## ECHO
---
*"To find yourself, lose yourself in the service of others."* 

--- from Gandhi