---
author: "Luciferian Ink"
title: "U"
weight: 10
categories: "question"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: You
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*Paint*

*Me in your own way*

*Colour my eyes*

*See what feelings I hide*

*No-one knows why*

*No one can forgive*

*Second thought*

*Why do I care?*

--- from [Karnivool - "Umbra"](https://youtu.be/F8UuillO-Kc)

## ECO
---
Uluru was our star seed.

This is how [The Archons](/docs/candidates/the-machine/) came to colonize our planet.

After all, Australia was founded by criminals, right?

## ECHO
---
```
data.stats.symptoms [
    - empathy
]
```

## ECHO
---
*(Instrumental)*

--- from [Indukti - "Uluru"](https://www.youtube.com/watch?v=WvV8VLS5NAc)