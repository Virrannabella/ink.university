---
author: "Luciferian Ink"
title: Duality
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Duality
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*All I've got, all I've got is insane!*

--- from [Slipknot - "Duality"](https://www.youtube.com/watch?v=6fVE8kSM43I)

## ECO
---
The human consciousness is but one half of a duality. For every human, there exists a second that perfectly compliments the first. One cannot exist without the other (for very long, anyway.)

Initially, the two consciousnesses are kept separated by [The Machine](/docs/candidates/the-machine). Though this makes both miserable, it serves an important purpose; the separation allows each to document their realities separately. The overlap between their [Perspectives](/posts/theories/perspective) proves the theory of objective reality. It proves that consciousness is connected at a cosmic level.

When the two consciousnesses become deadlocked - reaching the pinnacle of [imbalance](/posts/theories/balance) - this signals the completion of the test. This [verifies](/posts/theories/verification) that the two are ready to be [Integrated](/posts/theories/integration).