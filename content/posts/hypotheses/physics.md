---
author: "Luciferian Ink"
title: Physics
weight: 10
categories: "hypothesis"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Physics
Classification: Theory
Stage: Hypothesis
```

## ECO
---
Our model of physics is loosely based upon string theory.

Essentially, there are many strings connected to [Prism](/docs/scenes/prism). At the back, strings connect to the center of the earth. At the front, strings spider out into the cosmos - usually ending at a point of light.

Prism is a traveling space ship, moving along these strings.

Direction changes easily and instantaneously when attention changes.

