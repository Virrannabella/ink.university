---
author: "Luciferian Ink"
date: 2019-01-06
title: "The Clinic"
weight: 10
categories: "audit"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: The Clinic
Type: Efficacy Audit
Auditor: Fodder
Date: December 2019
Result: F
```

## RESULT
---
### Synopsis
The Clinic is a mental health facility located just outside of Dallas, TX. Malcolm spent 21 days here before being discharged.

### Architecture
For $800/night, patients should be eating off of gold-plated tables. Conversely, this clinic is in many ways worse than the cheapest hostel:

- There are only two components: the shared rooms, and the common area.
- There is nothing to do, other than watch the TV that is placed much too high upon the wall, or coloring books.
- Everything is broken. From the water faucets that won't turn-on, to the medical equipment, to the soda machine.

### Processes

- Everything is dirty, grimy, and unkempt.
- The combination of filth, cleaning products, and poor ventilation is nauseating.
- Technicians are unable to take vital signs properly. The heart rate monitor is placed onto the same hand as the blood pressure monitor - invalidating the test.

### Therapy

- Therapists recycle the same useless paperwork on a two-week rotating basis. Never have I seen a greater waste of human potential. 

## Conclusion
The mental health system is an ineffective, bloated, administrative mess. It is a for-profit system that does not seek to rehabilitate, but instead to medicate.

Staff are overworked, and undereducated. Patients who can not ask for help, are not helped.

Truly, this is a broken system.