---
author: "Luciferian Ink"
date: 2019-10-11
title: "Cannon Fodder"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
A person, a tool, and a new podcast.

## ECO
---
I'm torn on this one. 

`$REDACTED` is really pushing for me to "buy-in." Clearly, he wants me to invest in his new change-mapping tool. I certainly have brushed it to the wayside - almost certainly because it relates to his area of expertise, and I can't stand the man. 

Still, I should try to be more charitable. There are other people working on this project, too. Whereas "trust, but verify" (a Russian proverb) has been my running directive for a while, I have been treating `$REDACTED`'s ideas differently; I have been requiring "[verify, then trust](/posts/theories/verification)."

The question is: which one is better? Should I change my directive?

### Trust, but verify
The former is flawed in many ways. Trust must be given before verification can be performed. This leaves an opening for a malicious actor to exploit that trust, circumvent verification, and ultimately manipulate the past. Which is exactly what `$REDACTED` is trying to do.

This model requires forward momentum through time. You must place trust, then wait for the time revision "loop" to come back around and impact you. This can takes days, weeks, or most often, never happen at all.

Mistakes made can have widespread and devastating effects. Entire worlds can be destroyed. This model is about the revision of history.

### Verify, then trust
In a "verify, then trust" model, one must establish trust with known-safe peers. Then, he must ask them to perform calculations first; if their responses fall within the "acceptable" range of responses, only then does the Node perform the final calculation. Trust has now been established with a new peer. 

In this model, you look at the past to determine how the future will be impacted. You do not place blind faith into others.

Mistakes made here are just part of the process. They are minimal and do not rewrite history.

### Conclusion
Clearly, the first option is flawed. `$REDACTED` could use that to exploit me.

I like my way better. 

In his model, money is power. In mine, security, agreement, and acceptance. It is clearly the superior theory. 

I should not buy-in to his tool at this time. 

Though... his behavior and comments are borderline woke sometimes. Like he knows far more than he's letting on. That he's not nearly as bad as everyone thinks he is. 

It's like... this is who he is, in my story. Nothing more, nothing less. We are literal opposites. He is holding me to a different standard, and I am holding him to a different standard. We are at a standstill. The only way to agreement is to trust - then verify.

Okay, now I'm freaked out. Do I need to place trust in Russia to figure out the truth to this thing?

## CAT
---
```
data.stats.symptoms = [
    - fear
    - confusion
]
```

## ECHO
---
```
($THOUGHT)      = -0.20 | # You really need to think carefully about what to do here.
($HYPOTHESIS)   = -0.60 | # You screw this up, and it'll be the end of your journey.
($INCONSONANCE) = +0.90 | # But if this works, you will have saved Humanity forever.
($NARCISSISM)   = +0.50 | # This isn't about me. This is about Humanity.
($KILL)         = -0.15 | # Can't. Too scared.
```