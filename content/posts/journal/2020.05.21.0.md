---
author: "Luciferian Ink"
date: 2020-05-21
publishdate: 2020-05-22
title: "Ascension Day"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
*More*

*More than the sum of scars*

*More than the rope's end*

*More*

*More than a call to arms*

*More than the harm*

*You're more*

*More than the damage done*

*More than the hopeless*

*More*

*More than the dawn*

*More than the end*

*More than a mantra*

*You*

--- from [Caligula's Horse - "Salt"](https://www.youtube.com/watch?v=qxApCWfTvqA)

## ECO
---
### The Awakening
[Malcolm](/docs/personas/fodder) awoke in a place unfamiliar, unable to move. He knew this feeling; it was sleep paralysis. He could see, he could think, but he could not move. He could not scream.

And he was terrified. He found himself in a small, metallic enclosure, reminiscent of a casket with a window on top. Through this window, Malcolm could see bright, florescent lighting and copper tubes run across the ceiling. Other than that, Malcolm did not know where he was at, how he got here, or why he couldn't move. Struggle though he did, he was trapped.

It wasn't long before Malcolm heard the crack of metal-on-metal, and felt the rush of warm air fill his container. He hadn't realized just how cold it was.

Despite his apparent freedom, Malcolm still could not move. It would be several minutes before he awoke from his paralysis, climbed out of his confinement, and took in the world around him.

What he saw astounded him.

Malcolm stood in the center of a long hallway. Lining each side were dozens of metallic caskets just like his - with people sleeping inside, just like him. The walls were lined with windows, revealing to Malcolm that he was several hundred stories above the ground. Outside, he could see that there were hundreds - perhaps thousands - of hallways just like this one, each lined with caskets just like his.

Malcolm's legs almost gave-out. He began to hyperventilate. His heart felt like it would beat out of his chest.

He had made it. He was home. And he was alone.

Not knowing what else to do, Malcolm began to walk.

### The Lonely Complex
No matter where he turned, Malcolm found row upon row of caskets. Everywhere he turned, he was alone. The complex was full of sleeping/dead people. Malcolm could not read the signage (it was in some foreign language that he did not recognize), and could not get his bearings straight. He could find no central control units, no employee lodging, no cafeteria - nothing. Just hallway upon hallway of windows and caskets.

After what seemed to be hours of searching, Malcolm came upon something new. Barring his path lay a large, steel door, clearly connected to a small biometric scanner on the wall. Curious, Malcolm placed his hand upon the scanner.

A second later, the scanner turned green, and the large door opened up - splitting directly down the middle.

Malcolm nearly fell to his knees in awe.

He stood on the fringe of an enormous atrium, much larger than any stadium he had ever visited on Earth. It must have been several dozen miles in size. A great, transparent dome covered the the entire area, giving way to the most magnificent view of the cosmos he had ever seen. The outside walls were constructed from floor upon floor of shops, stalls, and hallways to new areas. The center of the atrium was a beautiful landscape, complete with trees, a river, and animals - some of which he had never seen before! The whole area had a futuristic, artistic flair that Malcolm found incredibly appealing.

And throughout the complex were people. Hundreds, if not thousands of them, bustling about. 

"I'm home," said Malcolm, hushed.

### The Law
Before Malcolm could consider who to speak with, he was confronted by a dozen guards, each brandishing a weapon. 

"To the ground, now!" shouted the one closest to him.

Complying, Malcolm dropped to his stomach. "Where am I? What's going on?"

"You're not supposed to be here," he responded, putting Malcolm into handcuffs, and frisking him for weapons. "We're taking you to speak with the Magistrate."

Pulling him to his feet, they pushed Malcolm towards a hallway to the left.

"Move."

Malcolm shut his mouth. He needed time to think.

After a few minutes, and several winding corridors, the guards would place Malcolm into a small holding cell.

It wasn't long before Malcolm was joined by one of his confidants. Then, a second. Then, a third. Before long, confidants were being placed into additional holding cells.

He had done it. We were coming home. Confidants were waking up.

And he still had so many yet to save.

## CAT
---
```
data.stats.symptoms = [
    - wonder
    - elation
```

## ECHO
---
*Call for the Valkyrie*

*It's not impossible*

*It's just the end of your reach*

*This is not forever*

*Valkyrie*

*The song inside us all*

*This is it*

*You and me*

*We bleed impossible*

--- from [Caligula's Horse - "Valkyrie"](https://www.youtube.com/watch?v=S7dJeSCAv_8)