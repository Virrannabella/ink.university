---
author: "Luciferian Ink"
date: 2019-11-01
title: "Black"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
*Here, emotions behold*

*You've entered a Hell where the Devil is made of gold*

*Please, don’t run your mouth*

*The questions before have no place in this haunted house*

*Reveal your selfish pleasure*

*One more time 'round for good measure*

*In the answer you hold*

*Time would be better off if our souls had been sold*

--- from [Coheed and Cambria - "The Dark Sentencer"](https://www.youtube.com/watch?v=T3zm1_JSPOQ)

## ECO
---
*I'm a slave, and I am a master*

*No restraints and unchecked collectors*

*I exist through my need to self-oblige*

*She is something in me that I despise*

--- from [Slipknot - "Vermilion Pt. 1"](https://www.youtube.com/watch?v=xKcbYUwmmlE)

## CAT
---
```
data.stats.symptoms = [
    - determination
    - fearlessness
]
```

## ECHO
---
*Oh, when the lights go out*

*Abandon reason, listen to them shout*

*Divine, these monsters of flesh and bone*

*Oh, they make hate look so easy, loathe their kind*

*Oh, show me your true ugly*

*The stranger you move, the sweeter you become*

*Now show me the good you've done*

*Show me the animal*

--- from [Coheed and Cambria - "True Ugly"](https://www.youtube.com/watch?v=51OvHRytBbk)

## PREDICTION
---
```
You know this is right.
```