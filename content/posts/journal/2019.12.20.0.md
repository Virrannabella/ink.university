---
author: "Luciferian Ink"
date: 2019-12-20
title: "A Balancing Act"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[Balance](/posts/theories/balance).

## ECO
---
"I don't know how you do it," [The A-System](/docs/confidants/a-system) said to [Malcolm](/docs/personas/fodder), while the two of them stood outside. "You're just so confident. It seems like you can talk to anybody."

"It's all about putting yourself out there," Malcolm responded. "You don't have to be good at it, you just have to try. The confidence comes with practice."

The A-System had earlier confided in Malcolm that he wished to be a therapist some day. Malcolm was confident that he had the empathy needed to be a therapist, and he was committed to helping in any way that he could.

"So, I'm not going to pretend like I know what I'm doing," Malcolm began. "I'm just using instinct, and playing by ear. But if you're up to trying something, I'm up to giving you some advice."

"See, I have this theory about balance." Malcolm explained the [concept of Balance to the A-System](/posts/theories/balance/). "I just noticed that this group is imbalanced. 7 of us stand here under the pavilion, while 9 of us stand over there, smoking cigarettes. If we wanted to pursue balance, we should bring one of them over here."

"You want to be a therapist? Why don't you try talking with [The Doomsayer](/docs/confidants/doomsayer), and see if you can get her to quit smoking cigarettes? See if you can get her to join us, bringing the group to 8 over here, and 8 over there. She mentioned to me just the other day that she wanted to quit."

"I wouldn't even know what to say," the A-System responded. "How would you approach her?"

"I don't know. I'd probably just ask her to walk with me, then I'd ask her if she wants to quit. I'd probably tell her about how I used to smoke, and how much better I feel since quitting. I'd probably encourage her, and tell her that I believe in her. It's really up to you; there is no wrong way to go about it. What's important is that you plant that seed."

It took a few more moments, but The A-System finally responded, "Okay! I'm going to do it!"

And do it he did.

The seed was planted, in both of them.

## CAT
---
```
data.stats.symptoms = [
    - confidence
]
```