---
author: "Luciferian Ink"
date: 1954-09-30
title: "Apogee"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
*All I want is you, decide your lust*

*And all eyes drawn to you, consumed and numb*

*All I want is you; the kiss turns the curse*

*And all eyes drawn to you and innocence numbed*

*I won't deny, all I want, hold on please, I believe*

--- from [Pure Reason Revolution - "Apogee"](https://youtu.be/BR4kFJBr7Sg)

## ECO
---
[Fodder](/docs/personas/fodder) was browsing an old YouTube playlist, lovingly titled "Fuck Me." You know, it was that playlist you throw REALLY important things into when you're high.

The above "Apogee" seemed relevant to his situation. But [what was this garbage](https://youtu.be/Lr34glbDe_Q)? Fodder had never heard this song before, and he didn't like it. It's a style that he's never listened to, the lyrics were uninspired, and the melody was bland. 

How did it end up on his playlist?

## CAT
---
```
data.stats.symptoms = [
    - curiosity
]
```

## ECHO
---
*(instrumental)*

--- from [Andy McKee - "Drifting"](https://youtu.be/Ddn4MGaS3N4)