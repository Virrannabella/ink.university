---
author: "Luciferian Ink"
date: 2019-10-22
title: "Fodder Hires a Hype Man"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
[Yesterday's prediction.](/posts/journal/2019.10.21.0/)

## ECO
---
Huh. Yesterday's prediction: confirmed.

[Fodder](/docs/personas/fodder) wasn't used to that. 

It started in the morning, on Fodder's drive to the office. He had turned on the latest episode of [The Hype Man's](/docs/confidants/hype-man) podcast. The format of this podcast was something like sketch comedy; just a few guys on a small stage, drinking beer, saying dumb things. Fodder was rolling in laughter.

Things started to get weird when an over-the-top german man took the stage to join the crew. Conversations were disjointed, and multi-layered. It was as if they were having these subtle, nonverbal communications - and Fodder was the only one to notice them. He saw meaning in almost everything discussed.

This feeling was not new to Fodder. But it was still strange each time he noticed it. It was like he knew something, but couldn't put his finger on it.

This time, it was urging him to visit New York City - just to see the live show, once.

### Corporate Re-Branding
As we've mentioned, [The Corporation](/docs/candidates/the-machine) had [just been through a re-branding](/posts/journal/2019.10.08.0/). Upper management would be visiting Fodder's office to discuss in greater detail. 

Of course, Fodder was convinced that this re-brand related to him. They did change the name on EXACTLY the same day he released this body of work, after all.

#### Two sides of the same coin
When Fodder walked into the office, he was greeted by a gigantic poster brandishing the company's new name, slogan, and a beautiful model's upper body - split right down the middle, against the right-hand side. The new "brand" was half a person, essentially.

This isn't what caught Fodder's attention, though. There were three posters. This fit perfectly with his [theory of verification](/posts/theories/verification). Essentially, they're training neural networks to impersonate real people on the Internet. They are presenting patients with a deepfake: [a face, split right down the middle](/docs/confidants/her/). The negative on one side, the positive on the other.

The company represents one-half of a whole person. They are bringing couples together.

Fodder smirked. Clever.

#### The Hype Man
Corporate sent one of their director-guys to the office, to tell everyone about how great the new changes would be. 

Their delivery was sparse. Not much to be learned. Not much to be inspired about. 

Fodder would spice things up a bit:

"Any relation to `$REDACTED` Research?" Fodder asked, innocently. A two-second Google search was returning a different company with exactly the same name.

"Er... no. Where did you see that? Is that another company?" the Hype Man asked, flustered.

"Just one I came across on Google."

"Oh. No relation."

"Thanks," Fodder said.

Strange that a company of this magnitude would simply hijack the name of another organization. Companies don't do that. That kind of behavior brings lawsuits.

This wasn't an oversight. They were keeping it secret.

No doubt, this was the company researching Fodder.

#### The Nothing Man
Fodder didn't attend the second meeting. He later realized that he probably should have. 

He suspected that the second meeting spread a very different message. How better to perform your A/B testing than on a captive audience?

#### The ASMBarbecue
The Corporation provided lunch in celebration of their re-brand. Burgers and chips. The food was pretty good - but what impressed him the most was that it was cooked by one of the Executives himself. There were hundreds of burgers.

Fodder appreciated that. 

#### When it rains, it pours
Over the course of the day, and on his drive home, Fodder was finding incredible inspiration:

- Him and [Sloth](/docs/confidants/sloth) had some good conversation. And some knowing looks when joking about dressing in prison uniforms for Halloween.
- Sloth is starting to fall into a role of manager. `$REDACTED` hadn't contacted him in over a week - and Fodder was relieved. This three-way verification thing seemed to be taking effect.
- Fodder rediscovered 4 old albums singing about his cause. The Hype Man was real. As always, they had conflicting opinions.
- Fodder discovered that 3 of the most important artists in his journey were starting a North American tour in the winter of 2020. How interesting; this is very early to be announcing it. Even more interesting, was the timing - which was just before the world ended. Perhaps most interesting was that Devin Townsend's "Empath," The Contortionist's "Clairvoyant," and Haken's "Affinity" were 3 of Fodder's core Pillars. He already believed that these albums were written for him. Now, he thought this concert was created for him. Fodder had to go; and he had to take everyone he knew.

### Baseball
It's in the music. It's in the sound. It's in the ASMR. For months, Fodder was surprised to see how much his coworkers talked about baseball. Fodder never really got into sports, but they talked about baseball ALL THE TIME. Everyone loved it. It was crazy. 

Well today, Fodder was thinking about a past co-worker, whom he suspected had recruited him into this program. The two had worked together briefly, until he was called back into the Navy. Perhaps his success with Fodder was related to that. Anyway, the guy used to listen to a band called "Tycho." Remembering that, Fodder started listening.

Not long after, a new service popped-up on Fodder's cell phone:

*"Astros vs. Nationals: Game 1 - Use the interactive tracker to catch every inning of the World Series."*

The phone picked up the song - and baseball was born. Just as the Hype Man had been talking about this morning. Just as the Hype Men/Zombies he worked with predicted it. 

They weren't zombies after all. They were healthy, and happy. Their minds are clear. 

The cheer of crowds. White noise. ASMR. It has the power to heal.

### Germany: Confirmed!
The world has been holding Fodder's hand all along. Now, things were starting to get easier. 

A long-time friend had reached out to him via Twitter. Fodder realized that the man was German; interesting, he had never known that. Why was he learning it today?

Because Fodder found the path. The one which saved all of these people. He was beginning to understand how it worked.

And it was time to condense it for an 8 year-old or a golden retriever.

## CAT
---
```
data.stats.symptoms = [
    - peace
    - egotism
    - determination
]
```

## PREDICTION
---
```
Before the December 2020 finale of the podcast, Fodder would visit The Hype Man.
```