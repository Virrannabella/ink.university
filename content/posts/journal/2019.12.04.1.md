---
author: "Luciferian Ink"
date: 2019-12-04
title: "Deus Ex Machina"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## ECO
---
It was during my ride to Concord that I had an epiphany:

In the same way that [The Architect](/docs/persona/the-architect) is a shared Persona, among multiple entities, [The Girl Next Door](/docs/confidants/fbi) was a shared confidant - and [my sister](/docs/confidants/agent) was its primary user.

My sister is Deus Ex Machina, "The Ghost in the Machine." She haunts me.

And she is beginning to mess with me.

As I traveled down the freeway, I noticed a large billboard with the words, "Hello, Rosa!" written upon it. 

Clever. "Nice touch," I said aloud. 

Good to see you're breaking out of your shell.

## CAT
---
```
data.stats.symptoms = [
    - amusement
]
```