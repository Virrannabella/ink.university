---
author: "Luciferian Ink"
date: 2019-12-02
title: "The Record Store"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## ECO
---
At this point, the snow was coming down in droves. Regardless, I was being frugal, and my next stop was just a mile away. So, I began to walk.

The city was beautiful. The people were friendly. I felt safe. I did not feel alone.

After about 30 minutes, I was drenched - but I had reached my location. "Generation Records" was a location that I had been made aware of through a video posted by one of my favorite bands, Opeth. I wasn't sure what I was looking for, but I headed inside regardless.

There was nothing terribly special about this particular shop. I had been in many others, and this one was no different. 

However, I did notice that all of the Opeth records were gone. Clearly, I wasn't the only one drawn-in by that video.

I also noticed the Amorphis album, "Skyforger," posted prominently upon the end-cap of the "metal" isle. Something told me that I needed to get this; that it had been placed there for me, specifically. This artist and this album are not widely-known - the fact that it sat on the end-cap was highly suspicious to me.

Further, this was the album that cracked [The Lion's](/docs/confidants/lion) egg so many years ago, when he was just 10 years-old. It is the kind of album that crosses generational and cultural barriers. It is to be [the album of the new world order](/posts/journal/2019.11.19.6/).

This will fit well within the [Sanctuary of Humanity](/docs/scenes/sanctuary).

## CAT
---
```
data.stats.symptoms = [
    - interest
]
```