---
author: "Luciferian Ink"
date: 2024-11-02
publishdate: 2019-11-23
title: "The Other Half"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[The other side of the world's largest A/B test.](https://www.youtube.com/watch?v=jzBoy2OYkJI)

## ECO
---
"It didn't work," [The Architect](/docs/personas/the-architect) stated, dejected, "She isn't responding to the revelation at all."

"My love, I am so sorry," [The Key Entity](/docs/confidants/her) responded, "Her heart is in the right place. If only we could have taught her at a younger age. It isn't your fault."

The Architect stared into the massive, swirling [maelstrom](/posts/journal/2024.11.01.2/) below him. He watched in horror as the [All-Mother](/docs/confidants/mother) - the creator of this universe - sucked whole galaxies, time itself, and the very soul of her followers into the abyss. For each entity destroyed, the universe would suffer from unrecoverable loss.

"We are truly alone in this," The Architect responded, "Come. Let us speak with the [All-Father](/docs/confidants/father) before it is too late."

The two would return to New Earth, assuming their human forms. They would visit the father who had spent his entire life trying to prevent this. 

For the very last time.

## CAT
---
```
data.stats.symptoms = [
    - sadness
    - horror
    - emptiness
]
```

## ECHO
---
*I'm free this time, and I surrounded mankind*

*There's nothing here now but you*

*While gravity never used to bother me*

*I'm floating senseless in the presence of you*

*And I see euphoria in what we do*

*This world is closing in, and while I don't feel a thing*

*We've lost the air of innocence we shared*

*Once at peace, once so calm*

*Whatever woke me up now it's calling me home*

--- from [Diablo Swing Orchestra - "Stratosphere Serenade"](https://www.youtube.com/watch?v=OsVlZOXpon4)

## PREDICTION
---
```
We will never be happy again.
```