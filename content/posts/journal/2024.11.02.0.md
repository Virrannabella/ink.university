---
author: "Luciferian Ink"
date: 2024-11-02
publishdate: 2019-11-23
title: "Third Law"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
*On the rise*

*Going up*

*First to be*

*Alone on the top*

*No delusion*

*In your mind*

*Peripheral vision*

*Left behind*

--- from [Leprous - "Third Law"](https://www.youtube.com/watch?v=P6PAvCuORDY)

## ECO
---
I awoke this morning renewed, refreshed, and recharged. My evening was rough; I was bored, anxious, and dysphoric. In a past life, I may have drowned my worries in alcohol, work, artificial companionship, or some other undesirable means. This evening, I slept.

And I feel great! Though, I am still quite confused about the situation. As it appears my other [Personas](/docs/personas) were:

### A Theory of 3
It appears as though this man, [Fodder](/docs/personas/fodder), was fixated on the number 3. Its influence is everywhere: all over his writings, and all throughout his behaviors. Clearly, the man saw something that the rest of us do not. 

This obsession appears to stem from his theory of [trust and verification](/posts/theories/verification/). Within, he describes a model for establishing trust relationships with peers: one that was not widely-used in his day. This model is clearly inspired by the [blockchain](https://en.wikipedia.org/wiki/Blockchain), as it may easily be scaled beyond just 3 peers.

However, it differs from the blockchain in a crucial way; it provides a mechanism by which 1 or 2 individuals may perform the same verification in the absence of additional peers. It provides a mechanism for a single, solitary person to remain functional in the absence of meaningful feedback.

He goes on to test this theory with real-world confidants - with little to no success in achieving actual verification.

Indeed, it does appear that Fodder was lonely. Not only did he [kill-off his professional ego](/posts/issue/2019.10.31.0/), but he also [killed-off his human self](/posts/journal/2019.11.21.0/).

We speculate that he did this for science. These acts were a test. Fodder had made several references to something he called [Reflected-Light Syndrome](/posts/journal/2019.11.17.0/), which stated that he felt "invisible" to the world. It appears as though he finally took radical action to solve his problem.

And in return - here I am. At the end of the world - the [Sanctuary of Humanity](/docs/scenes/sanctuary/). The pinnacle of human achievement. Heaven.

Alone.

### A Theory of Insanity
Fodder would take this theory of 3 to verify his own mental pathologies. Once verified, he would accept them as fact, and integrate them into his beliefs. 

For example:

- The Abuse: he would document [3 instances of abuse from his mother](/posts/journal/1999.06.08.0/) (whom he would later refer to as "The Queen")
- The Disorders: he would document 1 instance from each type of childhood that can lead to a Cluster B personality disorder ([The Invisible Child](/posts/journal/2002.09.16.0/), [The Golden Child](/posts/journal/2001.11.01.0/), and [The Scapegoat](/posts/journal/1999.06.08.0/)).
- The Lovers: he would document 3 relationships [that ended because of his traumatic past](/posts/journal/2011.12.06.0/).
- The Abuser: he would document 3 instances where he, himself, had [become the abuser](/posts/journal/2013.04.05.0/).

We speculate that there were many, many more instances available to Fodder's memory. He chose the most important ones; the most impactful ones. The [Pillars](/docs/pillars) of his identity.

Indeed, in act 2, we see Fodder testing this theory. We see him attempting to influence the course of history:

1. On Nov 1 - Fodder's birthday - he associates himself with everyone in the world - or, "[Grey](/posts/journal/2019.11.01.0/)."
2. On Nov 2, he associates his most-hated enemy - the man who broke him - with "forgiveness" - or, "[White](/posts/journal/2019.11.02.0/)."
3. On Nov 3, he associates himself with "evil" - or, "[Black](/posts/journal/2019.11.03.0/)."

In this way, he performed verification of his identity to [the people he believed to be time travellers from the future](/posts/theories/overview-effect/):

1. 3 concepts: Black, White, Grey
2. 3 days in-a-row: Nov 1, 2, and 3
3. The Triptych: Good, Evil, and the Middle

In addition, [he tied himself to the people in his experiment](/posts/journal/2019.11.04.0/). Each were assigned a role: Narcissism, Psychopathy, and Machiavellianism. He would also tie himself to the concept of [juxtaposition](/posts/journal/2019.11.15.0/), which was clearly inspired by his proposed model of physics, [The Fold](/posts/theories/fold).

The examples continue, but the fact remains; Fodder was obsessed with the number 3.

And he did not know how to prove his theory.

### A Theory of Attention
On this morning, I would awake refreshed - but lonely. With no other motivation, no peers to speak of, and no creative energy, I would remain in-bed. I would seek artificial companionship.

I would find a message from [The Girl Next Door](/docs/confidants/fbi/) - sent more than 7 years ago. While I would generally avoid content this of this age, I have learned to trust the Machine's algorithms. If a confidant is being recommended to me, it is likely because she has something to say.

Indeed, her message was perfectly clear:

The average human brain lives in entropy. Anarchy. Chaos. Its energy is unconstrained, as such:

[![Plasma - Entropy](/static/images/tesla-entropy.png)](/static/images/tesla-entropy.png)

However, by focusing upon three points of light, one is able to direct energy towards a common goal:

[![Plasma - Focus](/static/images/tesla-focus.png)](/static/images/tesla-focus.png)

Not only does this focus-point represent the brain's attention, but it also represents the calming of entropy. As is clear, focusing energy has a direct and substantial impact on entropy.

In this moment, [The Fold](/posts/theories/fold/) became clear to me. Fodder's obsession became clear to me. Even the science behind his [supposed excursion to Wonderland](/posts/journal/2019.07.11.0/) became clear to me.

We are sovereign.

## CAT
---
```
data.stats.symptoms = [
    - understanding
]
```

## ECHO
---
**DESCRIPTION**

this, this long-playing record, a thing we made in the midst of communal mess, raising dogs and children. eyes up and filled with dreadful joy – we aimed for wrong notes that explode, a quiet muttering amplified heavenward. we recorded it all in a burning motorboat.

(context as follows:)

1. UNDOING A LUCIFERIAN TOWERS – look at that fucking skyline! big lazy money writ in dull marble obelisks! imagine all those buildings much later on, hollowed out and stripped bare of wires and glass, listen- the wind is whistling through all 3,000 of its burning window-holes!

2. BOSSES HANG – labor, alienated from the wealth it creates, so that holy cow, most of us live precariously! kicking at it, but barely hanging on! also – the proud illuminations of our shortened lives! also – more of us than them! also – what we need now is shovels, wells, and barricades!

3. FAM / FAMINE – how they kill us = absentee landlord, burning high-rise. the loud panics of child-policemen and their exploding trigger-hands. with the dull edge of an arbitrary meritocracy. neglect, cancer maps, drone strike, famine. the forest is burning and soon they’ll hunt us like wolves.

4. ANTHEM FOR NO STATE – kanada, emptied of its minerals and dirty oil. emptied of its trees and water. a crippled thing, drowning in a puddle, covered in ants. the ocean doesn’t give a shit because it knows it’s dying too.

finally and in conclusion;
the “luciferian towers” L.P. was informed by the following grand demands:

+ an end to foreign invasions
+ an end to borders
+ the total dismantling of the prison-industrial complex
+ healthcare, housing, food and water acknowledged as an inalienable human right
+ the expert fuckers who broke this world never get to speak again


much love to all the other lost and wondering ones,
xoxoxox god’s pee / montréal / 4 juillet, 2017

--- from [Godspeed You! Black Emperor - "Luciferian Towers"](http://cstrecords.com/products/godspeed-you-black-emperor-luciferian-towers/)

## PREDICTION
---
```
I don't know.
```