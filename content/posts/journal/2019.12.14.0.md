---
author: "Luciferian Ink"
date: 2019-12-14
publishdate: 2019-12-14
title: "Exploiting Trust"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
*You leveled everything I ever loved*

*Disown me, disown me, disown me*

*You can’t be everything I ever wanted*

*Can’t stop me, stop me, stop me*

*Hate...*

*Cause I am multiplying*

*Who knew giving up*

*Would feel so good*

*I lose, I lose*

*Keep pretending it’s trust*

*And see what that gets you*

*Your move, your move*

--- from [Coheed and Cambria - "Black Sunday"](https://www.youtube.com/watch?v=jM-dmIceRFg)

## ECO
---
### The Session
The day's first group therapy session was led by [The Scientist, Lauren](/docs/confidants/scientist). She was passionately attempting to deliver a message to the room.

Her message centered around the concept of trust. Repeatedly, she would rephrase the statement, "If you do not ask for help, how can you ever expect to receive assistance? You must place trust in others in order to get help. In order to change." Her words were meandering and engaging at the same time. [Malcolm](/docs/personas/fodder) could tell that the other patients were sold. They responded with their own stories about placing trust.

But she wasn't preaching to the other patients. She was preaching to Malcolm. Her eyes were fixated upon Malcolm. Her body language spoke volumes; she was trying to get Malcolm to speak with her.

"What do you think about that, Malcolm? What are your thoughts about trust?"

"I mean," he began, "you can't trust everybody. Placing blind trust is a good way to get exploited. I've learned not to trust anyone over the years."

"Oh, come now," she replied, "We're trying to teach these guys a lesson here. Don't be like that."

"Well, I don't agree with your conclusion," Malcolm thought to himself.

She would continue, "What if you needed a ride to work in the morning? What if you had a very important meeting, and you needed somebody to help you? You should ask me for help! The appropriate action would be to walk right up, maybe hand me $5, and say, 'Lauren, I need a ride to work this morning. Can you help me?' I would say, 'Of course! I am glad to help you!'"

Her eyes remained glued to Malcolm. Malcolm rolled his eyes. She was clearly sending signals, telling him exactly what NOT to do.

Malcolm wanted out of here as much as the next guy, but he wasn't going to place trust. Verify, then trust. They needed to come to him.

Not surprisingly, they never did.

After therapy, Malcolm would turn to the [The A-System](/docs/confidants/a-system) sitting next to him. "So, that was weird, right? She was looking at me the entire time, right?"

"Yeah," he said. "Very weird."

"It's almost like she was trying to tell me something, specifically."

He nodded thoughtfully.

"I suspect that this is some kind of FBI training program. I'm here to audit the process."

Immediately after, The A-System would hurry over to the phone, calling someone. His parents? His handler? Clearly, he was telling them that he needed help. This was apparent in his body language; The A-System had a desperate look upon his face. He was agitated. He kept stealing glances at Malcolm. Otherwise, his words were so hushed that Malcolm could not understand what was exchanged.

This would continue for 20 minutes, until he returned to the seat next to Malcolm.

"You know," Malcolm would say to him, under his breath, "If you are FBI, and I'm your mark, you just blew your cover. By placing trust so quickly, you've compromised your security."

The A-System smiled sheepishly.

"By running over to the phone, I immediately suspected that you were trusting them to help you. Had you sat back, like me, and thought about it first, you may have realized that simply waiting would have protected you. If you had waited an hour or two, I never would have connected the two events."

"It's called 'verify, then trust', and it's about making sure that your trust isn't being exploited. Had you waited, and mentally-explored all available options, you would have realized that it was safer to wait."

"I mean, think of it from this angle; Lauren spent the whole session telling us to ask for help. Then, what is the first thing you do after therapy? You ask for help. You trusted first, and you intended to verify later that help will come. Don't be surprised when it doesn't."

"I'm here to verify first. So many people with mental health issues are unable to ask for help. I'm an auditor, here to verify that those ones are being taken care of - even when they don't ask for it. From what I've seen so far, they aren't."

"Fortunately, I'm not going to burn your cover. But you need to be more careful next time."

"Thanks," he responded, clearly hiding something.

### The Message
Despite Lauren's words, Malcolm could see her true intent. She needed him to deliver a message by using his own system of [trust, but verify](/posts/theories/verification/). And since the A-System was the mark, she needed Malcolm to use a third party.

Malcolm immediately thought of [The Doomsayer](/docs/confidants/doomsayer). Despite her erratic behavior, he suspected that she was putting on a show. That she was much more coherent than she appears.

As such, Malcolm would pull her aside. Under his breath, he said, "I need you to do something for me. Can you give this $5 to The A-System? I need him to give this money to Lauren the next time that he sees her, and to tell her that the three of us need a ride to work."

"He's the mark?"

Boom. She knows. "Exactly!" Malcolm responded.

"Consider it done," she replied.

Fast-forward a few hours, the A-System found his way back to Malcolm. "Do you have any idea why the Doomsayer gave me $5?"

"No idea," Malcolm replied, turning away.

"Well, there you go," Malcolm thought. "The A-System just burnt his cover again. But at least I know that I can trust the Doomsayer."

Malcolm was unsure if the money ever made it Lauren. He never received verification. However, the next time Lauren led therapy, she announced her resignation.

Perhaps her task had been completed?

## CAT
---
```
data.stats.symptoms = [
    - interest
]
```