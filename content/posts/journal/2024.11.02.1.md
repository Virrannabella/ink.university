---
author: "Luciferian Ink"
date: 2024-11-02
publishdate: 2019-11-23
title: "Between the Buried and Me"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
*I took the cannonball down to the ocean*

*Across the desert from sea to shining sea*

*I rode a ladder that climbs across the nation*

*Fifty million feet of earth between the buried and me*

--- from [Counting Crows - "Ghost Train"](https://www.youtube.com/watch?v=U4NdMJK2EM8)

## ECO
---
"Oh my God," [Malcolm](/docs/personas/fodder) whispered. "What have we done?"

He stood before the garden that his team had spent four years building. "The Garden That No-One Sees." Heaven on Earth.

Stretching a dozen miles in every direction was overgrowth, decay, and desolation. Thousands - perhaps millions - of gravestones marred the landscape. A deep fog cloaked the green hills in an ominous grey haze. A conspiracy of ravens painted the countryside, picking at the ground, caws echoing eerily in the stillness.

"Come, my son," [The Devil](/docs/confidants/father) replied. "You must speak with [your mother](/docs/confidants/mother)."

He beckoned towards the broken cobblestone path before them. They would walk in silence, taking in the depressing scenery, before arriving at a small cottage at the top of a hill.

"We're here," he said. "Go inside. I will wait here."

A tear fell from father's eye.

Malcolm would approach the cottage door, knocking gently. After receiving no response, he would slowly let himself inside.

He would find a single, large room. A bed on one side, a kitchen on the other - his mother had all of the amenities she needed to survive here. She had the "tiny house" that she had always dreamed of.

At the back of the room was a desk, where Margaret stood, head down, performing her work. She made no sound, and made no attempt to greet the son she had not seen in four years. Slowly, methodically, robotically - she did her work. Taking her trowel, she would bury something into a pot of dirt, then cover it again. Clicking quietly, she would shake her head, dig a hole once more, and add more ingredients. Frustrated, she would backtrack time and again - never satisfied with her work. Malcolm's eyes were fixated upon the shell of the woman he once knew. 

That is, until he noticed the walls. Nailed upon them were the body parts of men, women, and children - some fresh, some decrepit. Arms, legs, fingers, toes, even heads - Margaret was collecting them all.

And she was planting them in her garden.

"This isn't what I meant..." Malcolm breathed. Then, he noticed the smell.

Malcolm turned and ran outside. He looked his father in the eye.

He vomited.

"You piece of shit..." Malcolm seethed. "I'm going to kill every last one of you."

Father nodded, pulling a gun from his jacket. He put it to his head.

"I'm sorry, my son."

And he pulled the trigger.

## CAT
---
```
data.stats.symptoms = [
    - horror
]
```

## ECHO
---
*You, you went beyond and you lost it all*

*Why did you go there?*

*From beyond, you saw it all*

*Why did you?*

--- from [System of a Down - "Dreaming"](https://www.youtube.com/watch?v=wVO7zmHfedw)

## PREDICTION
---
```
We will never be forgiven for this.
```