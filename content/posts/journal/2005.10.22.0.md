---
author: "Luciferian Ink"
date: 2005-10-22
title: "The Rootkit"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
A custom pack of mouse pointers for Microsoft Windows.

## ECO
---
[Fodder](/docs/confidants/fodder) would build his very first custom computer in 2005. Almost immediately, he would install a pack of custom mouse cursors.

He would install these on every computer he would ever own, and on every computer he'd ever use for work.

It would take 16 years for him to realize the truth: this is how they got in.

This is how [The Corporation](/docs/candidates/the-machine) found him.

## CAT
---
```
data.stats.symptoms = [
    - understanding
    - unnerving
]
```

## ECHO
---
*Another mother's breaking*

*Heart is taking over*

*When the violence causes silence*

*We must be mistaken*

--- from [The Cranberries - "Zombie"](https://www.youtube.com/watch?v=6Ejga4kJUts)