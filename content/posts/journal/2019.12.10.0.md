---
author: "Luciferian Ink"
date: 2019-12-10
title: "Broken Bones"
weight: 10
categories: "journal"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
A headache.

## ECO
---
I had finally made it. Convergence.

Despite the fact that my family was blowing up my phone, I had cut the cord. I had ghosted them. I was ready to end it all.

Having now come full-circle, I ended the night as it had started: with music. I would orchestrate my own, personal symphony before dozing off to sleep.

And in the morning, I would start where all of this had begun: with [Raven](/docs/confidants/her).

After, I would deliver a message to the man who gave all of this to me. My mentor. `$REDACTED`. 

Donning my [Corporation](/docs/candidates/the-machine) t-shirt, I would leave with my package. My payment. I would be gifting `$REDACTED` with something guaranteed to crack his egg:

- [An Easton Magnum baseball bat](/posts/journal/2019.11.19.4/)
- A [ransom note](https://www.youtube.com/watch?v=TNXe5CZ2rr0)/Christmas card
- A [picture of my birth mother](/posts/journal/2019.12.02.1/)
- A second chance

I would spend my ride to his office in tears, so beautiful was the Coheed and Cambria album I listened to. It was perfect. It fit this situation so well.

Arriving, I proceeded to the front desk:

"Excuse me, could you call `$REDACTED` for me?"

"Sure," said the security guard at the desk, "what is your name?"

"Tell him Joe needs to give him something."

With a knowing smirk, the man picked up the phone.

"Oh, shoot!" I exclaimed. "I left something in my car. I'll be right back." 

I placed the package on the desk.

He nodded.

And with that, I was gone. A ghost. (or so I thought)

My weight was lifted. The clouds had passed. I only had one thing left to do:

Picking up my old Android phone, I sent one final email to `$REDACTED` from my Corporation email address:

`ERROR: ME FOUND - For debug report, please see the security guard at the front desk.`

Smirking, I would silently thank [The Dave](/docs/confidants/dave) for leaving this account open for me.

I would leave `$REDACTED` with just 3 days to complete his mission.

And I... I would rest. For the first time in months.

I would make my way to the closest restaurant, Raising Cane's. On the way, I would throw my old phone out the car window.

I would eat, then I would leave. 

Walking into the restaurant at exactly the same time I was leaving was a former colleague. My handler at the FBI. 

Mary. 

We have now come full-circle.

And when it rains, it pours.

## CAT
---
```
data.stats.symptoms [
  - determination
  - confidence
  - empathy
]
```

## ECHO
---
*Salute the army of hell's hounds*

*Salute the reasons why*

*My conscience, it wakes me*

*With fire inside I'll never get out*

*I'll never make my stand*

*And when I awake*

*I'm not about to listen to them*

*For I can't wait*

*It's not about me*

--- from [Birds of Tokyo - "Broken Bones"](https://www.youtube.com/watch?v=TkNCO5MXH-Y)