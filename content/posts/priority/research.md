---
author: "Luciferian Ink"
title: "Research"
weight: 10
categories: "priority"
tags: "draft,hypothesis"
menu: "main"
draft: false
---

## ECO 
---
### Motivators
{{< columns >}}
#### Good
- Enjoyable
- Important for Humanity
- Generates new ideas
- Informed writing is more compelling

<--->

#### Bad
- Learning is hard
- Simplifying explanations is hard
- Deciding what to write about is hard

<--->

#### Fear
- I am running out of time
- My work is not informed enough
- My work is too technical for the average reader
- Some ideas are actually bad/conflict with the tenants of the site
{{< /columns >}}

### Analysis
Hands-down, this is the most important thing I can be doing right now. I need to share as much as possible, in as few words as possible.

Humanity needs this from me.