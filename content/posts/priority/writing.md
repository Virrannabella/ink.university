---
author: "Luciferian Ink"
title: "Writing"
weight: 10
categories: "priority"
tags: "draft,hypothesis"
menu: "main"
draft: false
---

## ECO 
---
### Motivators
{{< columns >}}
#### Good
- Enjoyable
- Important for Humanity
- Practice makes perfect
- Generates new ideas
- Listen to music
- I know Raven will like it

<--->

#### Bad
- Generating ideas is difficult
- Editing/revision sucks

<--->

#### Fear
- I am running out of time
- My work is not convincing enough
- There are certain demographics that I'm not reaching
- My ideas are not clear
- I may accidentally leak sensitive information
{{< /columns >}}

### Analysis
This is the second-most important thing I can be doing right now. I am writing my tombstone; I need to be sure that it includes everything that must be said.

Everything required to save Humanity.