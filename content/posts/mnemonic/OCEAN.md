---
author: "Luciferian Ink"
title: "OCEAN"
weight: 10
categories: "mnemonic"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[The Warden's research.](/docs/confidants/warden)

## ECO
---
- **O**penness to experience
- **C**onscientiousness
- **E**xtroversion
- **A**greeableness
- **N**euroticism