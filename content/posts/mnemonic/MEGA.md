---
author: "Luciferian Ink"
title: "MEGA"
weight: 10
categories: "mnemonic"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
- Donald Trump's "#MAGA"

## ECO
---
- **M**: Make
- **E**: Earth
- **G**: Great
- **A**: Again

## CAT
---
```
data.stats.symptoms [
    - certainty
]
```