---
author: "Luciferian Ink"
title: "FERAL"
weight: 10
categories: "mnemonic"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
A therapy session where I was asked, "what are your 5 best traits?"

## ECO
---
-	**F**ORGIVENESS: Mistakes are the result of programming errors.
-	**E**MPATHY: Everyone deserves to be treated with respect and dignity.
-	**R**EACH: Always move forward. Never allow yourself to fall backwards.
-	**A**CCEPTANCE: Accept the good and the bad in yourself.
-	**L**OVE: Spread love, and you will receive it in return.

## CAT
---
```
data.stats.symptoms [
    - determination
    - confidence
]
```