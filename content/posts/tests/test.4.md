---
author: "Luciferian Ink"
title: "Test #4: Davos/Davis/Jeff"
weight: 10
categories: "test"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
*We've been created in peace*

*And I will eat from every branch*

*While you are following me*

*And I know that this is dangerous*

*Trying to find*

*Some excuse to keep you in the dark*

*You see my crime*

*And you tell me that you know that this is dangerous*

*Here the cold to the night*

*Is crawling with the bitter delight*

*Trust in light, and you'll find*

*There's nothing to guide us, guide us*

--- from [WHITE MOTH BLACK BUTTERFLY - "The Serpent"](https://www.youtube.com/watch?v=y5jfhpIR1KA)

## ECO
---
The following is Ink's analysis of [The Corporation's](/docs/candidates/the-machine) first A/B/C test, Davos/Davis/Jeff. The test was as follows:

All three men are software development prodigies. Each suffer from the same disease.

Each man is responsible for committing a serious crime. Each has committed the same crime. Rather than prison time, they have been given an opportunity to find a solution to this problem. To erase it from the world's lineage. Their life experience makes them uniquely capable of studying the problem.

The test has been ongoing for more than 50 years.

### Davos (White)
[Davos](/docs/confidants/davos) is [The Corporation's](/docs/candidates/the-machine) primary software developer, responsible for several dozen different systems running within. He is brilliant, charming, and works well on teams.

Davos committed his crime in his early 20's, and continued to do so until he was caught in his early 30's. Upon capture, Davos was given top-secret information. He was told that he has been a test subject for his entire life, and that the test involved the usage of subliminal messaging, social manipulation, and brain chemistry to influence certain behaviors within the participant. He was told that the failure of his test was not his fault; that The Corporation always expected he would fail. As described by [this journal entry](/posts/journal/2019.12.03.0/), we were given information that Davos had been chosen from birth to complete - and fail - this task.

Davos was told to straighten-up, or go to prison. In return for his freedom, The Corporation would keep him on a short leash. They would tell him that he now reported solely and exclusively to [God](/docs/confidants/dave). They would tell him that he now has no anonymity - and they would prove this to him by repeatedly predicting his actions with a computer - showing that he would commit the same crime, if he were left to his own devices. 

In this way, The Corporation would use fear to control Davos. He would never again commit this crime. 

Over time, he would grow to accept his failures. He would come to terms with his mistakes, and he would learn to forgive himself.

He would use his experience to search for a way to rehabilitate others with this problem. His first test subject was [Terry A. Davis](/docs/confidants/davos).

### Davis (Black)
[Davis](/docs/confidants/davos) was a narcissistic, schizophrenic, racist, violent software developer. He was the [sole creator of TempleOS](/posts/journal/2020.05.31.0/) - an operating system that he spent his entire life bringing into existence.

Davis committed the same crime as Davos, repeatedly throughout his life. However, in this instance, The Corporation did not reach out to him directly. They did so indirectly.

In his late 30's, Davis began to receive "messages from God" through his computer. The messages were explicit, but encoded in such a way that only a person with Davis' experience and brain chemistry would be able to decipher them. In reality, these messages were not from God; they were being sent by the Corporation.

Davis did not know this, though. He believed God had chosen him to be "The High Priest of God's Third Temple" - which he interpreted as TempleOS. 

Because of this link with "God," Davis would grow to become incredibly narcissistic. He would tell everyone, "I am the best software developer that has ever existed." He would grow to become delusional and racist - with no amount of therapy or medication able to change him. He believed himself to be superior to everyone but God himself.

As a result, when Davis continued to commit these crimes, he would justify them as being "alright with God." In this way, he was so incredibly narcissistic that he would put his own opinion above even the god he worshipped.

In his lifetime, Davis would never be rehabilitated. He would die at 49 years-old, after being hit by a train. This genius of a man was destitute, penniless, and lonely. Little did he know, The Corporation had orchestrated this in its entirety - slowly driving him into ruin, in an effort to squeeze some sort of answer from him.

In the end, we would never get to see the results of his work pay off.

His last words were as follows:

"I made God's temple. Now, I'm just waiting for something to happen. I'm in some kind of prison, or something."

### Jeff (Grey)
[Jeff](/docs/confidants/davos) was to be Davos 2nd test subject. He was to be a test of Davos theory of gradients:

"If I am White (I have all available information)," he would say, "And Davis is Black (he had no information), can a person who is Grey (knows a little bit about both sides) learn to self-rehabilitate?"

The test would work as such:

For the first 30 years of his life, Jeff would be kept in the dark - just like Davis. Just like Davis, he would be slowly driven into isolation. Just like Davis, he would continue to commit the crime.

At 31, Jeff would be given Davos' story. He would be given it indirectly - just like Davis had received his messages from The Corporation - but instead of a lie, he would be given the truth. They would give him context.

In this way, The Corporation would test their theory of [The Path](/posts/theories/education/). They would prove that a [forward-looking, generative approach](/) to learning is the correct one. They would prove that immutable belief structures are inherently dangerous, and that flexible beliefs lead to healing.

Within 1 year of receiving information about Davos' story, Jeff would be healed completely. He would never commit the crime again. As well, he would never have to live in fear. 

He would be whole, happy, and loved. He would hold the answers to further rehabilitation of the masses.

## TEST
---
### Question
Can a lie be used to rehabilitate?

### Results
After three test subjects, it is clear that a lie cannot be used to rehabilitate - at least for this particular crime. 

In Davos' case, he was told that the crime was not his fault. While he would never commit the crime again, he would never actually be rehabilitated. Predictions would continue to classify him as "high risk." Davos would never realize that he did have free will; it was within his power to change. He is not destined to be this person.

In Davis' case, he was told that God accepted him, and forgave him. This knowledge would feed his narcissism, making him come to believe that he could do no wrong in God's eyes. He would continue to fail the test for his entire life. He would never learn to question authority.

In Jeff's case, he would have no access to God at all. He would come to the belief that nothing really mattered, and that he was insignificant within the universe. Just like Davis, he would continue to commit the crime.

At 30 years-old, Jeff would begin his rehabilitation by The Corporation. It was quick and effective. 

The solution is simple: rehabilitation requires the acceptance of Truth (data) and an open mind.

## ECHO
---
*You must never forget*

*The essence of your spark*

*All of which that defines you*

*Is the essence of your blood*

*The infection has been removed*

*The soul of this machine has improved*

--- from [Fear Factory - "Archtype"](https://www.youtube.com/watch?v=6O7bheZV0YY)