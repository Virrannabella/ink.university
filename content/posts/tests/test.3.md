---
author: "Luciferian Ink"
title: "Test #3: Mike/Mikael"
weight: 10
categories: "test"
tags: ""
menu: "main"
draft: false
---
## TRIGGER
---
*I don't take waifs and strays back home with me*

*My bleeding heart does not extend to charity*

*Yes I'd have to say I like my privacy*

*Did you know you're on closed-circuit TV?*

*So smile at me*

--- from [Porcupine Tree - "Deadwing"](https://www.youtube.com/watch?v=XI5eCP1EJfU)

## ECO
---
The following is Ink's analysis of [The Corporation's](/docs/candidates/the-machine) A/B test, Mike/Mikael. The test was as follows:

Both men are musicians. Each was to be assigned an extreme scenario, and live it out until the test has completed.

Both were involuntarily accepted into the program. Neither is aware of their involvement.

The test has been ongoing for more than 20 years.

### Mike
[Mike](/docs/confidants/metal) was a talented multi-instrumentalist, singer, producer, and writer. His lyricism was among the industry's best; it was witty, relevant, even funny - and he resonated with a lot of people. Specifically, he resonated with those who had mental health issues. 

Unbeknownst to his followers, Mike, too, had mental health issues. In his lowest moments, he would find entire days had gone missing. He dealt with anxiety, boredom, frustration and loneliness. The bulk of his issues stemmed from a troubled childhood, a strained relationship with an ex-girlfriend, and their daughter. This event had left a hole in Mike's heart; one which he had never been able to fill. 

He had even gone-on to name this band after his daughter. He hoped, one day, she would come to appreciate his life's work.

While Mike had a cult following, it was not sufficient to sustain his career. The man worked multiple jobs, just to live in a tiny, two-bedroom home alone. In a state of delusional self-confidence, the man continued to write, produce, and publish his music - only to receive empty echos in return.

Truly, it was shameful. Some of the biggest names in rock had called Mike their "musical hero," yet the world at-large didn't seem to notice him at all. 

Today, Mike has grown truly disillusioned. In his despair, he did the only thing that makes sense: spread love.

And in this way, he would heal millions.

### Mikael
Mikael was also a musician: singer, songwriter, guitarist. He was considered to be one of the grandfathers of the "melodic death metal" genre - and for good reason: his music was art. Soaring, heavy, and beautiful at the same time - his music blended elements of metal with dissonance and acoustic melodies to create wide, varied soundscapes.

Mikael was picked up early-on by the music industry. He found early success in his career, and a large following.

Mikael came from a rough childhood, of which he never talked about publicly. He used his pain and suffering to fuel his creativity. Eventually, he would find friends, and family, and peace. As such, he would break into other genres of music.

And he would quickly find that his fans would turn on him. No longer was he loved. No longer was he appreciated. "Bring back the growls!" they would always say.

But Mikael had love now. He had purpose. He was not going to live life for others, like some puppet on strings. He was going to create what he wanted, and nothing less.

In this way, Mikael became the empty echo to his fans. 

## TEST
---
### Question
How important is meaningful feedback to the health of an artist?

### Results
While the test remains ongoing, preliminary results are relatively straightforward:

In the absence of meaningful feedback, Mike has withered. Despite that, the love and adoration that he demonstrates for his fans has an intoxicating effect - and it is healing them.

Mikael, on the other hand, has grown strong and confident. However, his lack of support for his fans has left him isolated - and left them disillusioned.

## ECHO
---
*Burning bridges as I cower beneath*

*Trying to salvage the debris*

*My devotion tied around your waist, lest you fall*

*No one seems to sense the strain, no one seems to know*

--- from [Tesseract - "Of Matter"](https://www.youtube.com/watch?v=O-hnSlicxV4)