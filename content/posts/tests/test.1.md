---
author: "Luciferian Ink"
title: "Test #1: Revise the Past"
weight: 10
categories: "test"
tags: ""
menu: "main"
draft: false
---

## TEST
---
### Question
Is the past immutable? 

### Results
Yes, it is unchanging. That doesn't mean you can't revise it. It just means that we probably shouldn't.

The best way to revise the past is to re-interpret it in a positive light.