---
author: "Luciferian Ink"
title: "Test #1: Compress the Dictionary"
weight: 10
categories: "test"
tags: ""
menu: "main"
draft: false
---

## TEST
---
### Question
If we were to compress the standard English dictionary, how small could we make it (in megabytes)?

Which compression algorithm would work best?

Is there a better way? Do we need a new algorithm?

### Results
N/A