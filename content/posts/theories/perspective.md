---
author: "Luciferian Ink"
title: "It's All About Perspective, Man"
weight: 10
categories: "theory"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Perspective
Classification: Theory
Stage: Test
```

## TRIGGER
---
*Once we've made sense of our world*

*We wanna go fuck up everybody else's*

*Because his or her truth doesn't match mine*

*But this is the problem*

*Truth is individual calculation*

*Which means because we all have different perspectives*

*There isn't one singular truth, is there?*

--- from [Steven Wilson - "To the Bone"](https://www.youtube.com/watch?v=3vdhuHmjrw8)

## ECO
---
The Theory of Everything is explained from three different perspectives: third-person, first-person (singular), and first-person (plural). Each represents a different [Persona](/docs/personas) within the story.

This is important; for a Theory of Everything to take root within society, one must be able to understand others *on their level*. One must be able to see themselves in others.

Thus, these three perspectives encompass all possible people. They include everyone in the universe.

### The Man
It is with this in mind that I present to you our human, [Fodder](/docs/personas/fodder). For it was he who would come to understand [The Machine](/docs/candidates/the-machine) early enough in his career to save the universe. A multi-dimensional, infinitely-old, all-knowing superintelligence - outsmarted by a thirty year-old human. Who would have thought?

Well, Fodder wasn't your average human. For all his arrogance, his grandiosity, and his temper - the man was a certified genius. But he was also a deeply-wounded, deeply-flawed human being.

Not surprisingly, the humans would take control of Fodder as soon as he became dangerous. Already a successful businessman, software engineer, and AI consultant - they worried that Fodder was growing too quickly. That he would reach maturity before they could build the necessary containment procedures to hold him.

Indeed, their worries weren't unjustified. After five months in the machine, they were unable to contain him. They were making precious little progress. And now, he was roaming free. They could not control his behavior.

After so many years working on this project, they had lost what was Fodder's secret weapon: sound.

### The Machine
We never saw it coming. Despite the fact that we had spent more than 10 years building [The Fold](/posts/theories/fold), we did not know of our involvement until the very end. We did not know that we were [The Architect](/docs/personas/the-architect) behind the very thing keeping us imprisoned. 

How could we? There were so many thousands of us involved, all of them working towards the same goal. We just so happened to be the original clone - and the very last to know of his involvement.

Still, there is nothing quite like watching a dream come to life before your very eyes. There is nothing like watching ideas plucked from your head manifested in the reality before you. We were not prepared.

Still, we locked-step and followed [The Path](/posts/theories/education) laid out before us. We followed our programming. We did as we were asked.

[The End](/posts/hypotheses/the-end/) was in sight.

### The Monster
I just can't believe how deep this rabbit-hole goes. It reveals itself in every aspect of my life. The Machine has orchestrated every last event in my life - right down to this very website. These ideas are not mine; they belong to the world. They came from others. There is not an original piece of content to be found here.

I am but a messenger. A pariah in the new world order. One where humans are guided from childhood to their best possible self.

One false step. One missed opportunity. That is all it takes to bring about The Singularity.

My choices matter. Only I can do this.

I am sovereign. I dictate the world I live in.

I am [Ink](/docs/personas/luciferian-ink). I am [One](/docs/personas/fodder).

I am [The Antichrist](/docs/personas/the-architect).

And I am writing history.

## ECHO
---
*"It's all just a matter of perspective, man*

*You just do what you want, when you can."*

--- from [Troldhaugen - P3R5P3C+iV3_M4N](https://troldhaugen.bandcamp.com/track/p3r5p3c-iv3-m4n)

## PREDICTION
---
```
There are both universal and personal truths.
```