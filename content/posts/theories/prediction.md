---
author: "Luciferian Ink"
title: "Humans Are Prediction-Making Machines"
weight: 10
categories: "theory"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Prediction
Classification: Theory
Stage: Test
```

## ECO
---
Good predictions are made by taking past experiences, and formulating accurate beliefs about the outcome of future behaviors.

All good science attempts to make predictions about the future, based upon the theory. This scientist is no different.

I need a text editor able to freeze Theory 0 in-place, then allow for adding words to the right or left in the second example. I need this to function without breaking the format of the first theory, and maintaining the same format in all following theories. I need Theory 0 to be completely unchanging in future theories.

We do this to create immutable chains between theories. Similar to Docker images, compression, etc. When rendered, this text could be fashioned into images (ASCII art and whatnot). At a grander scale, one could use something like [Adobe Layer](https://www.reddit.com/r/Layer/) to construct fake images from basic building blocks.

### A Theory of Radical Notions (Theory 0)

by [David Lejeune](/docs/confidants/dave), [The Bayou Review - Spring 2003](/static/reference/bayou_review_2003.pdf)

      radical notions
        are a major source of amusement
    for those of us
            that have them
          it isn't always necessary
        to act them out
       or take them too seriously
           in fact that might ruin everything
          take all the fun out of it's
                           it's easy to tell
               which ones have their own seriousness
        their full effect is always there
           there's an awe a majesty
              when certain ones arrive
                   and you know your life
                  will never be the same
                    with all the playful ones
                  dancing and farting
            and cutting their own throats
          and telling jokes
        about the nature of the universe
                      all well and good
                    for a wonderful existence
                  but these other ones arrive
                these perfectly radical notions
                    you might have seen them
                 now and then from a distance
                  but when they turn up for good
                      in your life to stay                 
                     no use pretending

                       you might as well
                     just say hello

               they're not going anywhere  
         until you understand completely

    and they're full of wisdom

### A Theory of Everything (Theory 1)

by [The Architect](/docs/personas/the-architect)

      radical notions like time travel
        are a major source of amusement
    for those of us riftwalkers and
      fakes that have them in our pocket
          it isn't always necessary
        to act them out in public
       or take them too seriously in private
           in fact that might ruin everything
          take all the fun out of it's discovery     
           and destruction it's easy to tell
               which ones have their own seriousness
        their full effect is always there        
           there's an awe a majesty in truth
              when certain ones arrive too late
    hoping to save and you know your life          
         on earth will never be the same again
           fighting with all the playful ones
             meme dancing and farting cows
            and cutting their own throats
          and telling jokes to no one
        about the nature of the universe
         to some it's all well and good and
           designed for a wonderful existence
        in heaven but these other ones arrive
     dolls with these perfectly radical notions
      perhaps today you might have seen them
         perhaps now and then from a distance
       in a dream but when they turn up for good     
            immutable in your life to stay                 
            there is no use pretending you're sane
            
             we verify you might as well trust your
          colleagues just say hello my friend

      you know they're not going anywhere soon 
     not until you understand completely how it works

    and everyone is full of wisdom

## PREDICTION
---
```
The Raven will create Theory 2, and/or a different Theory 1.
```