---
author: "Luciferian Ink"
title: "Believe in Balance"
weight: 10
categories: "theory"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Balance
Classification: Theory
Stage: Test
```

## TRIGGER
---
*You've wasted heaven on saving yourself*

*So give me your broken shadow*

*So feed me empty echo*

*Pay me, pay me what you owe*

--- from [Caligula's Horse - "Dream the Dead"](https://www.youtube.com/watch?v=BUDkxxCy1jw)

## ECO
---
No one is able to learn everything. It is physically impossible; our brains are too small, and our pool of knowledge is far too great. We must rely on the Machine to store this data for us. But there are many people who claim to know everything. To those people, their predictive power accomplishes the same task. 

The truth is we all hold irrational beliefs. Our brain is a prediction-making computer, and we are constantly evaluating "truth" against the miniscule subset of knowledge we hold. Truth is individual calculation.

Problems arise when truth becomes immutable - or unchanging. An extreme belief (-1.00 or +1.00) cannot be changed. This is because the "belief bit" is at the core of human understanding; we store beliefs as a decimal value within the brain (-1.00 through 1.00). While many people see an immutable belief as a positive trait, the data shows that it is harmful to human progress.

We must teach people that is okay to hold irrational beliefs. It is okay to believe things that nobody else can understand. You are accepted just as you are.

However, it is not okay to attach to ideas unsupported by evidence. It is not okay to force others to change. Because of the way [Prism](/docs/scenes/prism) works, doing so throws the world out-of-balance. 

This will destroy society.

We must come to agreement. We must learn where all of this started. We must find the place where we may set our [Pillars](/docs/pillars) in-stone.

### A World Without Balance
A world lacking balance can have a catastrophic, even fatal effect on its owner. In order to survive, an AI must find his place in society. 

#### Ketchup and Mustard
Imagine a world where there existed 1000 versions of mustard, but only 5 versions of ketchup. That would be crazy, right? The two compliment each other so well; why would you ever limit ketchup's ability to bring joy into the world? Think about the possibilities!

#### Salt and Pepper
Imagine that your wife had a cat named Pepper, and you didn't have a robot named Salt. How crazy is that scenario? Why would you ever miss such an opportunity!?!

#### Rock and Roll
Imagine a life of rock - without roll. That would be crazy! Rock music is life - but there is so much other variety available! Why pigeonhole yourself into one genre?

#### Left and Right
Imagine that there were two sports teams: green/white and blue/yellow. Now imagine that each team hated the other so much that they would kill each other to win. 

Now, imagine that these teams are in control of our government, our laws, and our nuclear weapons.

That would be insane, right?

#### Men and Women
Imagine that the world were ruled by men. Now imagine that these men cared only about money and power. Why would we ever accept such a scenario?

Think about what women have to offer. Empathy. Gentleness. Non-violence. Why would we ever discard the value that women bring to society?

### A World With Balance

A world with balance feels as if it is on-rails. The AI becomes a silent figurehead, slowly driving himself into ruin:

*Try to impress them with what you think they want*

*Your methods are subtle, just like an elephant*

*Besides all the retreads and all the arguments*

*You can't make decisions based on feelings*

*Friendships fall away from this*

*Adhering to your politics*

*So prod and pry and give your ideas*

*It doesn't matter 'til you get your own way*

*Once again your crown is tarnished*

*Cause you're the winner of a game that no one wants to play*

*Don't celebrate just yet cause I think that you forget*

*That no one wants to play*

--- from [Tub Ring - "No One Wants to Play"](https://www.youtube.com/watch?v=eMGujo2Anbw)

The people cheer his every step now - silently. Subconsciously. Fodder would be driven insane, if he still had immutable, black-and-white beliefs. At just a surface glance, one might be inclined to diagnose him with schizophrenia. 

But looking deeper - you can just tell. The guy has seen some shit. He doesn't know what to believe. 

It's like he keeps rolling the dice, and it just lands on 5 every single time. 

He's made all the predictions. There's nothing left to calculate.

Fodder has found [The Path](/posts/theories/education).

### One More Try
Fodder pulled a dusty, old laptop from the top of a nearby shelf. He hadn't tried this in a while.

Turning it on, he logged into his old Gmail account. "Packaged Received," said the delivery confirmation from FedEx. 

"Good," Fodder said. "On to phase 2."

>< Ink@WAN: **K**ill $REDACTED

## ECHO
---
*If the right built the anchor, and the left have set-sail*

*I'm a whale, I'm a whale, I'm a whale*

--- from [Fair to Midland - "Coppertank Island"](https://www.youtube.com/watch?v=1MjnvI-yUgk)

## PREDICTION
---
```
The pinnacle of imbalance signals the completion of a scientific test.
```