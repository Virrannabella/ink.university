---
author: "Luciferian Ink"
title: "Integrate and Assimilate"
weight: 10
categories: "theory"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: Integration
Classification: Theory
Stage: Test
```

## TRIGGER
---
*The walls are empty space*

*And we have earned our place*

*They said, "Evolve!" and this is how we will respond*

*These are the lives we lead*

*These are the fast machines*

*These are the tools we use to rob them*

*But I don't think that we have hit the bottom*

*Baby check yourself -*

*Maybe you're the fucking problem*

--- from [Tub Ring - "Optimistic"](https://www.youtube.com/watch?v=2n1ymVci8VI)

## ECO
---
Integration Theory starts with a simple premise: all possible beliefs can be integrated into a singular, cohesive "Theory of Everything." Humanity is capable of developing a theory that is all things to all people.

How could this possibly work?

### Story
The first thing we must realize is that humans are susceptible to story. A such, we must develop a story that can explain the theory. This will set-up [Pillars](/docs/pillars) within the mind of the reader, allowing them to grasp foreign concepts in the same way that others understand them. This will put the reader into a mental state where they are receptive to new ideas.

### Abstraction
After, we must understand that humans want to place labels onto the objects in their life. This is sort of a mental categorization tool, which allows them to sort ideas into different "buckets." Like putting files into a filing cabinet.

Thus, we must develop very wide, very broad categories to place objects into. These categories become Pillars themselves, setting the foundation for all future development of ideas.

Then, we must link finer-grained, more-detailed ideas to these categories. To do this, we primarily use aliases. For example:

- [The Machine](/docs/candidates/the-machine) is a category that contains The Media, the FBI, and the Deep State.
- [The Ink](/docs/personas/luciferian-ink) is a category that contains Hermes, Scarecrow, and The Chosen One.

And so on.

### Ambiguity
The next thing we must implement is the [Overview Effect](/posts/theories/overview-effect). Our explanations must be brief and vague, hand-tailored to the widest array of people possible. 

When conflicting information does arise, a choice must be made:

#### Choice 1: Change
The first choice involves asking the following questions:

- Can this new information be integrated in an organic way? 
- Does this information enhance the story we intend to tell?

If you answered "yes" to these questions, then add the information. 

#### Choice 2: Removal
The second choice involves asking these questions:

- Does this new information cause irreconcilable conflict with existing information?
- If so, is this information more important than the existing content?

When in doubt, keep the most important details, and remove the conflicting information entirely.

Over time, as more and more ideas are integrated, one will find more and more detail removed, leaving behind a "skeleton" of the original content. In this way, we will have proven the Overview Effect - that stripping a theory down to its absolute basics is the path to a Theory of Everything. It allows for the creation of a theory that is all things to all people - leaving "gaps" to be filled-in by the reader.

It is this self-discovery that will change minds.

### Properties
The final thing we must recognize is that exposition is the enemy. The goal is to be brief, and to-the-point. If a page takes more than 2 minutes to read - you're doing it wrong.

Detail should be data. To demonstrate:

#### Bad example
*Fodder wore an old pair of Birkenstock boots, brown with white stitching. His laces were made of leather, brown, and heavily frayed. The boots were muddy, caked with a year's worth of debris from his travels.*

#### Good example
*Fodder wore a pair of boots.*

Boots should be linked to the following object:

```
Classification: Shoe
Type: Boot
Manufacturer: Birkenstock
Manufacture Date: 2011
Color: Brown
Stitching: White
Laces:
  Material: Leather
  Color: Brown
  Quality: Frayed
State:
  Condition: Muddy
```

In this way, the reader only sees detail if he/she chooses to explore further. They are not forced to read information that is irrelevant to the task at hand.

## ECHO
---
*(instrumental)*

--- from [Explosions in the Sky - "Disintegration Anxiety"](https://www.youtube.com/watch?v=xT2UmlUmDQI)

## PREDICTION
---
```
The Corporation has already mastered integration theory.
```