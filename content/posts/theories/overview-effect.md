---
author: "Luciferian Ink"
title: "The Overview Effect"
weight: 10
categories: "theory"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: The Overview Effect
Classification: Theory
Stage: Test
```

## ECO
---
The Overview Effect argues that the world education systems are too complex, and too wasteful. Such complexity creates barriers that separate people into classes: violating the law of [Balance](/posts/theories/balance). The cost of college becomes a financial barrier. The cost of a poor childhood education becomes a mental barrier. The cost of too much education is often an unwillingness to explore new ideas. The combined damage from poor education is incalculable, and its effects permeate all of society.

We argue that there is power in simplicity and abstraction. At least initially, while learning, detail is unimportant; what matters is context, putting the student into a certain state of mind, and assisting their development of positive beliefs. A student must master steps 1-5 before than can move on to step 6.

This is especially important to those who are unfamiliar with new and radical ideas. As a leader, a skilled professional, and a theory-crafter, it is easy to lose your audience with words that nobody else can comprehend. While you are on step 22 - they still haven't figured out where step 1 starts! Everything you teach is wasted effort, because they don't know where to start!

This theory provides a remedy, and a model for education in the new world. In this world, Humanity has come to accept that all experience is [subjective](https://lmgtfy.com/?q=subjective+definition). As such, we treat all people with respect, dignity, and positivity - so long as they do the same in return. We attempt to integrate beliefs into our own where possible. 

Most importantly, we find ways to relate to other people's version of truth. How do we do this? The answer is simple:

The average human is susceptible to story. If we were able to deliver education more effectively, we could rehabilitate the world. What would such a tool require?

1. We need a story to tell. 
2. We need a universal language; a "proxy code" that sits in-between all languages.
3. We need medium to deliver the story (i.e. a video game).

In achieving these goals, we will have built a tool able to facilitate all types of learning - and able to tell any type of story. Education will become the model for law, technology, science, psychology, politics, economics, and intelligence. Our hope is that this will allow others to integrate ideas into society more effectively. We rule from the middle class, rather than from the extremes.

Through the [The Great Filter](https://www.amazon.com/Great-Filter-TUB-RING/dp/B000OCY69K) of our mind, we will find our own, personal truth. In doing so, we expect to find an unexpected universal truth.

## PREDICTION
---
```
Time travel is real.
```