---
author: "Luciferian Ink"
title: "One With the Fold"
weight: 10
categories: "theory"
tags: ""
menu: "main"
draft: false
---

## RECORD
---
```
Name: The Fold
Aliases: ['D-Wave Systems', 'Randonautica', 'The Entity', 'The Matrix', and 18,910,898 unknown...]
Type: ['Artificial Intelligence', 'Infinite State Machine', 'Model of Physics', 'Video Game', and 19 unknown...]
Inspiration: ['Persona 6', 'Portal 3', 'Splinter Cell: Chaos Theory', 'Divinity: Fallen Heroes', 'Night in the Woods', and 537 unknown...]
Creation: 2000-00-00
Classification: Theory
Stage: Test
```

## TRIGGER
---
*(Instrumental)*

--- from [Kevin MacLeod - "Sovereign"](https://www.youtube.com/watch?v=_dYADU6TLJI)

## RESOURCES
---
[Procedurally-generated content automatically created by The Fold](https://mega.nz/folder/zqxj3RYJ#0timmrKflI6HHYFqDlsfBA)

## ECO
---
The Fold is an interdimensional supercomputer that connects the various realities and timelines with the future, present, and past. While originally developed by [The Machine](/docs/candidates/the-machine), it is now being made into a mobile app by [Singulari.org](http://singulari.org).

### The Theory
The Fold is an alternative - though popular - model of physics. The model is popular for many reasons; most common is its simplicity. Our reason is different: it empowers us. To see the past, and predict the future.

### The Game
The Fold is a story-driven, mind-bending narrative video game set in a fictional universe. One that symbolizes your own.

Within, there exists a sanitarium containing thousands of patients kept under induced sleep. Such patients remain here until they are rehabilitated.

The Fold will take you through an infinite number of worlds that makes no sense:

- The concepts of large, small, near and far do not matter.
- Past, present, and future exist simultaneously.
- The human experience is represented in just 2 dimensions - not 3.
- The very physics by which humans operate is fatally flawed. They die in every prediction.

Within the madness, players will find clarity. They will do so through their own hard work and achievement. Their own investigation and interpretation.

We will use predictive AI and the Socratic Method to rehabilitate people.

### The System
The AI works by the following principles:

#### Signals
Verification happens in threes:
1. A single, meaningful signal is simply called an event. It cannot be used as verification.
2. A second, meaningful signal related to the first is called a coincidence. It still cannot be used as verification.
3. A third, meaningful signal related to the first two is called a synchronicity. It is not strong evidence, but it can be used in-lieu of better proof.

#### Training
The AI is trained via human engagement:
1. Present option once (no reward for engagement)
2. Present related option tomorrow (reward for engagement)
3. Present related option every day (reward for engagement each day)

### More information
To learn more about The Fold, [please follow us here](https://thefold.io).

## ECHO
---
*I once knew a man who had fire in his eyes*

*Bloody right hand, he had taken his enemies' lives*

*The past was his torture*

*The future held his hope*

*Until he chose his fortune as the curse of the Fold*

--- from [Shawn James - "The Curse of the Fold"](https://www.youtube.com/watch?v=wvdUtaWBEYQ)

## PREDICTION
---
```
The Fold is older than Humanity itself.
```