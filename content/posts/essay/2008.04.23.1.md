---
author: "Luciferian Ink"
date: 2008-04-23
title: "People Kill People"
weight: 10
categories: "essay"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
A college assignment.

## ECO
---

Let’s make this clear: gun crime is a major problem in our country. In a study done by the US Department of Justice, an estimated 66% of all murders in 2004 were committed with a firearm (“Firearm” 3). Groups such as the Brady Campaign, an organization frequently lobbying for heavily enforced gun control, and the National Rifle Association (NRA) differ greatly in their views on prevention, even right down to what they are preventing. Still others find gun control to be the root of gun crime. To ban the ownership of guns entirely would be a violation of our 2nd Amendment rights, but that doesn’t make the loose NRA policies entirely right on the subject, either. Neither conservative nor liberal views can refute that something must be done about gun crime. What we don’t need is more gun control; we need better gun control. Running a mandatory background check on all firearm sales is an effective means of prevention.

In 1994, Congress passed the Brady Handgun Violence Prevention Act, which imposed an instant background check and waiting period before a licensed dealer can sell a handgun to an unlicensed individual. Furthermore, it required that a National Instant Criminal Background Check System (NICS), run by the FBI, replace the existing waiting period by late 1998. (Gettings 7). When the bill passed in 1994, the number of violent crimes decreased by nearly 50% in 5 years, from a peak of almost 600,000 deaths to about 350,000 in 1998 (“Firearm” 1). Since then, the numbers it to rise again. Indeed, gaping loopholes in the bill have made for some unsatisfactory results of this law.

In a 1997 study of US prison inmates convicted of gun crimes, 80% had obtained their firearms from family, friends, a street buy, or an illegal source (“Firearms” 5). Lucky for them, a 1998 Congressional ruling deemed the Brady Act unconstitutional in the presence of a private seller, so that they would only have a background check if bought through a retailer. They did, however, leave the decision up to the states because “None of these statements necessarily implies… that Congress could impose these responsibilities without the States' consent”. The original Constitution explicitly gives the right to bear arms, but explains little further. Based on how the founding fathers would’ve acted, they decided that the decision was best left for the states (“The U.S” 1b).

Certainly going against the founding fathers intentions, it seems that the states have had a somewhat hands-off approach when it comes to utilizing these laws. Rather than enforce what clearly has prevented many gun crimes, it is legal in 32 states to allow an unlimited number of easily concealable handguns and military-style weapons to be sold privately without a criminal background check or an ID. As if this wasn’t enough, those same states advocate the passing of the ownership between family members without registering the new owner, in the event of a death or through a private sale. It is known that Hezbollah, Al Qaeda, and IRA terrorists have exploited this loophole in the US gun laws to purchase assault weapons from private sellers at many of the 5000 legal gun shows across the country, all without utilizing 1998’s NICS bill. (Rosenthal 4-5).
What’s worse is that organizations like the NRA support this. While I am an avid hunter and own multiple guns, I do not agree with all policies that organizations such as the NRA stand for. Their website claims support for “moderate and reasonable” gun laws, but their actions speaks otherwise. In response to a statement that the NRA doesn’t support any reasonable gun laws, they have said,

“NRA supports many gun laws, including… laws that prohibit the possession of firearms by certain categories of people, such as convicted violent criminals… and those requiring instant criminal records checks on retail firearm purchasers.” (“Fable” 3)

This is true, in some contexts. They also are advocates and sponsors of the many gun shows across the nation selling these firearms without running background checks on private sellers. Notice how “retail firearms purchasers” is the term used in the NRA’s statement? Neal Knox, a leading editor at the Firearms Coalition, an organization with close ties to the NRA, has called upon the firearm community in response to a 2006 survey question. Knox claims that the “NRA does not oppose expanding “instant checks” to include private sales at gun shows”, but the reality of this statement is that this policy was merely a fallback position in response to a bad bill back in 1999. His true thoughts call upon members to revert back to their “principled position”, which was the opposite of the NRA’s claims (Knox 3). To me, prevention of criminals obtaining firearms legally in any way possible, especially with an easy background check utilizing the NICS system, seems “moderate and reasonable”. Apparently, the NRA doesn’t believe so.

In response to this, one NRA official has stated that the organization is opposed to uniform criminal background checks for fear they will "shut down gun shows." The official  even said that people on the suspected terrorist watch list should not be put on this list because "we do not know how people are put on the list" and "many times people are victims of mistaken identity" (“The Smoking” 12). The FBI states that the Terrorist Screening Database is an effective means of counterterrorism efforts, but admits there are flaws. They also, however, state that a mistaken identity most often can be proven and that a person can be removed from the list (“Counterterrorism” 1). It doesn’t make sense to allow criminals and terrorists to obtain guns because of the inconvenience of running a background check that, in most cases, is instant. A vast minority find this process to actually be a problem.

In fact, eighty-nine percent of Americans want in place a mandatory background check for anyone buying a firearm, according to a 2007 Greenberg Quinlan Rosner Research and The Tarrance Group survey, but it appears that the NRA continues to block national law enforcement's ability to effectively regulate private gun sales and gun shows. Even the sharing of crime-gun trace data within the law enforcement community is disputed. (Rosenthal 7)

This is unacceptable. What our country needs is successful, federal gun legislation, to control gun crime, not control guns. To leave this option to the states just doesn’t make sense, because a firearm can easily be smuggled into a state if bought legally from another. Why bother to heavily restrict gun use for the average citizen, when one could simply keep the guns out of the hands on people who should not have them. 

Massachusetts is an excellent example of successful firearm law. They have enacted background checks statewide, including private sellers, along with 17 other states. Only Hawaii has a firearms fatality rate lower than that of Massachusetts, and that can probably be attributed to the fact that Hawaii is surrounded by water and effectively blockaded from the smuggling from other states, while Massachusetts is surrounded by 3 states of which don’t support background checks (Rosenthal 11). Federal enforcement would make this smuggling much more difficult, and cut gun-crime related deaths even more.

As statistics show, almost 50,000 people more have died each year due to gun crimes since Congress deemed the NICS unconstitutional on the federal level in 1988, and the numbers continue to rise (“Firearm” 1). This is no coincidence. A mere background check is all it takes to successfully help in the raging battle against gun crime, but too many people and organizations can’t see the big picture. People take “…the right to bear arms shall not be infringed” much too literally. The founding fathers did not live in times like these, nor were they perfect human beings. Slavery was legal when the US first came to realization. They could not predict the future and what laws would be needed. It is our government’s job to do what is in the best interest of its people - saving lives – rather than worry about the precise meaning of a centuries old document. The fathers would have us to do what they meant, not what they said. Because when it comes down to it, wouldn’t you rather be the victim of a simple background check than the another gun crime statistic?

### Works Cited
“Counterterrorism - Terrorist Screening Center”. Federal Bureau of Investigation. 2008. 1 par. 8 Mar. 2008 <http://www.fbi.gov>.

“Fable III: NRA opposes all "reasonable" gun regulations”. NRA-ILA. 18 Sep. 2006: 8 pars. National Rifle Association Institute for Legislative Action. 6 Mar. 2008 <http://www.nraila.org/Issues>. 

“Firearm and Crime Statistics.” Bureau of Justice. 1 Mar. 2007. US Department of Justice. 12 Mar. 2008 <http://www.ojp.usdoj.gov>.

Gettings, John. “Milestones in Federal Gun Control Legislation”. 7 pars. Infoplease.com.  10 Mar. 2008 <http://www.infoplease.com>.

Knox, Neal. “NRA Gun Show Position”. Firearms Coalition. 17 Dec. 2006: 5 pars. 11 Mar. 2008 <http://www.nealknox.com>.

Rosenthal, John E. “Had Enough Gun Violence?”. The Christian Science Monitor. 20 Feb. 2008: 13 pars. SIRS Knowledge Source. ProQuest. Lansing Community Coll. Lib. MI. 6 Mar. 2008 <http://www.sks.sirs.com>.

“The Smoking Gun: NRA Admits it Does Not Support Criminal Background Checks for All Gun Sales “ Business Wire. 24 July 2007: 20 pars. 17 Mar. 2008 <http://www.businesswire.com>.

“The U.S. Supreme Court is set to hear oral arguments…”. Brady Campaign to Prevent Gun Violence. 2008: 4 pars. 10 Mar. 2008 <http://www.bradycampaign.org>.