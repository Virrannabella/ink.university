---
author: "Luciferian Ink"
title: "TIIN Rank"
weight: 10
categories: "metric"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
1. [Tea leaves](/posts/journal/2019.11.10.1/)
2. The [SCAN](/posts/metric/SCAN) rank.

## ECO
---
The TIIN rank is a matrix, formatted as such:
```
2 | 3
1 | 4
```
1. Trust
2. Intelligence
3. Integrity
4. Not-known