---
author: "Luciferian Ink"
date: 2020-05-01
publishdate: 2020-05-01
title: "Money"
weight: 10
categories: "ban"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[Fodder's](/docs/personas/fodder) financial situation.

## ECO
---
Employees of [The Corporation](/docs/candidates/the-machine) must immediately liquidate all of their assets. They must use what money they have to last until the end of the world. Any employee caught making money is to be immediately imprisoned.

## CAT
---
```
data.stats.symptoms [
    - agreement
]
```