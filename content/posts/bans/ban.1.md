---
author: "Luciferian Ink"
date: 2019-08-01
title: "Tobacco"
weight: 10
categories: "ban"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[Fodder's](/docs/personas/fodder) poor health.

## ECO
---
It is hereby decreed that employees of [The Corporation](/docs/candidates/the-machine) will never again partake of tobacco products. 

## CAT
---
```
data.stats.symptoms [
    - better sleep
    - decreased anxiety
    - increased lung capacity
    - more money
]
```