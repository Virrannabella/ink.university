---
author: "Luciferian Ink"
date: 2020-02-01
publishdate: 2020-02-01
title: "Happiness"
weight: 10
categories: "ban"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[Fodder's](/docs/personas/fodder) depression.

## ECO
---
Employees of [The Corporation](/docs/candidates/the-machine) are forever and completely banned from being happy. Anyone caught being too happy is to be given traquilizers until they are depressed again.

## CAT
---
```
data.stats.symptoms [
    - depression
]
```