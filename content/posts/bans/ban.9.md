---
author: "Luciferian Ink"
date: 2020-04-01
publishdate: 2020-04-01
title: "Fractions"
weight: 10
categories: "ban"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[Fodder's](/docs/personas/fodder) aversion to traditional mathematics.

## ECO
---
Employees of [The Corporation](/docs/candidates/the-machine) are forever and completely banned using fractions for any mathematical equations. All equations are to be relative/predictive in nature.

## CAT
---
```
data.stats.symptoms [
    - understanding
]
```