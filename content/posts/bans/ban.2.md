---
author: "Luciferian Ink"
date: 2019-09-01
title: "Coffee"
weight: 10
categories: "ban"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[Fodder's](/docs/personas/fodder) orange skin.

## ECO
---
[The Corporation](/docs/candidates/the-machine) has been spiking the coffee with carrot juice. This has turned Fodder orange.

It is hereby decreed that all employees refrain from drinking the Corporation's coffee. It cannot be trusted.

## CAT
---
```
data.stats.symptoms [
    - heartburn remedy
    - increased water intake
]
```