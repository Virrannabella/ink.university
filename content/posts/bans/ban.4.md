---
author: "Luciferian Ink"
date: 2019-11-01
title: "Work"
weight: 10
categories: "ban"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[Fodder](/docs/personas/fodder) ghosted his employer.

## ECO
---
Employees of [The Corporation](/docs/candidates/the-machine) are forever and completely banned from working. This is to include:

- The total erasure of work history, schooling, and resume.
- The establishment of law making it illegal to employ former members of the Corporation.
- Anyone caught employing former employees should immediately suffer the same fate.

## CAT
---
```
data.stats.symptoms [
    - uncertainty
    - anticipation
    - poverty
]
```