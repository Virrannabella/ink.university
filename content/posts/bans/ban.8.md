---
author: "Luciferian Ink"
date: 2020-03-01
publishdate: 2020-03-01
title: "Music"
weight: 10
categories: "ban"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
[The happiness ban](/posts/bans/ban.7).

## ECO
---
Employees of [The Corporation](/docs/candidates/the-machine) are forever and completely banned from owning, distributing, or listening to music of any kind. Anyone caught doing so is to be immediately imprisoned.

## CAT
---
```
data.stats.symptoms [
    - anxiety
]
```