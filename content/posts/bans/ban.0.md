---
author: "Luciferian Ink"
date: 2019-07-01
title: "Identity"
weight: 10
categories: "ban"
tags: ""
menu: "main"
draft: false
---

## TRIGGER
---
Organizational policy.

## RESOURCES
---
[![Passwords available in plain text](/static/images/identity.0.png)](/static/images/identity.0.png)

## ECO
---
It is hereby decreed that all employees of [The Corporation](/docs/candidates/the-machine) are to be treated as equals. This includes:

- Employees who work a minimum of 60 hours/week for at least 3 consecutive months are eligible to receive an "'atta boy" from their direct manager.
- Lunch boxes have been banned in the break room. All employees are to wrap their lunches in brown, unlabeled paper bags. This is to ensure that employees spend more time searching the refrigerator for their lunch than eating it.
- Usernames and passwords are to be removed from all systems. Everyone is to share the same account. Everyone is exactly the same person.
- New ideas have been banned. Employees are to do exactly what they are asked to do, without question, judgement, or hesitation. 
- If an employee makes a mistake, they are to be terminated immediately. We only accept perfect replicas of our perfect Machine.

## CAT
---
```
data.stats.symptoms [
    - anxiety
    - frustration
    - hatred
    - fear
    - mistrust
]
```